package com.service.processor;

import com.service.processor.domain.Book;
import com.service.processor.dto.BookRequest;
import com.service.processor.logic.utils.ServiceMarshaller;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
public class ServiceMarchallerTest {

    @Test
    public void shouldMarshalBookObjectToJSO() {
        BookRequest book = new BookRequest();
        book.setPrice(550.00);
        book.setQuantity(100);
        //Academic, religious, LeaderShip
        book.setType("Academic");
        book.setTitle("Java Programming");

        String bookJson = ServiceMarshaller.marshalObjectToJson(book);
        System.out.println("book json " + bookJson);

    }

    @Test
    public void shouldUnamrshalBookJsonToObject() throws IOException {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("bookRequest.json");
        String json = IOUtils.toString(inputStream, "UTF-8");
        BookRequest book = ServiceMarshaller.unmarshallJsonFile(json, BookRequest.class);
        Assert.assertEquals(String.valueOf(book.getPrice()),"550.0");
        Assert.assertEquals(book.getQuantity(),100);
        Assert.assertEquals(book.getType(),"Academic");
        Assert.assertEquals(book.getTitle(), "Java Programming");
    }
}
