package com.service.processor.infastructure.adapter;


import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface KeystoneAdapter {

    CustomerProfileEnquiryResponse genericCustomerEnquiry(String ucn, Integer ucnCompanyId, Boolean fetchAllAccounts);

    CustomerProfileEnquiryResponse inquireOnCustomerAndAccountProfile(String ucn, Integer ucnCompanyId, Boolean fetchAllAccounts);

    RegulatoryStatus inquireRegulatoryStatus(String ucn);

    SadInquiryResponse inquireSad(InquireSadRequest inquireSadRequest);

    PayoutInfo personalLoansAccountSetup(AccountSetupRequest accountSetupRequest);

    PayoutInfo payoutToPersonalLoanAccount(PayoutRequest payoutRequest);

    PayoutInfo addPersonalLoanInitiationFee(AddInitiationFeeRequest addInitiationFeeRequest);

    PayoutInfo addCustomerProtectionPlanToPersonalLoan(AddCustomerProtectionPlanRequest addCustomerProtectionPlanRequest);

    PayoutInfo schedulePersonalLoan(ScheduleLoanRequest scheduleLoanRequest);

    PayoutInfo storeMandateInformationForPersonalLoan(StoreMandateInformationForLoanRequest storeMandateInformationForLoanRequest);

    Integer inquireOnMultipleProfilesForCustomer(Integer companyId, String idNumber, String idType);

    Map<String, String> getAllSettings();

    Map<String, String> getSettingsForApplication(KeystoneSettingRequest request);

    String getSettingForApplicationAndKey(KeystoneSettingRequest request);

    Optional<List<BranchDTO>> fetchBranchList();
}
