package com.service.processor.infastructure.adapter.impl;

import com.service.processor.infastructure.adapter.MqAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;


import javax.transaction.Transactional;

@Component
@RefreshScope
public class ActiveMQAdapterBean implements MqAdapter {

    @Autowired JmsMessagingTemplate noTxJmsMessagingTemplate;

    @Qualifier("txTemplate")
    @Autowired JmsMessagingTemplate txJmsMessagingTemplate;

    @Value("${d.response.queue.name}") String dCreateWorkResponseQueue;

    @Value("${d.status.update.queue.name}") String dStatusUpdateQueue;

    @Value("${a.request.queue.name}") String aPRRequestQueue;

    @Value("${s.request.queue.name}") String aSPRequestQueue;


    @Override
    public void sendDCreateWorkResponse(String message, final String correlationID) {
        txJmsMessagingTemplate.getJmsTemplate().convertAndSend(dCreateWorkResponseQueue, message, messageHeader -> {
            messageHeader.setJMSCorrelationID(correlationID);
            return messageHeader;
        });
    }

    @Override
    public void sendDStatusUpdate(String message, final String correlationID) {
        noTxJmsMessagingTemplate.getJmsTemplate().convertAndSend(dStatusUpdateQueue, message, messageHeader -> {
            messageHeader.setJMSCorrelationID(correlationID);
            return messageHeader;
        });
    }

    @Override
    public void sendRequest(String message, String correlationID) {
        txJmsMessagingTemplate.getJmsTemplate().convertAndSend(aPRRequestQueue, message, messageHeader -> {
            messageHeader.setJMSCorrelationID(correlationID);
            return messageHeader;
        });
    }

    @Override
    public void sendRequest(String message, String correlationID) {
        txJmsMessagingTemplate.getJmsTemplate().convertAndSend(aSPRequestQueue, message, messageHeader -> {
            messageHeader.setJMSCorrelationID(correlationID);
            return messageHeader;
        });
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public <T> void sendTransactedMessage(String queueName, T message) {
        txJmsMessagingTemplate.getJmsTemplate().convertAndSend(queueName, message);
    }
}
