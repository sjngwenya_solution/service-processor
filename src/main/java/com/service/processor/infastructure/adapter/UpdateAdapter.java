package com.service.processor.infastructure.adapter;

public interface UpdateDebitOrderDetailsAdapter {

     void update(DebitOrderUpdateDetails debitOrderUpdateDetails);
}
