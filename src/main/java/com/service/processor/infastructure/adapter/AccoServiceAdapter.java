package com.service.processor.infastructure.adapter;


import java.util.function.Predicate;

public interface AccountServiceAdapter {

    Product getProduct();

    Predicate<Account> cisAccountFilter();

    AccountDTO accountSearch(String accountNumber);
}
