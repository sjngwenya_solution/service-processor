package com.service.processor.infastructure.adapter;


public interface CommunicationServiceAdapter {

    void sendEmailNotification(EmailNotificationDTO emailNotificationDTO, EmailDetailsDTO emailDetailsDTO);
    void sendSLAEmailNotification(WorkflowEventDTO workflowEventDTO, EmailDetailsDTO emailDetailsDTO);
}
