package com.service.processor.infastructure.adapter;

public interface ActiveMQAdapter {

    void sendDCreateWorkResponse(String message, String correlationID);

    void sendDStatusUpdate(String message, String correlationID);

    void sendADOPRRequest(String message, String correlationID);

    void sendADOSPRequest(String message, String correlationID);

    <T> void sendTransactedMessage(String queueName, T message);
}
