package com.service.processor.infastructure.adapter.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Component
@RefreshScope
public class ContentServiceAdapterBean implements ContentServiceAdapter {

    @Value("${plexus.core.service.url.content}") String contentServiceURL;
    @Value("${plexus.tds.sender.id}") String systemId;
    @Value("${plexus.core.service.companyId.customer}") String companyId;

    @Autowired RestTemplate restTemplate;

    @Override
    @HystrixCommand(commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000") })
    public DocumentURLResponseDTO retrieveDocumentURL(String documentID) {
        HttpEntity requestEntity = new HttpEntity<>(httpHeaders());
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(contentServiceURL + "content/"+ documentID +"/viewUrl");
        String requestUrl = builder.build(false).toUriString();


        ResponseEntity<DocumentURLResponseDTO> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, requestEntity, DocumentURLResponseDTO.class);
        return responseEntity.getBody();
    }

    @Override
    @HystrixCommand(commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000") },
        fallbackMethod = "documentSearchFallback"
    )
    public List<DocumentSearchResponseDTO> documentSearch(DocumentSearchCriteriaDTO documentSearchCriteriaDTO){
        String url = contentServiceURL + "/content/documentSearch/all/viewInformation";
        HttpEntity<DocumentSearchCriteriaDTO> request = new HttpEntity<>(documentSearchCriteriaDTO, httpHeaders());
        ResponseEntity<String> result = this.restTemplate.postForEntity(url, request, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        List<DocumentSearchResponseDTO> listDTO =null;
        try {
             listDTO =  objectMapper.readValue(result.getBody(), new TypeReference<List<DocumentSearchResponseDTO>>(){});
        }catch (IOException e) {
            throw new ServiceException(ServiceExceptionType.UNKNOWN, e.getMessage(), e);
        }
        return listDTO;
    }

    public List<DocumentSearchResponseDTO> documentSearchFallback(DocumentSearchCriteriaDTO documentSearchCriteriaDTO) {
        return Collections.EMPTY_LIST;
    }

    private org.springframework.http.HttpHeaders httpHeaders() {
        org.springframework.http.HttpHeaders jsonHeaders = new HttpHeaders();
        jsonHeaders.set("companyId",companyId);
        jsonHeaders.set("REQUEST-SYSTEM-ID",systemId);
        jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        return jsonHeaders;
    }
}
