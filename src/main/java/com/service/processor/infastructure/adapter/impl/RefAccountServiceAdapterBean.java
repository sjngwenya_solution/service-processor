package com.service.processor.infastructure.adapter.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;


@Component
@Qualifier("revolvingFacility")
public class RFAccountServiceAdapterBean implements AccountServiceAdapter {

    private static final String DEFAULT_URL = "http://localhost:9380/keystone-webservice/}";

    private static final Collection RF_SUB_PRODUCT_CODES = Arrays.asList("Y1", "Y2", "Y3", "Y4", "Y5", "Y6", "Y7");
    private static final Predicate<String> RF_ACCOUNT_FILTER = subCode -> RF_SUB_PRODUCT_CODES.contains(subCode);

    private static final String INQUIRE_ENDPOINT_URI = "/hogan/revolvingFacilityEnquire";
    private static final String ALL_ACCOUNTS_ENDPOINT_URI = "/hogan/genericCustomerEnquiry";

    private RestTemplate restTemplate;
    private String url;

    @Autowired
    private TypeMapper mapper;

    public RFAccountServiceAdapterBean(RestTemplate restTemplate,
                                       @Value("${plexus.keystone.service.url.hogan:" + DEFAULT_URL) String url) {
        this.restTemplate = restTemplate;
        this.url = url;
    }

    @Override
    public Product getProduct() {
        return RF;
    }

    @Override
    public Predicate<Account> cisAccountFilter() {
        return account -> RF_ACCOUNT_FILTER.test(account.getAccountSubProduct());
    }

    @Override
    public AccountDTO accountSearch(String accountNumber) {

        if (StringUtils.isEmpty(accountNumber))
            return null;

        RevolvingFacilityEnquiryRequest enquire = new RevolvingFacilityEnquiryRequest(accountNumber);
        HttpEntity<RevolvingFacilityEnquiryRequest> requestEntity = new HttpEntity<>(enquire, httpHeaders());

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + INQUIRE_ENDPOINT_URI);
        String requestUrl = builder.build(false).toUriString();
        ResponseEntity<RevolvingFacilityEnquiryResponse> responseEntity =
                restTemplate.exchange(requestUrl, HttpMethod.POST, requestEntity, RevolvingFacilityEnquiryResponse.class);
        HttpStatus statusCode = responseEntity.getStatusCode();

        if(! statusCode.is2xxSuccessful())
            throw new ServiceException(ServiceExceptionType.UNKNOWN, String.format("Unexpected response code : %s", statusCode.value()));

        return mapResponse(responseEntity.getBody());
    }

    private AccountDTO mapResponse(RevolvingFacilityEnquiryResponse response) {
        if(Objects.isNull(response))
            return null;

        return mapper.convert(response, RFAccountDetailsDTO.class);
    }

    private org.springframework.http.HttpHeaders httpHeaders() {
        org.springframework.http.HttpHeaders jsonHeaders = new HttpHeaders();
        jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        return jsonHeaders;
    }
}
