package com.service.processor.infastructure.adapter.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RefreshScope
public class CommunicationServiceAdapterBean implements CommunicationServiceAdapter {

    @Value("${plexus.core.service.url.communication}") String communicationServiceEndpoint;
    @Value("${plexus.core.service.companyId.customer}") String companyId;
    @Value("${plexus.tds.sender.id}") String senderID;
    @Value("${plexus.ado.file.notification.template.name}") String fileTemplateName;
    @Value("${plexus.ado.sla.notification.template.name}") String slaTemplateName;

    @Autowired
    RestTemplate restTemplate;

    @Override
    @HystrixCommand(commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000") })
    public void sendEmailNotification(EmailNotificationDTO emailNotificationDTO, EmailDetailsDTO emailDetailsDTO) {

        HttpEntity requestEntity = new HttpEntity<>(httpHeaders());
        restTemplate.exchange(buildFileNotificationUrl(emailNotificationDTO, emailDetailsDTO), HttpMethod.POST, requestEntity, EmailNotificationResponseDTO.class);
    }

    @Override
    @HystrixCommand(commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000") })
    public void sendAdoSLAEmailNotification(WorkflowEventDTO workflowEventDTO, EmailDetailsDTO emailDetailsDTO) {

        HttpEntity requestEntity = new HttpEntity<>(httpHeaders());
        restTemplate.exchange(buildAdoSLANotificationUrl(workflowEventDTO, emailDetailsDTO), HttpMethod.POST, requestEntity, EmailNotificationResponseDTO.class);
    }

    private String buildFileNotificationUrl(EmailNotificationDTO emailNotificationDTO, EmailDetailsDTO emailDetailsDTO){
        StringBuilder endpointUrl = new StringBuilder(communicationServiceEndpoint);
        endpointUrl.append("from=" +emailDetailsDTO.getFromEmail())
                .append("&subject=" +extractFileName(emailNotificationDTO.getDescription()))
                .append("&templateName=" +fileTemplateName).append("&to=" +emailDetailsDTO.getToEmail());
        return endpointUrl.toString();
    }

    private String buildAdoSLANotificationUrl(WorkflowEventDTO workflowEventDTO, EmailDetailsDTO emailDetailsDTO){
        StringBuilder endpointUrl = new StringBuilder(communicationServiceEndpoint);
        endpointUrl.append("from=" +emailDetailsDTO.getFromEmail())
                .append("&subject=SLA Violation on status <" + workflowEventDTO.getSlaViolationStatus() + "> for workitem <" + workflowEventDTO.getWorkItemId() + ">" )
                .append("&templateName=" +slaTemplateName).append("&to=" +emailDetailsDTO.getToEmail());
        return endpointUrl.toString();
    }

    private org.springframework.http.HttpHeaders httpHeaders() {
        org.springframework.http.HttpHeaders jsonHeaders = new HttpHeaders();
        jsonHeaders.set("REQUEST-SYSTEM-USER-ID",companyId);
        jsonHeaders.set("REQUEST-SYSTEM-ID", senderID);
        jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        return jsonHeaders;
    }

    public String extractFileName(String value){
        return value.substring((value.lastIndexOf("/") + 1), value.lastIndexOf("]"));
    }
}
