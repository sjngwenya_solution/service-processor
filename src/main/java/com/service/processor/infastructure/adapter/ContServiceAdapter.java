package com.service.processor.infastructure.adapter;



import java.util.List;

public interface ContentServiceAdapter {

    DocumentURLResponseDTO retrieveDocumentURL(String documentID);

    List<DocumentSearchResponseDTO> documentSearch(DocumentSearchCriteriaDTO documentSearchCriteriaDTO);

}
