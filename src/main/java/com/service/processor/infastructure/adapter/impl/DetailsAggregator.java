package com.service.processor.infastructure.adapter.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;


import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;


@Component
public class DebitOrderDetailsAggregator {

    private static final BiFunction<CustomerServiceAdapter, DebitOrderSearchCriteria, CustomerProfile> CIS_ACCOUNT_SEARCH
            = (adapter, searchCriteria) ->  adapter.customerSearchByAccountNo(searchCriteria.getAccountNumber(),
            searchCriteria.getProduct().getProductCode());

    private static final BiFunction<AccountServiceAdapter, DebitOrderSearchCriteria, AccountDTO> PRODUCT_HOUSE_ACCOUNT_SEARCH
            = (adapter, searchCriteria) -> {
        AccountDTO accountDTO = adapter.accountSearch(searchCriteria.getAccountNumber());
        if(nonNull(accountDTO))
            AccountDataPostProcessor.process(accountDTO);
        return accountDTO;
    };

    private static final BiFunction<CustomerServiceAdapter, DebitOrderSearchCriteria, List<CustomerProfile>> CIS_CUSTOMER_SEARCH
            = (adapter, searchCriteria) -> {

        if(isNull(searchCriteria.getUcn()) && isEmpty(searchCriteria.getIdentityNumber()))
            throw new ServiceException(INCOMPLETE_REQUEST, "UCN or ID Number required for customer search.");

        if(isNull(searchCriteria.getUcn())) {
            List<CustomerProfile> customerProfiles = adapter.customerSearchByIdNumber(searchCriteria.getIdentityNumber());
            if (isNull(customerProfiles))
                return Collections.emptyList();

            return customerProfiles;
        }else {
            CustomerProfile profile = adapter.customerSearchByUcn(searchCriteria.getUcn());
            if(isNull(profile))
                return Collections.emptyList();

            return Collections.singletonList(profile);
        }
    };

    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    private CustomerServiceAdapter customerServiceAdapter;
    private List<AccountServiceAdapter> accountServiceAdapters;

    public DebitOrderDetailsAggregator(CustomerServiceAdapter customerServiceAdapter,
                                       List<AccountServiceAdapter> accountServiceAdapters) {

        this.customerServiceAdapter = customerServiceAdapter;
        this.accountServiceAdapters = accountServiceAdapters;
    }

    public DebitOrderDetailsDTO searchForDetails(DebitOrderSearchCriteria searchCriteria) {
        try {

            if( isNull(searchCriteria.getProduct()))
                throw new ServiceException(INVALID_REQUEST, "Product required for search");

            if (searchCriteria.profileSearch() && searchCriteria.accountSearch()) {
                return searchByIdentityAndAccount(searchCriteria);
            } else if(searchCriteria.accountSearch()) {
                return searchByAccount(searchCriteria);
            }else if(searchCriteria.profileSearch()) {
                return searchByIdentity(searchCriteria);
            }

            throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, "Unable to determine search parameters.");
        } catch (Exception e) {
            throw new ServiceException(ServiceExceptionType.UNKNOWN, e.getMessage());
        }
    }

    private DebitOrderDetailsDTO searchByIdentityAndAccount(DebitOrderSearchCriteria searchCriteria) throws
            ExecutionException, InterruptedException {

        Future<List<CustomerProfile>> futureProfileResult = executorService.submit(() ->
                CIS_CUSTOMER_SEARCH.apply(customerServiceAdapter, searchCriteria));
        Future<AccountDTO> futureAccountResult = executorService.submit(() ->
                PRODUCT_HOUSE_ACCOUNT_SEARCH.apply(findAccountServiceAdapter(searchCriteria.getProduct()), searchCriteria));

        List<CustomerProfile> customerProfiles = futureProfileResult.get();
        AccountDTO accountDTO = futureAccountResult.get();

        if( !isNull(accountDTO) ) {
            //Check matched profile on CIS
            CustomerProfile linkedProfile = customerProfiles.stream()
                    .filter(profile -> accountMatch(profile, searchCriteria.getAccountNumber()))
                    .findFirst()
                    .orElse(CIS_ACCOUNT_SEARCH.apply(customerServiceAdapter, searchCriteria));

            if(linkedProfile == null)
                return new DebitOrderDetailsDTO(customerProfiles, accountDTO);
            else
                return new DebitOrderDetailsDTO(Collections.singletonList(linkedProfile), accountDTO);
        }
        return new DebitOrderDetailsDTO(customerProfiles, accountDTO);
    }

    private DebitOrderDetailsDTO searchByAccount(DebitOrderSearchCriteria searchCriteria) throws
            ExecutionException, InterruptedException {

        Future<CustomerProfile> futureProfileResult = executorService.submit(() ->
                CIS_ACCOUNT_SEARCH.apply(customerServiceAdapter, searchCriteria));

        Future<AccountDTO> futureAccountResult = executorService.submit(() ->
                PRODUCT_HOUSE_ACCOUNT_SEARCH.apply(findAccountServiceAdapter(searchCriteria.getProduct()), searchCriteria));

        return new DebitOrderDetailsDTO(futureProfileResult.get(), futureAccountResult.get());
    }

    private DebitOrderDetailsDTO searchByIdentity(DebitOrderSearchCriteria searchCriteria) throws
            ExecutionException, InterruptedException {

        List<CustomerProfile> profiles = CIS_CUSTOMER_SEARCH.apply(customerServiceAdapter, searchCriteria);
        if(profiles.size() == 1) {
            AccountServiceAdapter accountServiceAdapter = findAccountServiceAdapter(searchCriteria.getProduct());

            CustomerProfile profile = profiles.iterator().next();
            profile.setAccounts(filterOut(profile.getAccounts(), accountServiceAdapter.cisAccountFilter()));

            Optional<String> optionalAccount = tryGetAccountNumber(profile);
            if(optionalAccount.isPresent())
            {
                searchCriteria.setAccountNumber(optionalAccount.get());
                AccountDTO accountDTO = PRODUCT_HOUSE_ACCOUNT_SEARCH.apply(accountServiceAdapter, searchCriteria);
                return new DebitOrderDetailsDTO(profiles, accountDTO);
            }
        }
        return DebitOrderDetailsDTO.createPartialResults(profiles);
    }

    private AccountServiceAdapter findAccountServiceAdapter(Product product) {
        return accountServiceAdapters.stream()
            .filter(accountServiceAdapter -> Objects.equals(product, accountServiceAdapter.getProduct()))
            .findFirst()
            .orElseThrow(() -> new ServiceException(INVALID_REQUEST, format("Unable to find product adapter for request. [%s] ", product)));
    }

    private Collection<Account> filterOut(Collection<Account> customerAccounts, Predicate<Account> predicate) {
        return customerAccounts.stream()
                .filter(account -> predicate.test(account))
                .collect(Collectors.toList());
    }

    private Optional<String> tryGetAccountNumber(CustomerProfile customerProfile) {

        if(customerProfile.hasCurrentOneAccount())
        {
            String accountNo = customerProfile.getAccounts().iterator().next().getAccountNo();
            return Optional.ofNullable(stripLeadingZeros(accountNo));
        }
        return Optional.empty();
    }

    private boolean accountMatch(CustomerProfile profile, String accountNumber) {
        return profile.getAccounts().stream()
                .anyMatch(account -> StringUtils.equals(stripLeadingZeros(account.getAccountNo()),
                        stripLeadingZeros(accountNumber)));
    }
}
