package com.service.processor.infastructure.adapter.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Qualifier("revolvingFacility")
@Component
@RefreshScope
public class UpdateRFDebitOrderDetailsKeystoneAdapterBean implements UpdateDebitOrderDetailsAdapter {

    private static final String ENDPOINT_URI = "/hogan/revolvingFacilityMaintenance";

    private RestTemplate restTemplate;
    private String url;

    public UpdateRFDebitOrderDetailsKeystoneAdapterBean(RestTemplate restTemplate,
                                                        @Value("${plexus.keystone.service.url.hogan}") String url) {
        this.restTemplate = restTemplate;
        this.url = url;
    }

    @Override
    public void update(DebitOrderUpdateDetails debitOrderUpdateDetailsRequest) {
        HttpEntity<DebitOrderUpdateDetails> requestEntity = new HttpEntity<>(debitOrderUpdateDetailsRequest, httpHeaders());

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + ENDPOINT_URI);
        String requestUrl = builder.build(false).toUriString();
        ResponseEntity<String> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.POST, requestEntity, String.class);
        HttpStatus statusCode = responseEntity.getStatusCode();

        if(! statusCode.is2xxSuccessful())
            throw new ServiceException(ServiceExceptionType.UNKNOWN, String.format("Unexpected response code : %s", statusCode.value()));
    }

    private HttpHeaders httpHeaders() {
        HttpHeaders jsonHeaders = new HttpHeaders();
        jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        return jsonHeaders;
    }
}
