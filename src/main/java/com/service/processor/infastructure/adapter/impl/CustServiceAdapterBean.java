package com.service.processor.infastructure.adapter.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.util.Arrays.asList;

@Component
@RefreshScope
public class CustomerServiceAdapterBean implements CustomerServiceAdapter {

    @Value("${plexus.keystone.service.url.hogan}") String keystoneServiceURL;
    @Value("${plexus.core.service.companyId.customer}") String companyId;

    @Autowired
    private  RestTemplate restTemplate;

    @Override
    @HystrixCommand(commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "20000") })
    public CustomerProfile customerSearchByUcn(Long ucn) {

        if(Objects.isNull(ucn))
            return null;

        CustomerProfileEnquiryRequest enquiryRequest = CustomerProfileEnquiryRequest.createUcnSearchRequest(ucn);
        return getFirst(enquiryRequest);
    }

    @Override
    @HystrixCommand(commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "20000") })
    public CustomerProfile customerSearchByAccountNo(String accountNo, String accountProductCode) {
        if(Objects.isNull(accountNo) && Objects.isNull(accountProductCode))
            return null;

        CustomerProfileEnquiryRequest enquiryRequest = CustomerProfileEnquiryRequest
                .createAccountSearchRequest(accountNo, accountProductCode);

        return getFirst(enquiryRequest);
    }


    @Override
    @HystrixCommand(commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "20000") })
    public List<CustomerProfile> customerSearchByIdNumber(String idNumber) {

        if(StringUtils.isEmpty(idNumber))
            return null;

        CustomerProfileEnquiryRequest enquiryRequest = CustomerProfileEnquiryRequest.createIDSearchRequest(idNumber, "RSAID");
        return executeCustomerSearch(enquiryRequest);
    }

    private CustomerProfile getFirst(CustomerProfileEnquiryRequest enquiryRequest) {
        return executeCustomerSearch(enquiryRequest)
                .stream().findFirst().orElse(null);
    }

    private List<CustomerProfile> executeCustomerSearch(CustomerProfileEnquiryRequest enquiryRequest) {
        String url = keystoneServiceURL + "/hogan/customerSearch ";

        HttpEntity<CustomerProfileEnquiryRequest> requestEntity = new HttpEntity<>(enquiryRequest, httpHeaders());
        ResponseEntity<CustomerProfile[]> responseEntity =
                restTemplate.exchange(url, HttpMethod.POST, requestEntity, CustomerProfile[].class);

        if(Objects.isNull(requestEntity.getBody()))
            return Collections.emptyList();

        return asList( responseEntity.getBody() );
    }

    private org.springframework.http.HttpHeaders httpHeaders() {
        org.springframework.http.HttpHeaders jsonHeaders = new HttpHeaders();
        jsonHeaders.set("companyId",companyId);
        jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        return jsonHeaders;
    }
}
