package com.service.processor.infastructure.adapter;


public interface MandateServiceAdapter {
   String sendMandate(MandateDTO mandateDTO);
   String cancelMandate(String applicationNumber);
   boolean isExistingMandateInFlight(String applicationNumber);
}
