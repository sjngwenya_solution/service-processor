package com.service.processor.infastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DebitCheckMandateAuditRepository extends JpaRepository<DebitCheckMandateAudit, Long> {
}
