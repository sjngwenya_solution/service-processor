package za.co.fnb.plexus.services.request.infrastructure.repository;

import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;

import java.util.Set;

public interface ExtendedRequestRepository {

    void updateRequestMetaData(Long workItemId, String descriptionKey, String description, String statusDate);

    void updateRequestMetaData(Long workItemId, Set<RequestMetadata> requestMetadata, String statusDate);

    void updateRequestMetaData(Long workItemId, String metadataKey, String value);
}
