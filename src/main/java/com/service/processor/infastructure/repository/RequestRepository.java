package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Repository
public interface RequestRepository extends JpaRepository<Request, Long>, ExtendedRequestRepository {

    /**
     * Finds request with matching ID
     *
     * @param id
     * @return The request with the corresponding ID
     */
    Request findRequestEntityById(Long id);

    Request findByReferenceNumber(@Param("referenceNumber") String referenceNumber);

    Request findByWorkItemId(@Param("workItemId") Long workItemId);

    Request findByExternalReferenceNumber(@Param("externalReferenceNumber") String externalReferenceNumber);

    @Query("from Request req, RequestMetadata data where data.value = :reference and req.id = data.request_id ")
    Request findByExternalReference(@Param("reference") String reference);

    @Query("select data.value from Request req, RequestMetadata data where data.name = :name and req.id = data.request_id  and req.workItemId = :workItemId")
    String findMetadataValue(@Param("workItemId") Long workItemId, @Param("name") String name);

    @Query("from Request where externalReferenceNumber = :externalReferenceNumber and status in (:statuses) and workItemId is not null order by createdOn desc")
    Set<Request> findByExternalReferenceNumberAndStatus(@Param("externalReferenceNumber") String externalReferenceNumber, @Param("statuses") List<RequestStatus> statuses);
}
