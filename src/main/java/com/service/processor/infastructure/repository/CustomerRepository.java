package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.fnb.plexus.services.request.domain.party.Customer;

import java.util.stream.Stream;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Stream<Customer> findByPartyUcn(String ucn);
}
