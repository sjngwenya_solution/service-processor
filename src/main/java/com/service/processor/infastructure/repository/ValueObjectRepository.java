package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import za.co.fnb.plexus.services.request.domain.ValueObject;

import java.util.List;

@Repository
public interface ValueObjectRepository<T extends ValueObject> extends CrudRepository<T, String> {

    <T> T findByCode(String code);

    @Query(value = "from #{#entityName} where lower(description) = lower(:description) ")
    <T> T findByDescription(@Param("description") String description);

    @Query(value = "from #{#entityName} where lower(description) = lower(:description) ")
    List<T> findListByDescription(@Param("description") String description);
}
