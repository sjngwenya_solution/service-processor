package za.co.fnb.plexus.services.request.infrastructure.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import za.co.fnb.plexus.services.request.domain.Branch;

/**
 * Created by neha on 2018/04/24.
 */
@Repository
public interface BranchRepository extends JpaRepository<Branch, String> {

    @Query("select b from Branch b where b.code = :code")
    Branch findByCode(@Param("code") String code);
}
