package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

@Repository
public class ExtendedRequestRepositoryImpl implements ExtendedRequestRepository {

    @PersistenceContext
    private EntityManager manager;

    @Modifying
    @Override
    public void updateRequestMetaData(Long workItemId, String descriptionKey, String description, String statusDate) {
        getRequest(workItemId).ifPresent(request -> {
            request.updateMetaData(statusDate, new Date().toString());
            request.updateMetaData(descriptionKey, description);
            manager.merge(request);
        });
    }

    @Override
    public void updateRequestMetaData(Long workItemId, Set<RequestMetadata> requestMetadata, String statusDate) {
        getRequest(workItemId).ifPresent(request -> {
            requestMetadata.forEach(metadata -> {
                request.updateMetaData(metadata.getName(), metadata.getValue());
            });
            request.updateMetaData(statusDate, new Date().toString());
            manager.merge(request);
        });
    }

    @Modifying
    @Override
    public void updateRequestMetaData(Long workItemId, String metadataKey, String value) {
        getRequest(workItemId).ifPresent(request -> {
            request.updateMetaData(metadataKey,value);
            manager.merge(request);
        });
    }

    private Optional<Request> getRequest(long workItemId) {
        Query query = manager.createQuery("from Request where workItemId = :workItemId");
        query.setParameter("workItemId", workItemId);

        return Optional.ofNullable(query.getSingleResult())
                .map(o -> (Request) o);
    }
}
