package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.fnb.plexus.services.request.domain.LookupData;

import java.util.Optional;

public interface LookupDataRepository extends JpaRepository<LookupData, Long> {

    Optional<LookupData> findTopByActiveAndLookupKey(boolean active, String lookupKey);
}
