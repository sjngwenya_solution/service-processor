package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.fnb.plexus.services.request.domain.task.Tasklist;

public interface TaskListRepository extends JpaRepository<Tasklist, Long> {

    Tasklist findByWorkItemId(Long workItemId);

    Tasklist findByListTasksTaskReference(String correlationId);
}
