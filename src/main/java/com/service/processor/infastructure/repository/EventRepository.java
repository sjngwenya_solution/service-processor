package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.EventStatus;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Stream;

public interface EventRepository extends JpaRepository<DomainEvent, Long> {

    Stream<DomainEvent> findByWorkItemId(Long workItemId);

    Stream<DomainEvent> findByClassNameAndStatus(String className, EventStatus status);

    Stream<DomainEvent> findByStatusAndCreateDateBefore(EventStatus status, Timestamp createDate);

    @Query("from DomainEvent where status = :status and retryCount >= :retryCount ")
    Stream<DomainEvent> findByStatusAndRetryCountGreaterThan(@Param("status") EventStatus status,
                                                             @Param("retryCount") Integer retryCount);

    @Query("from DomainEvent where workItemId = :workItemId and className = :className and status in (:statuses) ")
    Stream<DomainEvent> findByWorkItemIdAndClassNameAndStatusIn(@Param("workItemId") Long workItemId,
                                                                @Param("className") String className,
                                                                @Param("statuses") List<EventStatus> statuses);
}
