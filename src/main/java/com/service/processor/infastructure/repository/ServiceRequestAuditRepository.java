package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import za.co.fnb.plexus.services.request.domain.request.ServiceRequestAudit;

@Repository
public interface ServiceRequestAuditRepository extends JpaRepository<ServiceRequestAudit, Long> {

    ServiceRequestAudit findDebitOrderAuditById(Long id);

    ServiceRequestAudit findDebitOrderDisputesAuditByWorkItemId(@Param("workItemId") Long workItemId);
}
