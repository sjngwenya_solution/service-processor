package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.fnb.plexus.services.request.domain.request.ServiceRequestType;

public interface ServiceRequestTypeRepository extends JpaRepository<ServiceRequestType, Long> {
    ServiceRequestType findByCode(String code);
}
