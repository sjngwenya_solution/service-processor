package za.co.fnb.plexus.services.request.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.fnb.plexus.services.request.domain.task.BranchLookUp;

import java.util.Optional;

public interface BranchLookUpRepository extends JpaRepository<BranchLookUp, Long> {

    BranchLookUp findByCode(String code);
    Optional<BranchLookUp> findByHoganBranchCode(String hoganBranchCode);
}
