package com.service.processor.infastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AfterDebitOrderAuditRepository extends JpaRepository<AfterDebitOrderAudit, Long> {

    AfterDebitOrderAudit findAfterDebitOrderAuditByWorkItemId(@Param("workItemId") Long workItemId); //add to tests
}
