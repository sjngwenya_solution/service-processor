package za.co.fnb.plexus.services.request.infrastructure;

import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.client.RestTemplate;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;
import za.co.fnb.plexus.services.request.infrastructure.repository.ExtendedRequestRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import javax.jms.ConnectionFactory;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

import static javax.jms.Session.SESSION_TRANSACTED;

/**
 * Created by lunga on 2018/11/13.
 */
@Configuration
@ComponentScan({"za.co.fnb.plexus.services.request.infrastructure.*"})
@EnableAutoConfiguration
@EnableCircuitBreaker
public class IntegrationConfig {

    @Bean(name = "restTemplate")
    @Primary
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }

    @LoadBalanced
    @Bean(name = "loadBalancedRestTemplate")
    public RestTemplate loadBalancedRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }

    @Primary
    @Bean(name = "nonTxTemplate")
    public JmsMessagingTemplate nonTxTemplate(ConnectionFactory connectionFactory) {
        return new JmsMessagingTemplate(new JmsTemplate(connectionFactory));
    }

    @Bean(name = "txTemplate")
    public JmsMessagingTemplate txTemplate(ConnectionFactory connectionFactory) {
        JmsTemplate template = new JmsTemplate(connectionFactory);
        template.setSessionTransacted(true);
        template.setSessionAcknowledgeMode(SESSION_TRANSACTED);
        return new JmsMessagingTemplate(template);
    }

    @Bean
    public ExtendedRequestRepository repositoryExtended(final RequestRepository requestRepository) {
        return new ExtendedRequestRepository() {
            @Override
            public void updateRequestMetaData(Long workItemId, String descriptionKey, String description, String statusDate) {
                Request request = requestRepository.findByWorkItemId(workItemId);
                request.updateMetaData(statusDate, new Date().toString());
                request.updateMetaData(descriptionKey, description);
                requestRepository.save(request);
            }

            @Override
            public void updateRequestMetaData(Long workItemId, Set<RequestMetadata> requestMetadata, String statusDate) {
                Optional.ofNullable(requestRepository.findByWorkItemId(workItemId)).ifPresent(request -> {
                    requestMetadata.forEach(metadata -> {
                        request.updateMetaData(metadata.getName(), metadata.getValue());
                    });
                    request.updateMetaData(statusDate, new Date().toString());
                    requestRepository.save(request);
                });
            }

            @Override
            public void updateRequestMetaData(Long workItemId, String metadataKey, String value) {
                Request request = requestRepository.findByWorkItemId(workItemId);
                request.updateMetaData(metadataKey, value);
                requestRepository.save(request);
            }
        };
    }
}