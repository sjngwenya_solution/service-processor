package za.co.fnb.plexus.services.request.infrastructure.mapper;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import za.co.fnb.cbs.common.business.shared.dto.WorkItemDto;
import za.co.fnb.plexus.services.base.dto.domain.party.CustomerDTO;
import za.co.fnb.plexus.services.request.domain.party.Customer;
import za.co.fnb.plexus.services.request.domain.party.Organisation;
import za.co.fnb.plexus.services.request.domain.party.Person;
import za.co.fnb.plexus.services.request.dto.workflow.FetchedWorkItem;

import java.lang.reflect.Type;

@Component
public class TypeMapper implements InitializingBean {

    private ModelMapper modelMapper;

    public <Destination, Source> Destination convert(Source source, Class<Destination> destination) {
        return modelMapper.map(source, destination);
    }

    public <DestinationType>  DestinationType map(Object input, Type destinationType) {
        return modelMapper.map(input, destinationType);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        modelMapper = new ModelMapper();
        //FetchedWorkItem to WorkItemDto
        TypeMap<FetchedWorkItem, WorkItemDto> typeMap = modelMapper.typeMap(FetchedWorkItem.class, WorkItemDto.class);
        typeMap.setProvider( provisionRequest -> {
            FetchedWorkItem source = FetchedWorkItem.class.cast(provisionRequest.getSource());
            return new WorkItemDto(source.getId(), source.getWorkItemTypeName(), null);
        });

        typeMap.addMappings(mapper -> {
            mapper.map(FetchedWorkItem::getWorkItemStateName, WorkItemDto::setIndicator);
            mapper.map(FetchedWorkItem::getQueueName, WorkItemDto::setCurrentQueueName);
            mapper.map(FetchedWorkItem::getCurrentStatusName, WorkItemDto::setStatus);
            mapper.map(FetchedWorkItem::getAllocatedToUsername, WorkItemDto::setAssignee);
            mapper.map(FetchedWorkItem::getAllocatedToUserDescription, WorkItemDto::setAssigneeDescription);
            mapper.map(FetchedWorkItem::getLastActivityByUsername, WorkItemDto::setLastActivityUsername);
        });

        modelMapper.typeMap(Customer.class, CustomerDTO.class).addMappings(mapper -> {
            mapper.when(Conditions.isType(Person.class)).map(Customer::person, CustomerDTO::setPerson);
            mapper.when(Conditions.isType(Organisation.class)).map(Customer::organisation, CustomerDTO::setOrganisation);
        });
    }
}
