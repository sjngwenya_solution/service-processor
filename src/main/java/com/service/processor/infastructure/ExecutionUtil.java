package za.co.fnb.plexus.services.request.infrastructure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.BiFunction;

@Component
public class ExecutionUtil {

    @Autowired
    private ApplicationContext context;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public <Input, Response> Response executeInNew(BiFunction<ApplicationContext, Input, Response> executable, Input input) {
        return executable.apply(context, input);
    }
}
