package za.co.fnb.plexus.services.request.infrastructure;

import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;

@Component
public class TransactionUtil {

    @Resource
    private PlatformTransactionManager txManager;

    public <T> void executeInTransaction(TransactionCallback<T> action) {
        newTransaction(TransactionDefinition.PROPAGATION_REQUIRES_NEW)
                .execute(action);
    }

    public <T> void executeInTransaction(int propagationBehavior, TransactionCallback<T> action) {
        newTransaction(propagationBehavior)
                .execute(action);
    }

    private TransactionTemplate newTransaction(int propagationBehavior) {
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.setPropagationBehavior(propagationBehavior);
        return txTemplate;
    }
}
