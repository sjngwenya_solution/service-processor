package com.service.processor.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "book")
public class Book {
    private static final String SK_SEQUENCE = "seq_book_id";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SK_SEQUENCE)
    @SequenceGenerator(name = SK_SEQUENCE, sequenceName = SK_SEQUENCE, allocationSize = 1)
    @Column
    private long id;
    private String title;
    private double price;
    private  int quantity;
    private String type;
}
