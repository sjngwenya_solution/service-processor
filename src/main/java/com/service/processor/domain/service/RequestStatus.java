package com.service.processor.domain.service;

import java.util.*;


public enum RequestStatus {

    OPEN("Open"),
    FAILED("Failed"),
    PENDING("Pending"),
    COMPLETED("Completed"),
    DUPLICATE("Duplicate"),
    CANCELLED("Cancelled"),
    SUCCESSFUL("Successful"),
    UNSUCCESSFUL("Unsuccessful"),
    FAILED_VALIDATION("Failed Validation");

    private static final Map<String, RequestStatus> lookup = new HashMap<String, RequestStatus>();
    static {
        for (RequestStatus requestStatus : EnumSet.allOf(RequestStatus.class)) {
            lookup.put(requestStatus.getCode(), requestStatus);
        }
    }

    private String code;

    RequestStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static List<RequestStatus> openRequestStatuses() {
        return Arrays.asList(RequestStatus.OPEN, RequestStatus.PENDING);
    }
}
