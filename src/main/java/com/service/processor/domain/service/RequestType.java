package com.service.processor.domain.service;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum RequestType {
    BS("Book Store");
    private static final Map<String, RequestType> lookup = new HashMap<String, RequestType>();
    static {
        for (RequestType requestType : EnumSet.allOf(RequestType.class)) {
            lookup.put(requestType.getDescription(), requestType);
        }
    }

    private String description;

    RequestType(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public static RequestType getRequestType(String description) {
        return lookup.get(description);
    }
}
