package com.service.processor.domain.service;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "service_type")
public class ServiceType extends IdentifiableValueObject  {
    @Column
    private Boolean active;
    protected ServiceType() {}
    public ServiceType(String code, String description) {
        super(code, description);
    }
    public Boolean getActive() {
        return active;
    }
}
