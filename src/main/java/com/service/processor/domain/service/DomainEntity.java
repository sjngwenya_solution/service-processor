package com.service.processor.domain.service;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PreUpdate;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public abstract class DomainEntity implements Serializable {

    @Version
    @Column
    protected Integer version;

    @Column(name = "created_on")
    protected Date createdOn = new Date();

    @Column(name = "last_Update_on")
    protected Date updatedOn = new Date();

    @Column(name = "created_by")
    protected String createdBy;

    public abstract Long getId();

    public Integer getVersion() {
        return version;
    }

    public boolean hasId() {
        return getId() != null;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    @PreUpdate
    public void updated() {
        updatedOn = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DomainEntity that = (DomainEntity) o;

        return getId() != null ? getId().equals(that.getId()) : that.getId() == null;
    }

    @Override
    public int hashCode() {
        return createdOn != null ? createdOn.hashCode() : 0;
    }

    public String getCreatedBy() {
        return createdBy;
    }
}
