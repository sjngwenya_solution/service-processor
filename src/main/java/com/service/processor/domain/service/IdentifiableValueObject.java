package com.service.processor.domain.service;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@MappedSuperclass
@Inheritance(strategy= InheritanceType.TABLE_PER_CLASS)
public abstract class IdentifiableValueObject implements Serializable {

    private static final String PK_SEQUENCE = "seq_identifiable_value_object_id";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PK_SEQUENCE)
    @SequenceGenerator(name = PK_SEQUENCE, sequenceName = PK_SEQUENCE, allocationSize = 1)
    @Column
    protected Long id;

    @Column
    protected String code;

    @Column
    protected String description;

    protected IdentifiableValueObject() {}

    public IdentifiableValueObject(String code, String description) {
        this.code = code;
        this.description = description;
    }

}
