package com.service.processor.domain.service;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "request_metadata")
public  class RequestMetadata implements Serializable {

    private static final String PK_SEQUENCE = "seq_request_metadata_id";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PK_SEQUENCE)
    @SequenceGenerator(name = PK_SEQUENCE, sequenceName = PK_SEQUENCE, allocationSize = 1)
    @Column
    protected Long id;

    @Column
    protected String name;

    @Column
    protected String value;

    protected BigInteger request_id;

    protected RequestMetadata() {}

    public RequestMetadata(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

}
