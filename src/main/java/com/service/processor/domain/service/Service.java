package com.service.processor.domain.service;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@Table(name = "service",
        indexes = { @Index(name = "reference_number_idx", columnList = "reference_number"),
                @Index(name = "external_reference_number_idx", columnList = "external_reference_number")})
@NoArgsConstructor
public class Service extends AggregateRoot {
    private static final String PK_SEQUENCE = "seq_service_id";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PK_SEQUENCE)
    @SequenceGenerator(name = PK_SEQUENCE, sequenceName = PK_SEQUENCE, allocationSize = 1)
    @Column
    protected Long id;

    @Column(name = "reference_number")
    protected String referenceNumber;

    @Column(name = "external_reference_number")
    protected String externalReferenceNumber;

    @Column
    private String description;

    @Enumerated(EnumType.STRING)
    private RequestStatus status = RequestStatus.OPEN;

    public Service( String externalReferenceNumber) {
        this.externalReferenceNumber = externalReferenceNumber;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "service_type_id")
    private ServiceType serviceType;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id")
    protected Set<RequestMetadata> requestMetadata = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

    @PrePersist
    public void generateReference() {
        String prefix = "SERVICE";
        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .withinRange('0', 'z')
                .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                .build();
        referenceNumber = prefix + "_" + generator.generate(10);
    }

    public void addMetaData(String name, String value) {
        requestMetadata.add(new RequestMetadata(name, value));
    }

    public void updateMetaData(String name, String value) {
        if(requestMetadata == null) {
            requestMetadata = new HashSet<>();
        }
        else {
            metaDataByName(name).ifPresent(metadata -> {
                requestMetadata.remove(metadata);
            });
        }
        addMetaData(name, value);
    }
    public Optional<RequestMetadata> metaDataByName(String name) {
        Optional<RequestMetadata> optionalRequestMetadata = requestMetadata.parallelStream().filter(metaData -> StringUtils.equalsIgnoreCase(metaData.getName(), name)).findAny();

        if (optionalRequestMetadata.isPresent()) return optionalRequestMetadata;
        return Optional.empty();
    }
    public String metaDataValueByName(String name) {
        Optional<RequestMetadata> optionalRequestMetadata = requestMetadata.parallelStream().filter(metaData -> StringUtils.equalsIgnoreCase(metaData.getName(), name)).findAny();

        if (optionalRequestMetadata.isPresent()) return optionalRequestMetadata.get().getValue();
        return null;
    }

}

