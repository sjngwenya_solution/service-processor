package com.service.processor.logic.service;


import com.service.processor.logic.service.io.IncomingRequest;


public interface RequestService {
      void processRequest(IncomingRequest incomingRequest);
}
