package com.service.processor.logic.service.impl;


import com.service.processor.logic.factory.RequestTemplateFactory;
import com.service.processor.logic.service.RequestService;
import com.service.processor.logic.service.io.IncomingRequest;
import com.service.processor.logic.template.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;




@Service
@RefreshScope
public class RequestServiceBean implements RequestService {

    @Autowired
    private RequestTemplateFactory requestTemplateFactory;

    @Override
    @Transactional
    public void processRequest(IncomingRequest request) {
        System.out.println("***************** getPayload "+request.getPayload());
        System.out.println("***************** getRequestType "+request.getRequestType());
        RequestTemplate requestTemplate = requestTemplateFactory.getTemplateByRequestType(request.getRequestType());
        requestTemplate.process(request);
    }
}
