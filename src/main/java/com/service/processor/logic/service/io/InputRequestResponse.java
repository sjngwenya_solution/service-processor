package com.service.processor.logic.service.io;



import com.service.processor.logic.template.RequestTemplate;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Data

public abstract class InputRequestResponse<T> {
    @NonNull
    private  T payload;
    @NonNull
    private RequestTemplate.REQUEST_TYPE requestType;

    protected Map<String, String> parameters = new HashMap<>();

    public void addParam(String key, String value){
        parameters.put(key,value);
    }

    public String getParam(String key){
        return  parameters.getOrDefault(key,"");
    }

}

