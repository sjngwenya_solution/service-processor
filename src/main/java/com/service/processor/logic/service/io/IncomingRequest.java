package com.service.processor.logic.service.io;


import com.service.processor.logic.template.RequestTemplate;
import lombok.Data;

@Data
public class IncomingRequest<T> extends InputRequestResponse {

    public IncomingRequest(Object payload, RequestTemplate.REQUEST_TYPE requestType) {
        super(payload, requestType);
    }
}
