package za.co.fnb.plexus.services.request.businesslogic.transformation;

import java.util.Set;

public interface TypeConverter<Source, Destination> {
    Destination convert(Source source);
    Set<Destination> convert(Set<Source> source);
}
