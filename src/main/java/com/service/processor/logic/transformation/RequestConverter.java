package za.co.fnb.plexus.services.request.businesslogic.transformation;

import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestDTO;
import za.co.fnb.plexus.services.request.infrastructure.mapper.TypeMapper;

import java.lang.reflect.Type;
import java.util.Set;

@Component
public class RequestConverter implements TypeConverter<Request, RequestDTO> {

    @Autowired
    private TypeMapper mapper;

    @Override
    public RequestDTO convert(Request request) {
        if(request == null)
            return null;
        return mapper.convert(request, RequestDTO.class);
    }
    @Override
    public Set<RequestDTO> convert(Set<Request> request) {
        if(request == null)
            return null;
        return mapper.convert(request, new TypeToken<Set<RequestDTO>>(){}.getRawType());
    }

}
