package com.service.processor.logic;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAsync
@Configuration
@ComponentScan({"za.co.fnb.plexus.services.request.businesslogic.*"})
@EnableTransactionManagement
@Import({JobsConfig.class})
public class LogicConfig {
}
