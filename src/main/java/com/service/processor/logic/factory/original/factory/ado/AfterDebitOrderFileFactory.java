package za.co.fnb.plexus.services.request.businesslogic.factory.ado;

import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.dispute.AfterDebitOrderRequestDTO;

@Component
public class AfterDebitOrderFileFactory {

    public AfterDebitOrderRequestDTO afterDebitOrderFileMapper(Request request){
        AfterDebitOrderRequestDTO afterDebitOrderRequestDTO =new AfterDebitOrderRequestDTO();

        afterDebitOrderRequestDTO.setAccountNumber(request.getServiceRecipient().getAccount().getAccountNumber());
        afterDebitOrderRequestDTO.setUniqueCustomerNumber(request.getFirstCustomer().person().getUcn());
        afterDebitOrderRequestDTO.setDebitOrderDate(request.metaDataValueByName("disputedDebitOrderDate"));
        afterDebitOrderRequestDTO.setDebitOrderAmount(request.metaDataValueByName("disputedDebitOrderAmount"));
        afterDebitOrderRequestDTO.setDebitOrderReference(request.getExternalReferenceNumber());
        afterDebitOrderRequestDTO.setSequenceNumber(request.metaDataValueByName("disputeSequenceNumber"));
        afterDebitOrderRequestDTO.setProductType(request.metaDataValueByName("productType"));
        afterDebitOrderRequestDTO.setUserCode(request.metaDataValueByName("userCode"));
        afterDebitOrderRequestDTO.setCurrencyIndicator(request.metaDataValueByName("disputedDebitOrderCurrency"));
        afterDebitOrderRequestDTO.setDisputeReasonCode(request.metaDataValueByName("disputeReasonCode"));
        afterDebitOrderRequestDTO.setAfricaSubIndicator(request.metaDataValueByName("africaSubIndicator"));
        afterDebitOrderRequestDTO.setUserAbbrIndicator(request.metaDataValueByName("userAbbrIndicator"));
        afterDebitOrderRequestDTO.setAbbreviatedName(request.metaDataValueByName("abbreviatedName"));
        afterDebitOrderRequestDTO.setComments(request.getFirstComment().getComment());
        afterDebitOrderRequestDTO.setChargeCode(request.metaDataValueByName("chargeCode"));
        afterDebitOrderRequestDTO.setSourceID(request.metaDataValueByName("sourceID"));
        afterDebitOrderRequestDTO.setCustomerTypeIndicator(request.getFirstCustomer().person().getIdentityDocumentType());
        afterDebitOrderRequestDTO.setCustomerTieBreaker(request.metaDataValueByName("clientTieBreaker"));
        afterDebitOrderRequestDTO.setNumberOfTransactions(request.metaDataValueByName("numberOfTransactions"));
        afterDebitOrderRequestDTO.setKeyDate(request.metaDataValueByName("customerKeyDate"));
        afterDebitOrderRequestDTO.setCompanyName(request.metaDataValueByName("disputedCompanyName"));

        afterDebitOrderRequestDTO.setServiceInstruction(request.metaDataValueByName("serviceInstruction"));
        afterDebitOrderRequestDTO.setReasonCodeDescription(request.metaDataValueByName("disputeReasonDescription"));

        return afterDebitOrderRequestDTO;
    }
}
