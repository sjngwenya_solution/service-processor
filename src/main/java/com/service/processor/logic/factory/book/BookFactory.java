package com.service.processor.logic.factory.book;


import com.service.processor.domain.service.RequestMetadata;
import com.service.processor.domain.service.RequestType;
import com.service.processor.domain.service.Service;
import com.service.processor.dto.BookRequest;
import com.service.processor.logic.factory.RequestDomainFactory;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

import static com.service.processor.logic.utils.ApplicationConstants.*;

@Component
public class BookFactory extends RequestDomainFactory <BookRequest> {

    @Override
    public String getExternalReference(BookRequest bookRequest) {
        return bookRequest.getReference();
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.BS;
    }

    @Override
    public Set<RequestMetadata> createMetadata(BookRequest bookRequest) {
        Set<RequestMetadata> requestMetadata = new HashSet<>();
        requestMetadata.add(new RequestMetadata(TYPE, bookRequest.getType()));
        requestMetadata.add(new RequestMetadata(TITLE, bookRequest.getTitle()));
        requestMetadata.add(new RequestMetadata(PRICE, String.valueOf(bookRequest.getPrice())));
        requestMetadata.add(new RequestMetadata(QUANTITY, String.valueOf(bookRequest.getQuantity())));
        return requestMetadata;
    }

    @Override
    public void postRequestCreation(BookRequest bookRequest, Service service) {

    }
}
