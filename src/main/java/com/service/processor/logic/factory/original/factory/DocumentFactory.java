package za.co.fnb.plexus.services.request.businesslogic.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.domain.DocumentType;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.SupportingDocument;
import za.co.fnb.plexus.services.request.dto.content.DocumentSearchCriteriaDTO;
import za.co.fnb.plexus.services.request.dto.content.DocumentSearchResponseDTO;
import za.co.fnb.plexus.services.request.infrastructure.adapter.ContentServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.ValueObjectRepository;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.NOT_FOUND;

@Component
public class DocumentFactory {

    @Value("${plexus.dispute.create.work.supporting.document.code}")
    private String documentCode;

    @Autowired
    private ValueObjectRepository<DocumentType> documentTypeRepository;

    @Autowired
    private ContentServiceAdapter contentServiceAdapter;

    public void buildSupportingDocument(Request serviceRequest, String objectId, String ucn, String documentReference) {

        DocumentType documentType = documentTypeRepository.findByCode(documentCode);
        if(isNotBlank(objectId))
        {
            buildDocument(objectId, serviceRequest, documentType);

        }else{
            DocumentSearchCriteriaDTO dto = new DocumentSearchCriteriaDTO(ucn);

            List<String> refList = new ArrayList<>();
            refList.add(documentReference);
            dto.setEcmDocumentRefs(refList);

            List<DocumentSearchResponseDTO> searchResponse = contentServiceAdapter.documentSearch(dto) ;

            if(searchResponse!=null && searchResponse.size()>0){
                buildDocument(searchResponse.get(0).getDocumentID(), serviceRequest, documentType);
            }
        }
    }

    private void buildDocument(String objectId, Request serviceRequest, DocumentType documentType) {
        if(documentType==null)
            throw new ServiceException(NOT_FOUND, String.format("Unable to find document for code %s", documentCode));

        SupportingDocument supportingDocument = new SupportingDocument(documentType, objectId);
        serviceRequest.getSupportingDocuments().add(supportingDocument);
    }
}
