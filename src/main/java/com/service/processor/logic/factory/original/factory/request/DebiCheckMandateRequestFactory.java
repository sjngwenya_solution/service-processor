package za.co.fnb.plexus.services.request.businesslogic.factory.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.factory.debicheck.MandateRequestDomainFactory;
import za.co.fnb.plexus.services.request.businesslogic.validation.DebiCheckMandateRequestValidator;
import za.co.fnb.plexus.services.request.businesslogic.validation.RequestValidator;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import java.util.HashMap;

@Component
public class DebiCheckMandateRequestFactory extends RequestAbstractFactory{

    @Autowired private MandateRequestDomainFactory domainFactory;
    @Autowired private DebiCheckMandateRequestValidator validator;

    @Override
    public RequestType requestType() {
        return RequestType.DCM;
    }

    @Override
    public RequestValidator getValidator() {
        return validator;
    }

    @Override
    public ServiceRequestDomainFactory getDomainFactory() {
        return domainFactory;
    }

    @Override
    public HashMap<String, Object> createKeyFieldsFactory(Request request) {
        HashMap<String, Object> keyFields = new HashMap<>();
        keyFields.put("WIID", request.getWorkItemId());
        keyFields.put("IDENTITY_NUMBER", request.getFirstCustomer().person().getIdentityNumber());
        keyFields.put("PRODUCT_ACCOUNT", request.getServiceProvider().getAccount().getAccountNumber());
        return keyFields;
    }
}