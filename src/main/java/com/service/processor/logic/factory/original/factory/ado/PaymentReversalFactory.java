package za.co.fnb.plexus.services.request.businesslogic.factory.ado;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.businesslogic.util.common.IntegrationLayerUtil;
import za.co.fnb.plexus.services.request.domain.party.Person;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.ado.paymentreversal.*;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.math.BigDecimal;
import java.math.BigInteger;

import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;

@Component
@RefreshScope
public class PaymentReversalFactory {

    @Value("${plexus.tds.channel.id}") String channelID;
    @Value("${plexus.tds.sender.id}") String senderID;
    @Value("${plexus.ado.operating.country.code}") String operatingCountryCode;
    @Value("${plexus.ado.payment.reversal.request.requested.response.mode}") String requestedResponseMode;

    @Autowired
    private RequestRepository requestRepository;

    public PaymentReversalRequest createReversePaymentRequest(final long workItemId) {

        PaymentReversalRequest paymentReversalRequest = new PaymentReversalRequest();

        Request request = requestRepository.findByWorkItemId(workItemId);

        paymentReversalRequest.setPrSubProduct(request.metaDataValueByName("subProduct"));
        paymentReversalRequest.setPrProduct(request.metaDataValueByName("productRequested"));
        paymentReversalRequest.setPrClearingInstrument(request.metaDataValueByName("productInstrument"));

        paymentReversalRequest.setCustomerTransactionInformation(buildPaymentReversalCustomerTransactionInformation(request));
        paymentReversalRequest.getDisputeTransactionInformation().add(buildPaymentReversalDisputeTransactionInformation(request));
        paymentReversalRequest.setGroupHeader(buildPaymentReversalGroupHeader());
        paymentReversalRequest.setHeader(buildPaymentReversalEnterpriseMessageHeader());

        return paymentReversalRequest;
    }

    private EnterpriseMessageHeader buildPaymentReversalEnterpriseMessageHeader() {
        String messageId = IntegrationLayerUtil.generateMessageID();
        EnterpriseMessageHeader header = new EnterpriseMessageHeader();

        header.setSenderID(senderID);
        header.setMessageID(messageId);
        header.setTimestamp(IntegrationLayerUtil.getCurrentXMLGregorianCalendarDateTime());
        header.setCorrelationID(IntegrationLayerUtil.buildCorrelationID(senderID, messageId));

        return header;
    }

    private GroupHeader buildPaymentReversalGroupHeader() {
        GroupHeader groupHeader = new GroupHeader();

        groupHeader.setOperatingCountryCode(operatingCountryCode);
        groupHeader.setMessageId(IntegrationLayerUtil.generateMessageID());
        groupHeader.setChannelId(channelID);
        groupHeader.setRequestedResponseMode(requestedResponseMode);
        groupHeader.setDateTimeStamp(IntegrationLayerUtil.getCurrentXMLGregorianCalendarDateTimeForNEPS());

        return groupHeader;
    }

    private CustomerTransactionInformation buildPaymentReversalCustomerTransactionInformation(Request request) {
        CustomerTransactionInformation customerTransactionInformation = new CustomerTransactionInformation();

        customerTransactionInformation.setCustomerIDType(request.metaDataValueByName("custTypeInd"));
        customerTransactionInformation.setCustomerProductCode(request.metaDataValueByName("productType"));
        customerTransactionInformation.setCustomerAccountNumber(request.getServiceRecipient().getAccount().getAccountNumber());

        customerTransactionInformation.setTransactionKeyDate(new BigInteger(request.metaDataValueByName("customerKeyDate")));
        customerTransactionInformation.setTransactionTraceIndicator(new BigInteger(request.metaDataValueByName("clientTieBreaker")));
        customerTransactionInformation.setTransactionNumber(new BigInteger(request.metaDataValueByName("numberOfTransactions")));

        return customerTransactionInformation;
    }

    private DisputeTransactionInformation buildPaymentReversalDisputeTransactionInformation(Request request) {
        DisputeTransactionInformation disputeTransactionInformation = new DisputeTransactionInformation();

        disputeTransactionInformation.setDisputeCompanyName(request.metaDataValueByName("disputedCompanyName"));
        disputeTransactionInformation.setDisputeDebitOrderReference(StringUtils.truncate(request.getExternalReferenceNumber(), 23));
        disputeTransactionInformation.setDisputeDebitOrderDate(new BigInteger(request.metaDataValueByName("disputedDebitOrderDate")));
        disputeTransactionInformation.setDisputeDebitOrderCurrency(request.metaDataValueByName("disputedDebitOrderCurrency"));
        disputeTransactionInformation.setDisputeDebitOrderAmount(new BigDecimal(request.metaDataValueByName("disputedDebitOrderAmount")));
        disputeTransactionInformation.setDisputeReasonCode(request.metaDataValueByName("disputeReasonCode"));

        return disputeTransactionInformation;
    }
}
