package za.co.fnb.plexus.services.request.businesslogic.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.base.exception.ServiceExceptionType;
import za.co.fnb.plexus.services.request.businesslogic.service.io.SLAViolationWrapper.VIOLATION_TYPE;
import za.co.fnb.plexus.services.request.businesslogic.template.AutomatedDisputesSLAViolationTemplate;
import za.co.fnb.plexus.services.request.businesslogic.template.DebiCheckMandateSLAViolationTemplate;
import za.co.fnb.plexus.services.request.businesslogic.template.Post40DisputesSLAViolationTemplate;
import za.co.fnb.plexus.services.request.businesslogic.template.SLAViolationTemplate;

import static za.co.fnb.plexus.services.request.businesslogic.service.io.SLAViolationWrapper.VIOLATION_TYPE.AUTOMATED_DEBIT_ORDER_DISPUTE;
import static za.co.fnb.plexus.services.request.businesslogic.service.io.SLAViolationWrapper.VIOLATION_TYPE.DEBICHECK_MANDATE;
import static za.co.fnb.plexus.services.request.businesslogic.service.io.SLAViolationWrapper.VIOLATION_TYPE.DEBIT_ORDER_DISPUTE;

@Component
@RefreshScope
public class SLAViolationTemplateFactory {

    @Autowired Post40DisputesSLAViolationTemplate post40SLAViolationTemplate;
    @Autowired AutomatedDisputesSLAViolationTemplate automatedDisputesSLAViolationTemplate;
    @Autowired DebiCheckMandateSLAViolationTemplate debiCheckMandateSLAViolationTemplate;

    public SLAViolationTemplate getTemplate(VIOLATION_TYPE violationType) {
        //If we get more, optimise this.
        if(DEBIT_ORDER_DISPUTE == violationType) {
            return post40SLAViolationTemplate;
        }else if(AUTOMATED_DEBIT_ORDER_DISPUTE == violationType) {
            return automatedDisputesSLAViolationTemplate;
        } else if(DEBICHECK_MANDATE == violationType) {
            return debiCheckMandateSLAViolationTemplate;
        }
        throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, violationType + "not supported");
    }
}
