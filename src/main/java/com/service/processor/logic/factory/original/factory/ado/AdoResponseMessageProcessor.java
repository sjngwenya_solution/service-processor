package za.co.fnb.plexus.services.request.businesslogic.factory.ado;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.base.exception.ServiceExceptionType;
import za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado.AdoResponseCode;
import za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado.AdoResponseData;
import za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller;
import za.co.fnb.plexus.services.request.infrastructure.repository.LookupDataRepository;

import java.util.Objects;

import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.ADO_RESPONSE_CODES;

@Component
public class AdoResponseMessageProcessor implements InitializingBean {

    @Autowired
    private LookupDataRepository lookupDataRepository;

    private AdoResponseCode response;

    public OutcomeDetails processResponse(AdoResponseData responseData) {

        if("FAILURE".equals(responseData.getStatus()))
            return failureReason(responseData);

        if("REJECTED".equals(responseData.getStatus()))
            return new OutcomeDetails(OUTCOME.FAILED, responseData.getFailedEventCode());

        if("SUCCESS".equals(responseData.getStatus()))
           return new OutcomeDetails(OUTCOME.SUCCESS, responseData.getSuccessEventCode());

        throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, "Response out not determined.");
    }

    private OutcomeDetails failureReason(AdoResponseData data) {

        if(response.getServiceUnavailable().stream().anyMatch(code -> code.equalsIgnoreCase(data.getCode()))) {
            return new OutcomeDetails(OUTCOME.RETRY, data.getRetryEventCode());
        }

        if(response.getDuplicateRequest().stream().anyMatch(code -> code.equalsIgnoreCase(data.getCode()))) {
            return new OutcomeDetails(OUTCOME.SUCCESS, data.getSuccessEventCode());
        }

        //Default to failed, then override where applicable.
        return new OutcomeDetails(OUTCOME.FAILED, data.getFailedEventCode());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        lookupDataRepository.findTopByActiveAndLookupKey(true, ADO_RESPONSE_CODES).ifPresent(lookupData ->
            response = ServiceRequestMarshaller.unmarshallJsonFile(lookupData.getData(), AdoResponseCode.class)
        );

        if(Objects.isNull(response))
            throw new ServiceException(INVALID_REQUEST, ADO_RESPONSE_CODES + " lookup data not defined.");
    }

    public enum OUTCOME { SUCCESS, FAILED, RETRY }

    @Data
    @RequiredArgsConstructor
    public class OutcomeDetails{
        @NonNull private OUTCOME outcome;
        @NonNull private String eventCode;
    }
}
