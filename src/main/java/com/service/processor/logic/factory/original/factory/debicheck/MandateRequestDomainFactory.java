package za.co.fnb.plexus.services.request.businesslogic.factory.debicheck;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.factory.request.ServiceRequestDomainFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.BranchLookUpService;
import za.co.fnb.plexus.services.request.businesslogic.service.SupportingService;
import za.co.fnb.plexus.services.request.domain.Branch;
import za.co.fnb.plexus.services.request.domain.Channel;
import za.co.fnb.plexus.services.request.domain.party.Account;
import za.co.fnb.plexus.services.request.domain.party.Customer;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;
import za.co.fnb.plexus.services.request.domain.request.RequestType;
import za.co.fnb.plexus.services.request.domain.request.ServiceProvider;
import za.co.fnb.plexus.services.request.dto.customer.CustomerProfile;
import za.co.fnb.plexus.services.request.dto.mandate.creatework.CreditorDetails;
import za.co.fnb.plexus.services.request.dto.mandate.creatework.DebitCheckMandateRequest;
import za.co.fnb.plexus.services.request.dto.mandate.creatework.DebtorDetails;

import za.co.fnb.plexus.services.request.infrastructure.adapter.CustomerServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.CustomerRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.ServiceRequestTypeRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.ValueObjectRepository;

import java.util.*;

import static java.lang.String.valueOf;
import static java.util.Optional.ofNullable;
import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INCOMPLETE_REQUEST;
import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;
import static za.co.fnb.plexus.services.request.dto.common.ServiceRequestUtil.stripLeadingZeros;

@Component
public class MandateRequestDomainFactory extends ServiceRequestDomainFactory<DebitCheckMandateRequest> implements InitializingBean {

    @Autowired
    private SupportingService supportingService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private BranchLookUpService branchLookUpService;

    @Autowired
    private ValueObjectRepository<Channel> channelRepository;

    private MandateRequestDomainFactoryHelper requestDomainFactoryHelper;

    public MandateRequestDomainFactory(CustomerServiceAdapter customerServiceAdapter,
                                       ServiceRequestTypeRepository typeRepository,
                                       CustomerRepository customerRepository) {
        super(customerServiceAdapter, typeRepository, customerRepository);
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.DCM;
    }

    @Override
    public Channel getChanelId(DebitCheckMandateRequest mandateRequest) {
        return channelRepository.findById(mandateRequest.getApplicationDetails().getApplicationChannel().toUpperCase())
                .orElse(null);
    }

    @Override
    public void postRequestCreation(DebitCheckMandateRequest mandateRequest, Request request) {
        //Update Metadata
        CustomerProfile customerProfile = mandateRequest.getCustomerProfile();
        ofNullable(customerProfile.getCustomer()).ifPresent(customer -> {
            request.getRequestMetadata().add(new RequestMetadata(FR_RATING, customer.getRiskIndicator()));
            request.getRequestMetadata().add(new RequestMetadata(ID_TYPE, ID_TYPE_VALUE));
            request.getRequestMetadata().add(new RequestMetadata(CUSTOMER_FUll_NAME, customer.getCustomerFullName()));
            request.getRequestMetadata().add(new RequestMetadata(CUSTOMER_TYPE, customer.getCustomerType()));
        });

        ofNullable(mandateRequest.getAccountDetails()).ifPresent(rfAccountDetailsDTO -> {
            request.getRequestMetadata().add(new RequestMetadata(COLLECTION_AMOUNT, valueOf(rfAccountDetailsDTO.getCollectionAmount())));
            request.getRequestMetadata().add(new RequestMetadata(COLLECTION_AMOUNT, valueOf(rfAccountDetailsDTO.getCollectionAmount())));
            request.getRequestMetadata().add(new RequestMetadata(CONTRACT_DATE, rfAccountDetailsDTO.getContractDate()));
            request.getRequestMetadata().add(new RequestMetadata(CONTRACT_TERM, valueOf(rfAccountDetailsDTO.getContractTerm())));
            request.getRequestMetadata().add(new RequestMetadata(REMAINING_TERM, valueOf(rfAccountDetailsDTO.getRemainingTerm())));
            request.getRequestMetadata().add(new RequestMetadata(INTEREST_RATE, valueOf(rfAccountDetailsDTO.getInterestRate())));
            request.getRequestMetadata().add(new RequestMetadata(FACILITY_BALANCE, valueOf(rfAccountDetailsDTO.getFacilityBalance())));
            request.getRequestMetadata().add(new RequestMetadata(FACILITY_AMOUNT, valueOf(rfAccountDetailsDTO.getFacilityAmount())));
            request.getRequestMetadata().add(new RequestMetadata(FACILITY_STATUS, rfAccountDetailsDTO.getFacilityStatus()));
            request.getRequestMetadata().add(new RequestMetadata(FACILITY_TYPE, rfAccountDetailsDTO.getFacilityType()));
            request.getRequestMetadata().add(new RequestMetadata(PAYMENT_ARREARS_AMOUNT,  valueOf(rfAccountDetailsDTO.getPayAreasAmount())));
            request.getRequestMetadata().add(new RequestMetadata(PAY_ARREARS_AMOUNT_SIGN,  rfAccountDetailsDTO.getPayAreasAmountSign()));
            request.getRequestMetadata().add(new RequestMetadata(COLLECTION_METHOD, rfAccountDetailsDTO.getCollectionMethod()));
            request.getRequestMetadata().add(new RequestMetadata(CURRENT_COLLECTION_DAY, rfAccountDetailsDTO.getPayDay()));
        });
    }

    @Override
    public String getExternalReference(DebitCheckMandateRequest mandateRequest) {
        return mandateRequest.getApplicationDetails().getApplicationNumber();
    }

    @Override
    public Account createClientAccount(DebitCheckMandateRequest debitCheckMandateRequest) {
        DebtorDetails debtorDetails = debitCheckMandateRequest.getDebtorDetails();
        Optional<za.co.fnb.plexus.services.request.dto.keystone.hogan.Account> cisAccount =
                findAccount(debtorDetails.getDebtorAccountNo(), debitCheckMandateRequest.getCustomerProfile());

        if(cisAccount.isPresent()) {
            return createAccount(cisAccount.get());
        }else {
            return createAccount(
                    debtorDetails.getDebtorBranch(),
                    debtorDetails.getDebtorAccountNo(),
                    debtorDetails.getDebtorAccountType(),
                    debtorDetails.getDebtorAccountType());
        }
    }

    @Override
    public Set<Customer> createCustomers(DebitCheckMandateRequest debitCheckMandateRequest) {
        //Search CIS and Product house
        if (debitCheckMandateRequest.requiresEnrichment()) {
            requestDomainFactoryHelper.enrich(debitCheckMandateRequest);
        }

        CustomerProfile customerProfile = debitCheckMandateRequest.getCustomerProfile();

        if(Objects.isNull(customerProfile))
            throw new ServiceException(INCOMPLETE_REQUEST, "Customer profile required for mandate request creation.");

        return Collections.singleton( createCustomer(customerProfile).get() );
    }

    @Override
    public Set<RequestMetadata> createMetadata(DebitCheckMandateRequest mandateRequest) {

        Set<RequestMetadata> requestMetadata = new HashSet<>();

        requestMetadata.add(new RequestMetadata(MANDATE_CONTRACT_REFERENCE_NO, mandateRequest.getApplicationDetails().getApplicationNumber()));
        requestMetadata.add(new RequestMetadata(COMPANY_ID ,mandateRequest.getApplicationDetails().getCompanyId()));
        requestMetadata.add(new RequestMetadata(SUB_PRODUCT, mandateRequest.getApplicationDetails().getSubProduct()));
        requestMetadata.add(new RequestMetadata(APPLICATION_NUMBER, mandateRequest.getApplicationDetails().getApplicationNumber()));
        requestMetadata.add(new RequestMetadata(APPLICATION_DATE_TIME,mandateRequest.getApplicationDetails().getApplicationDateTime()));
        requestMetadata.add(new RequestMetadata(APPLICATION_BRANCH, mandateRequest.getApplicationDetails().getBranch()));

        requestMetadata.add(new RequestMetadata(APPLICATION_SOURCE, mandateRequest.getApplicationDetails().getApplicationSource()));
        requestMetadata.add(new RequestMetadata(APPLICATION_TYPE, mandateRequest.getApplicationDetails().getApplicationType()));
        requestMetadata.add(new RequestMetadata(APPLICATION_CHANNEL, mandateRequest.getApplicationDetails().getApplicationChannel()));

        requestMetadata.add(new RequestMetadata(COLLECTION_DAY, mandateRequest.getCollectionDetails().getCollectionDay()));

        requestMetadata.add(new RequestMetadata(COLLECTION_CURRENCY, mandateRequest.getCollectionDetails().getCollectionCurrency()));
        requestMetadata.add(new RequestMetadata(MAX_COLLECTION_CURRENCY, mandateRequest.getCollectionDetails().getMaximumCollectionCurrency()));

        requestMetadata.add(new RequestMetadata(LINK_ACCOUNT_NUMBER, mandateRequest.getApplicationDetails().getLinkAccountNumber()));
        requestMetadata.add(new RequestMetadata(LIMIT_AMOUNT, mandateRequest.getApplicationDetails().getLimitAmount()));
        requestMetadata.add(new RequestMetadata(CREDITOR_NAME, mandateRequest.getCreditorDetails().getCreditorName()));
        requestMetadata.add(new RequestMetadata(CREDITOR_ABB_NAME, mandateRequest.getCreditorDetails().getCreditorAbbName()));

        requestMetadata.add(new RequestMetadata(CREDIT_ACC_PRODUCT_GROUP, CREDIT_ACC_PRODUCT_GROUP_VALUE));

        requestMetadata.add(new RequestMetadata(WORK_ITEM_INDICATOR, WORK_ITEM_INDICATOR_VALUE));
        requestMetadata.add(new RequestMetadata(RT_INDICATOR, RT_INDICATOR_VALUE));
        requestMetadata.add(new RequestMetadata(PRODUCT_TYPE, PRODUCT_TYPE_VALUE));
        requestMetadata.add(new RequestMetadata(REQUEST_TYPE, mandateRequest.getApplicationDetails().getApplicationType()));

        requestMetadata.add(new RequestMetadata(PRODUCT_ID,mandateRequest.getApplicationDetails().getProduct()));
        requestMetadata.add(new RequestMetadata(ACCOUNT_PRODUCT_CODE,mandateRequest.getApplicationDetails().getSubProduct()));

        requestMetadata.add(new RequestMetadata(CREDIT_ACC_SUB_PRODUCT_CODE, mandateRequest.getCreditorDetails().getCreditorAccountType()));
        requestMetadata.add(new RequestMetadata(APPLICATION_SOURCE, mandateRequest.getApplicationDetails().getApplicationSource()));

        return requestMetadata;
    }

    @Override
    public ServiceProvider createServiceProvider(DebitCheckMandateRequest mandateRequest) {

        CreditorDetails creditorDetails = mandateRequest.getCreditorDetails();
        Optional<za.co.fnb.plexus.services.request.dto.keystone.hogan.Account> cisAccount =
                findAccount(creditorDetails.getCreditorAccountNumber(), mandateRequest.getCustomerProfile());

        ServiceProvider serviceProvider = new ServiceProvider();
        if(cisAccount.isPresent()) {
            serviceProvider.setAccount( createAccount(cisAccount.get()) );
        }else {
            serviceProvider.setAccount( createAccount(
                    creditorDetails.getCreditorBranch(),
                    creditorDetails.getCreditorAccountNumber(),
                    creditorDetails.getCreditorAccountType(),
                    creditorDetails.getCreditorAbbName())
            );
        }
        return serviceProvider;
    }

    private Optional<za.co.fnb.plexus.services.request.dto.keystone.hogan.Account> findAccount(
            String accountNumber,
            CustomerProfile customerProfile ) {

        if(customerProfile.getAccounts() == null)
            return Optional.empty();

        return customerProfile.getAccounts()
                .stream()
                .filter(account -> StringUtils.equals(accountNumber, stripLeadingZeros(account.getAccountNo())))
                .findFirst();
    }

    private Account createAccount(za.co.fnb.plexus.services.request.dto.keystone.hogan.Account cisAccount) {
        Branch branch = branchLookUpService.findBranchForHoganBranch(cisAccount.getBranchCode());

        if(branch == null)
            throw new ServiceException(INVALID_REQUEST, String.format("No branch information found for hogan branch code %s",
                    cisAccount.getBranchCode()));

        return new Account(branch,
                cisAccount.getAccountProductCode(),
                stripLeadingZeros(cisAccount.getAccountNo()),
                cisAccount.getAccountShortName());
    }

    private Account createAccount(String hoganBranchCode,
                                  String accountNo,
                                  String accountType,
                                  String accountName) {

        Branch branch = branchLookUpService.findBranchForHoganBranch(hoganBranchCode);
        if(branch == null)
            throw new ServiceException(INVALID_REQUEST, String.format("No branch information found for hogan branch code %s",
                    hoganBranchCode));

        return new Account(branch, accountType, accountNo, accountName);
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        requestDomainFactoryHelper = new MandateRequestDomainFactoryHelper(supportingService, auditLogService);
    }
}