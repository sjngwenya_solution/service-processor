package za.co.fnb.plexus.services.request.businesslogic.factory.debicheck;

import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.service.SupportingService;
import za.co.fnb.plexus.services.request.dto.DebitOrderDetailsDTO;
import za.co.fnb.plexus.services.request.dto.common.Product;
import za.co.fnb.plexus.services.request.dto.customer.CustomerAndAccountSearchRequest;
import za.co.fnb.plexus.services.request.dto.customer.account.RFAccountDetailsDTO;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;
import za.co.fnb.plexus.services.request.dto.mandate.creatework.DebitCheckMandateRequest;
import za.co.fnb.plexus.services.request.dto.mandate.creatework.EnrichmentData;

import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.DebiCheckEnrichmentData;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalObjectToJson;
import static za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO.withReference;
import static za.co.fnb.plexus.services.request.dto.mandate.creatework.EnrichmentData.create;

public class MandateRequestDomainFactoryHelper {

    private SupportingService supportingService;
    private AuditLogService auditLogService;

    public MandateRequestDomainFactoryHelper(SupportingService supportingService, AuditLogService auditLogService) {
        this.supportingService = supportingService;
        this.auditLogService = auditLogService;
    }

    public void enrich(DebitCheckMandateRequest mandateRequest) {

        CustomerAndAccountSearchRequest searchRequest = new CustomerAndAccountSearchRequest();
        searchRequest.setProduct(Product.RF);
        searchRequest.setUcn(Long.parseLong(mandateRequest.getDebtorDetails().getCustomerNumber()));
        searchRequest.setAccountNumber(mandateRequest.getCreditorDetails().getCreditorAccountNumber());

        DebitOrderDetailsDTO oDetailsDTO = supportingService.searchProfileAndAccount(searchRequest);
        mandateRequest.setEnrichmentData(create(oDetailsDTO.getProfile(), (RFAccountDetailsDTO) oDetailsDTO.getAccountDetailsDTO()));

        log(mandateRequest.getApplicationDetails().getApplicationNumber(), mandateRequest.getEnrichmentData());
    }

    private void log(String reff, EnrichmentData enrichmentData) {
        auditLogService.log( withReference(marshalObjectToJson(enrichmentData), DebiCheckEnrichmentData.getAuditLogType(),
                RequestType.DCM, null, reff));
    }
}
