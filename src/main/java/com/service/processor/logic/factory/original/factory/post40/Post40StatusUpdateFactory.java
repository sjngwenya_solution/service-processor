package za.co.fnb.plexus.services.request.businesslogic.factory.post40;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import za.co.fnb.cbs.common.business.shared.dto.WorkItemDto;
import za.co.fnb.plexus.services.request.businesslogic.util.common.QueueName;
import za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Disputes;
import za.co.fnb.plexus.services.request.businesslogic.util.common.IntegrationLayerUtil;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.dispute.enterprisemessage.v2.EnterpriseMessageHeader;
import za.co.fnb.plexus.services.request.dto.dispute.statusupdate.v1.GroupHeader;
import za.co.fnb.plexus.services.request.dto.dispute.statusupdate.v1.StatusUpdateRequest;
import za.co.fnb.plexus.services.request.dto.dispute.statusupdate.v1.StatusUpdateRequestDTO;
import za.co.fnb.plexus.services.request.dto.dispute.statusupdate.v1.StatusUpdateRequestInformation;
import za.co.fnb.plexus.services.request.infrastructure.adapter.PlexusWorkflowAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.math.BigInteger;

@Component
@RefreshScope
public class Post40StatusUpdateFactory {

    @Value("${plexus.tds.sender.id}") String senderID;
    @Value("${plexus.tds.channel.id}") String channelID;
    @Value("${plexus.debit.order.dispute.operating.country.code}") String countryCode;

    @Value("${plexus.dispute.status.update.request.sub.product}") String subProduct;
    @Value("${plexus.dispute.status.update.request.clearing.instrument}") String clearingInstrument;

    @Value("${plexus.dispute.create.work.response.status}") String disputeLogged;
    @Value("${plexus.debit.order.dispute.mandate.requested}") String mandateRequested;
    @Value("${plexus.debit.order.dispute.user.feedback}") String feedback;
    @Value("${plexus.debit.order.dispute.unsuccessful}") String unsuccessful;
    @Value("${plexus.debit.order.dispute.successful}") String successful;

    @Value("${plexus.debit.order.dispute.queue.disp.new}") String dispNew;
    @Value("${plexus.debit.order.dispute.queue.disp.requested}") String dispRequested;
    @Value("${plexus.debit.order.dispute.queue.disp.in.progress}") String dispInProgress;
    @Value("${plexus.debit.order.dispute.queue.disp.sla.violated}") String dispSLAViolated;
    @Value("${plexus.debit.order.dispute.queue.disp.complete}") String dispComplete;
    @Value("${plexus.dispute.status.update.request.requested.response.mode}") String requestedResponseMode;

    private static final String ID_TYPE = "A";

    @Autowired RequestRepository requestRepository;

    @Autowired PlexusWorkflowAdapter plexusWorkflowAdapter;

    public StatusUpdateRequestDTO generateStatusUpdateRequest(final Long workItemId, final String previousQueue,
                                                              final WorkItemDto workItem) {

        Request request = requestRepository.findByWorkItemId(workItemId);

        StatusUpdateRequestDTO statusUpdateRequestDTO = new StatusUpdateRequestDTO();
        StatusUpdateRequest statusUpdateRequest = new StatusUpdateRequest();

        //Status Update Request Information
        StatusUpdateRequestInformation statusUpdateRequestInformation = new StatusUpdateRequestInformation();
        statusUpdateRequestInformation.setSubProduct(subProduct);
        statusUpdateRequestInformation.setClearingInstrument(clearingInstrument);

        //Customer Information
        StatusUpdateRequestInformation.CustomerInformation customerInformation = new StatusUpdateRequestInformation.CustomerInformation();
        //customerInformation.setCustomerIDType(request.getFirstCustomer().person().getIdentityDocumentType());
        customerInformation.setCustomerIDType(ID_TYPE);
        customerInformation.setCustomerProductCode(request.metaDataValueByName("customerProductCode"));
        customerInformation.setCustomerAccountNumber(request.getServiceRecipient().getAccount().getAccountNumber());
        customerInformation.setCustomerKeyDate(new BigInteger(request.metaDataValueByName("customerKeyDate")));
        customerInformation.setClientItemTiebreaker(new BigInteger(request.metaDataValueByName("clientTieBreaker")));
        statusUpdateRequestInformation.setCustomerInformation(customerInformation);

        //Action Information
        StatusUpdateRequestInformation.ActionInformation actionInformation = new StatusUpdateRequestInformation.ActionInformation();
        actionInformation.setProcessExecuted(getProcessExecuted(workItem.getStatus()));
        actionInformation.setPlexusReferenceNumber(BigInteger.valueOf(request.getWorkItemId()));
        statusUpdateRequestInformation.setActionInformation(actionInformation);

        //Maintenance Information
        StatusUpdateRequestInformation.MaintenanceInformation maintenanceInformation = new StatusUpdateRequestInformation.MaintenanceInformation();
        maintenanceInformation.setUserLastMaintained(workItem.getLastActivityUsername());
        maintenanceInformation.setChannelLastMaintained(senderID);
        statusUpdateRequestInformation.setMaintenanceInformation(maintenanceInformation);

        //Queue Information
        StatusUpdateRequestInformation.QueueInformation queueInformation = new StatusUpdateRequestInformation.QueueInformation();
        queueInformation.setPreviousQueueName(getShortenedQueueName(previousQueue));
        queueInformation.setNewQueueName(getShortenedQueueName(workItem.getCurrentQueueName()));
        statusUpdateRequestInformation.setQueueInformation(queueInformation);

        String messageId = IntegrationLayerUtil.generateMessageID();
        String correlationID = IntegrationLayerUtil.buildCorrelationID(senderID, messageId);

        //Group HeaderInformation
        GroupHeader groupHeader = new GroupHeader();
        groupHeader.setOperatingCountryCode(countryCode);
        groupHeader.setChannelId(channelID);
        groupHeader.setMessageId(messageId);
        groupHeader.setDateTimeStamp(IntegrationLayerUtil.getCurrentXMLGregorianCalendarDateTimeForNEPS());
        groupHeader.setRequestedResponseMode(requestedResponseMode);

        //Enterprise Message Header
        EnterpriseMessageHeader header = new EnterpriseMessageHeader();
        header.setSenderID(senderID);
        header.setMessageID(request.getReferenceNumber());
        header.setTimestamp(IntegrationLayerUtil.getCurrentXMLGregorianCalendarDateTime());
        header.setCorrelationID(correlationID);

        statusUpdateRequest.setStatusUpdateRequestInfo(statusUpdateRequestInformation);
        statusUpdateRequest.setGroupHeader(groupHeader);
        statusUpdateRequest.setHeader(header);

        statusUpdateRequestDTO.setStatusUpdateRequest(statusUpdateRequest);
        statusUpdateRequestDTO.setCorrelationID(correlationID);
        return statusUpdateRequestDTO;
    }

    private String getProcessExecuted(String newStatus) {
        Disputes queueStatus = Disputes.getQueueStatusByStatus(newStatus);
        switch (queueStatus) {
            case Dispute_Received:
                return disputeLogged;
            case Awaiting_Feedback_On_US:
                return mandateRequested;
            case Awaiting_Feedback_Off_us:
                return mandateRequested;
            case Feedback_Received:
                return feedback;
            case Invalid_Mandate_Exception:
                return feedback;
            case SLA_Violated:
                return feedback;
            case Valid_Mandate_Received:
                return unsuccessful;
            case Invalid_Mandate_Refund:
                return successful;
             default: return "DEFAULT";
        }
    }

    private String getShortenedQueueName(String queueName) {
        QueueName queue = QueueName.getQueueNameByName(queueName);
        switch(queue) {
            case DISP_New:
                return dispNew;
            case On_Us_DISP_Requested:
                return dispRequested;
            case Off_Us_DISP_Requested:
                return dispRequested;
            case DISP_In_Progress:
                return dispInProgress;
            case DISP_SLA_Violated:
                return dispSLAViolated;
            case DISP_Complete:
                return dispComplete;
            default: return "DEFAULT";
        }
    }
}
