package com.service.processor.logic.factory;


import com.service.processor.logic.template.RequestTemplate;
import com.service.processor.logic.utils.ServiceException;
import com.service.processor.logic.utils.ServiceExceptionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class RequestTemplateFactory {

    @Autowired
    private List<RequestTemplate> requestTemplates;

    public RequestTemplate getTemplateByRequestType(RequestTemplate.REQUEST_TYPE requestType) {
        Optional<RequestTemplate> requestTemplate = requestTemplates.stream().filter(template ->
                template.getRequestType() == requestType).findFirst();

        if (requestTemplate.isPresent()){
            return requestTemplate.get();
        }

        throw new ServiceException(ServiceExceptionType.INVALID_REQUEST,"Template not defined for service type "+ requestType);
    }

    public RequestTemplate getRequestTemplate(String type){
        return  null;
    }
}
