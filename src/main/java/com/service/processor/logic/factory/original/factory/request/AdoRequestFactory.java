package za.co.fnb.plexus.services.request.businesslogic.factory.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.factory.ado.AdoRequestDomainFactory;
import za.co.fnb.plexus.services.request.businesslogic.validation.AdoRequestValidator;
import za.co.fnb.plexus.services.request.businesslogic.validation.RequestValidator;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import java.util.HashMap;

@Component
public class AdoRequestFactory extends RequestAbstractFactory {

    @Autowired private AdoRequestDomainFactory domainFactory;
    @Autowired private AdoRequestValidator validator;

    @Override
    public RequestType requestType() {
        return RequestType.ADO;
    }

    @Override
    public RequestValidator getValidator() {
        return validator;
    }

    @Override
    public ServiceRequestDomainFactory getDomainFactory() {
        return domainFactory;
    }

    @Override
    public HashMap<String, Object> createKeyFieldsFactory(Request request) {
        HashMap<String, Object> keyFields = new HashMap<>();
        keyFields.put("WIID", request.getWorkItemId());
        keyFields.put("IDENTITY_NUMBER", request.getFirstCustomer().person().getIdentityNumber());
        return keyFields;
    }
}
