package za.co.fnb.plexus.services.request.businesslogic.factory;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.domain.events.*;
import za.co.fnb.plexus.services.request.domain.events.ado.*;
import za.co.fnb.plexus.services.request.domain.events.mandate.*;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.UNKNOWN;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;

@Component
public class ServiceRequestEventFactory implements InitializingBean {

    private static final Map<String, List<Class<? extends DomainEvent>>> REGISTERED_EVENT_TYPES = new HashMap<>();

    public Set<DomainEvent> createEvents(String code, final Long workItemId) {

        return REGISTERED_EVENT_TYPES.getOrDefault(code, Collections.emptyList()).stream()
                .map(event -> {
                    try { return ((DomainEvent) event.newInstance()); } catch (Exception e) {
                        throw new ServiceException(UNKNOWN, e.getMessage());
                    }
                })
                .peek(event ->  event.setWorkItemId(workItemId))
                .collect(Collectors.toSet());
    }

    public void registerEventType(String code, List<Class<? extends DomainEvent>> events) {
        if(StringUtils.isEmpty(code) || events == null)
            return;

        List<Class<? extends DomainEvent>> registeredEvents = REGISTERED_EVENT_TYPES.get(code);
        if(events != null){
            registeredEvents.addAll(events);
        }

        REGISTERED_EVENT_TYPES.put(code, events);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        //Instructions
        REGISTERED_EVENT_TYPES.put(STOP_PAYMENT_INSTRUCTION, singletonList(StopPaymentRequestedDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(PAYMENT_REVERSAL_INSTRUCTION, singletonList(ReversePaymentRequestedDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(STOP_AND_REVERSE_PAYMENT_INSTRUCTION, Arrays.asList(ReversePaymentRequestedDomainEvent.class, StopPaymentRequestedDomainEvent.class));

        //Stop payment response types
        REGISTERED_EVENT_TYPES.put(STOP_PAYMENT_RESPONSE_SUCCESSFUL, singletonList(PaymentStoppedDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(STOP_PAYMENT_RESPONSE_FAILED, singletonList(StopPaymentFailedDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(STOP_PAYMENT_RESPONSE_RESCHEDULE, singletonList(StopPaymentRescheduledDomainEvent.class));

        //Reverse payment response types
        REGISTERED_EVENT_TYPES.put(PAYMENT_REVERSAL_RESPONSE_SUCCESSFUL, singletonList(PaymentReversedDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(PAYMENT_REVERSAL_RESPONSE_FAILED, singletonList(ReversePaymentFailedDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(PAYMENT_REVERSAL_RESPONSE_RESCHEDULE, singletonList(ReversePaymentRescheduledEvent.class));

        //Mandate response types
        REGISTERED_EVENT_TYPES.put(MANDATE_REQUEST, singletonList(MandateReceivedDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(MANDATE_ACCEPTED, singletonList(MandateAcceptedDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(MANDATE_EXCEPTION, singletonList(MandateExceptionDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(MANDATE_EXPIRED, singletonList(MandateExpiredDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(DUPLICATE_REQUEST, singletonList(MandateDuplicateDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(MANDATE_REJECTED, singletonList(MandateRejectedDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(MANDATE_CANCELLED, singletonList(MandateCanceledDomainEvent.class));
        REGISTERED_EVENT_TYPES.put(SUBMISSION_FAILED, singletonList(MandateSubmissionFailedDomainEvent.class));
    }
}
