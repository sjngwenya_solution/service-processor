package com.service.processor.logic.factory;

import com.service.processor.domain.service.*;

import java.util.Set;

public abstract class RequestDomainFactory<InDto> {

    public Service createDomainModel(InDto inDto) {
        Service service = new Service( getExternalReference(inDto));
        service.setStatus(RequestStatus.OPEN);
        ServiceType serviceType = new ServiceType(getRequestType().toString(), getRequestType().getDescription());
        service.setServiceType(serviceType);
        service.setRequestMetadata(createMetadata(inDto));
        postRequestCreation(inDto, service);
        return service;
    }
    public abstract String getExternalReference(InDto inDto);
    public abstract RequestType getRequestType();
    public abstract Set<RequestMetadata> createMetadata(InDto inDto);
    public abstract void postRequestCreation(InDto inDto, Service service);
}
