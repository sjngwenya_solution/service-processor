package za.co.fnb.plexus.services.request.businesslogic.factory.request;

import za.co.fnb.plexus.services.request.domain.request.Request;

import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import java.util.Map;

import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.*;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.CORRELATION_ID;

public class AuditLogFactory {

    public ServiceRequestAuditDTO serviceRequestLog(RequestType requestType, Request request, String requestPayload,
                                                    Map<String, String> parameters) {

        ServiceRequestAuditDTO auditDTO = new ServiceRequestAuditDTO(requestPayload, ServiceRequest.getAuditLogType(),
                requestType, request.getWorkItemId());
        auditDTO.setReference(request.getExternalReferenceNumber());
        auditDTO.setCorrelationID(parameters.get(CORRELATION_ID));

        return auditDTO;
    }

    public ServiceRequestAuditDTO workItemCreatedLog(RequestType requestType,
                                                     Request request, String requestPayload) {

        ServiceRequestAuditDTO auditDTO = new ServiceRequestAuditDTO(requestPayload, WorkItemCreated.getAuditLogType(),
                requestType, request.getWorkItemId());
        auditDTO.setReference(request.getExternalReferenceNumber());

        return auditDTO;
    }

    public ServiceRequestAuditDTO validationFailedLog(RequestType requestType,String requestPayload,
                                                      Map<String, String> parameters, String reference) {
        ServiceRequestAuditDTO auditDTO = new ServiceRequestAuditDTO(requestPayload, ServiceRequestValidation.getAuditLogType(),
                requestType);

        auditDTO.setReference(reference);
        auditDTO.setCorrelationID(parameters.get(CORRELATION_ID));
        return auditDTO;
    }
}
