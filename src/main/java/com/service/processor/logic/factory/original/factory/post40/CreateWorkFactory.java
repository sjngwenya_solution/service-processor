package za.co.fnb.plexus.services.request.businesslogic.factory.post40;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.businesslogic.util.common.IntegrationLayerUtil;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkReply;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkReply.CustomerInfomation;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkResponse;
import za.co.fnb.plexus.services.request.dto.dispute.enterprisemessage.v2.EnterpriseMessageHeader;
import za.co.fnb.plexus.services.request.dto.dispute.enterprisemessage.v2.ResultStatus;
import za.co.fnb.plexus.services.request.dto.dispute.enterprisemessage.v2.ResultType;
import za.co.fnb.plexus.services.request.dto.dispute.enterprisemessage.v2.Severity;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.math.BigInteger;

@Component
@RefreshScope
public class CreateWorkFactory {

    @Value("${plexus.dispute.create.work.response.status}") String newStatus;
    @Value("${plexus.dispute.create.work.response.reply.code}") BigInteger successReplyCode;
    @Value("${plexus.dispute.create.work.response.validation.error.reply.code}") BigInteger errorReplyCode;
    @Value("${plexus.dispute.create.work.response.validation.duplicate.error.reply.code}") BigInteger duplicateErrorReplyCode;
    @Value("${plexus.dispute.create.work.response.validation.duplicate.error.description}") String duplicateErrorDescription;
    @Value("${plexus.dispute.create.work.response.application.id}") BigInteger replyApplicationID;
    @Value("${plexus.tds.sender.id}") String senderID;

    private static final String ID_TYPE = "A";

    @Autowired RequestRepository requestRepository;

    public CreateWorkResponse createCreateWorkResponse(Long requestID, String correlationId) {
        Request request = requestRepository.findRequestEntityById(requestID);

        CreateWorkResponse createWorkResponse = new CreateWorkResponse();

        CreateWorkReply createWorkReply = buildSuccessCreateWorkReply(request);
        buildCustomerInformation(request, createWorkReply);

        createWorkResponse.setHeader(buildHeader( buildSuccessResultType(), correlationId));
        createWorkResponse.setCreateWorkReply(createWorkReply);

        return createWorkResponse;
    }

    private CreateWorkReply buildSuccessCreateWorkReply(Request request) {
        CreateWorkReply createWorkReply = new CreateWorkReply();
        createWorkReply.setReplyCode(successReplyCode);
        createWorkReply.setPlexusReferenceNumber(BigInteger.valueOf(request.getId()));
        createWorkReply.setNewStatus(newStatus);
        return createWorkReply;
    }

    private void buildCustomerInformation(Request request, CreateWorkReply createWorkReply) {
        CustomerInfomation customerInformation = new CustomerInfomation();
        //customerInformation.setCustomerIDType(request.getFirstCustomer().person().getIdentityDocumentType());
        customerInformation.setCustomerIDType(ID_TYPE);
        customerInformation.setCustomerProductCode(request.metaDataValueByName("customerProductCode"));
        customerInformation.setCustomerAccountNumber(request.getServiceRecipient().getAccount().getAccountNumber());
        customerInformation.setCustomerKeyDate(new BigInteger(request.metaDataValueByName("customerKeyDate")));
        customerInformation.setClientItemTiebreaker(new BigInteger(request.metaDataValueByName("clientTieBreaker")));
        createWorkReply.setCustomerInfomation(customerInformation);
    }

    private ResultType buildSuccessResultType() {
        ResultType resultType = new ResultType();
        resultType.setStatus(ResultStatus.SUCCESS);
        resultType.setDescription(ResultStatus.SUCCESS.value());
        return resultType;
    }

    private EnterpriseMessageHeader buildHeader( ResultType resultType, String correlationId) {
        EnterpriseMessageHeader header = new EnterpriseMessageHeader();
        header.setSenderID(senderID);
        header.setMessageID(IntegrationLayerUtil.generateMessageID());
        header.setTimestamp(IntegrationLayerUtil.getCurrentXMLGregorianCalendarDateTime());
        header.setCorrelationID(correlationId);
        header.setResult(resultType);
        return header;
    }

    public CreateWorkResponse createFailureCreateWorkResponse(Notification notification, String correlationId) {
        CreateWorkResponse createWorkResponse = new CreateWorkResponse();
        CreateWorkReply createWorkReply = new CreateWorkReply();

        notification.getErrors().stream().findFirst()
                .ifPresent(error -> createWorkReply.setErrorDescription(error.getDescription()));

        createWorkResponse.setHeader(buildHeader(buildErrorResultType(), correlationId));
        createWorkReply.setReplyCode(errorReplyCode);
        createWorkReply.setReplyApplicationId(replyApplicationID);
        createWorkResponse.setCreateWorkReply(createWorkReply);

        return createWorkResponse;
    }

    private ResultType buildErrorResultType() {
        ResultType resultType = new ResultType();
        resultType.setSeverity(Severity.ERROR);
        resultType.setStatus(ResultStatus.FAILURE);
        return resultType;
    }

    public CreateWorkResponse createDuplicateCreateWorkResponse(String correlationId) {
        CreateWorkResponse createWorkResponse = new CreateWorkResponse();
        createWorkResponse.setHeader(buildHeader(buildErrorResultType(), correlationId));
        CreateWorkReply createWorkReply = new CreateWorkReply();
        createWorkReply.setReplyCode(duplicateErrorReplyCode);
        createWorkReply.setErrorDescription(duplicateErrorDescription);
        createWorkReply.setReplyApplicationId(replyApplicationID);
        createWorkResponse.setCreateWorkReply(createWorkReply);

        return createWorkResponse;
    }
}
