package za.co.fnb.plexus.services.request.businesslogic.factory.post40;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.factory.DocumentFactory;
import za.co.fnb.plexus.services.request.businesslogic.factory.request.ServiceRequestDomainFactory;
import za.co.fnb.plexus.services.request.domain.Branch;
import za.co.fnb.plexus.services.request.domain.Channel;
import za.co.fnb.plexus.services.request.domain.PhoneNumber;
import za.co.fnb.plexus.services.request.domain.party.Account;
import za.co.fnb.plexus.services.request.domain.party.ContactDetails;
import za.co.fnb.plexus.services.request.domain.party.Customer;
import za.co.fnb.plexus.services.request.domain.party.Party;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;
import za.co.fnb.plexus.services.request.domain.request.RequestType;
import za.co.fnb.plexus.services.request.domain.request.ServiceProvider;
import za.co.fnb.plexus.services.request.dto.common.ServiceRequestUtil;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkInput.AccountInformation.DisputerInformation;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkInput.AccountInformation.ServiceProviderInformation;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkInput.CustomerEnrichInformation;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkRequest;
import za.co.fnb.plexus.services.request.infrastructure.adapter.CustomerServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.CustomerRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.ServiceRequestTypeRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.ValueObjectRepository;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static za.co.fnb.plexus.services.request.domain.party.Party.createParty;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;

@Component
public class Post40RequestDomainFactory extends ServiceRequestDomainFactory<CreateWorkRequest> {

    @Autowired
    private ValueObjectRepository<Channel> channelRepository;

    @Autowired
    private DocumentFactory documentFactory;


    public Post40RequestDomainFactory(CustomerServiceAdapter customerServiceAdapter,
                ServiceRequestTypeRepository typeRepository,
                CustomerRepository customerRepository) {
        super(customerServiceAdapter, typeRepository, customerRepository);
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.DOD;
    }

    @Override
    public Channel getChanelId(CreateWorkRequest createWorkRequest) {
        return channelRepository.findById(createWorkRequest.getCreateWorkInput().getChannelId()).orElse(null);
    }

    @Override
    public void postRequestCreation(CreateWorkRequest createWorkRequest, Request request) {
        documentFactory.buildSupportingDocument(request, createWorkRequest.getCreateWorkInput().getObjectId(),
                createWorkRequest.getCreateWorkInput().getCustomerEnrichInformation().getUniqueCustomerNumber(),
                createWorkRequest.getCreateWorkInput().getDisputedataInformation().getDisputedDebitOrderReference());
    }

    @Override
    public String getExternalReference(CreateWorkRequest createWorkRequest) {
        return ServiceRequestUtil.buildExternalReferenceNumber(
                createWorkRequest.getCreateWorkInput().getCustomerInformation().getCustomerAccountNumber(),
                createWorkRequest.getCreateWorkInput().getCustomerInformation().getCustomerKeyDate(),
                createWorkRequest.getCreateWorkInput().getCustomerInformation().getClientItemTiebreaker());
    }

    @Override
    public Account createClientAccount(CreateWorkRequest createWorkRequest) {
        DisputerInformation disputerInformation = createWorkRequest.getCreateWorkInput().getAccountInformation()
                .getDisputerInformation();

        Branch customerBranch = new Branch(disputerInformation.getDisputerBranch(), createWorkRequest
                .getCreateWorkInput().getCustomerInformation().getCustomerProductCode(), disputerInformation.getDisputerBankName());

        Account customerAccount = new Account(customerBranch, disputerInformation.getDisputerAccountType(),
                ServiceRequestUtil.stripLeadingZeros(createWorkRequest.getCreateWorkInput().getCustomerInformation()
                        .getCustomerAccountNumber()), disputerInformation.getDisputerAccountName());

        return customerAccount;
    }

    @Override
    public Set<Customer> createCustomers(CreateWorkRequest createWorkRequest) {
        CustomerEnrichInformation customerEnrichInformation = createWorkRequest.getCreateWorkInput().getCustomerEnrichInformation();
        Set<Customer> customers = new HashSet<>();

      /*Optional<Customer> optionalCustomer = createCustomer(customerEnrichInformation.getUniqueCustomerNumber());
      if(optionalCustomer.isPresent()) {
            customers.add(optionalCustomer.get());
        }else {
            Party party = createParty(customerEnrichInformation.getName(), customerEnrichInformation.getLastName(), "",
                    createWorkRequest.getCreateWorkInput().getCustomerInformation().getCustomerIDType(),
                    customerEnrichInformation.getTitle(), customerEnrichInformation.getUniqueCustomerNumber());
            party.setContactDetails(buildContactDetails(customerEnrichInformation));
            customers.add(new Customer(party));
        }*/
        Party party = createParty(customerEnrichInformation.getName(), customerEnrichInformation.getLastName(), "",
                createWorkRequest.getCreateWorkInput().getCustomerInformation().getCustomerIDType(),
                customerEnrichInformation.getTitle(), customerEnrichInformation.getUniqueCustomerNumber());

        party.setContactDetails(buildContactDetails(customerEnrichInformation));
        customers.add(new Customer(party));

        return customers;
    }

    @Override
    public Set<RequestMetadata> createMetadata(CreateWorkRequest createWorkRequest) {
        Set<RequestMetadata> requestMetadata = new HashSet<>();

        requestMetadata.add(new RequestMetadata(CLIENT_TIE_BREAKER, String.valueOf(createWorkRequest.getCreateWorkInput().getCustomerInformation().getClientItemTiebreaker())));
        requestMetadata.add(new RequestMetadata(CUSTOMER_KEY_DATE, String.valueOf(createWorkRequest.getCreateWorkInput().getCustomerInformation().getCustomerKeyDate())));
        requestMetadata.add(new RequestMetadata(CUSTOMER_PRODUCT_CODE, createWorkRequest.getCreateWorkInput().getCustomerInformation().getCustomerProductCode()));

        requestMetadata.add(new RequestMetadata(DISPUTED_COMPANY_NAME, createWorkRequest.getCreateWorkInput().getDisputedataInformation().getDisputedCompanyName()));
        requestMetadata.add(new RequestMetadata(DISPUTED_SEQUENCE_NUMBER, null));
        requestMetadata.add(new RequestMetadata(DISPUTED_DEBIT_ORDER_REFERENCE, createWorkRequest.getCreateWorkInput().getDisputedataInformation().getDisputedDebitOrderReference()));
        requestMetadata.add(new RequestMetadata(DISPUTED_DEBIT_ORDER_DATE, String.valueOf(createWorkRequest.getCreateWorkInput().getDisputedataInformation().getDisputedDebitOrderDate())));
        requestMetadata.add(new RequestMetadata(DISPUTED_DEBIT_ORDER_CURRENCY, String.valueOf(createWorkRequest.getCreateWorkInput().getDisputedataInformation().getDisputedDebitOrderCurrency())));
        requestMetadata.add(new RequestMetadata(DISPUTED_DEBIT_ORDER_AMOUNT, formatAmount(createWorkRequest.getCreateWorkInput().getDisputedataInformation().getDisputedDebitOrderAmount())));
        requestMetadata.add(new RequestMetadata(DISPUTED_REASON_CODE, createWorkRequest.getCreateWorkInput().getDisputedataInformation().getDisputeReasonCodeDescription()));
        requestMetadata.add(new RequestMetadata(CORRELATION_ID, createWorkRequest.getHeader().getCorrelationID()));

        requestMetadata.add(new RequestMetadata(DISPUTED_EMAIL_ADDRESS,
                isNotBlank(createWorkRequest.getCreateWorkInput().getDisputedataInformation().getDisputerEmailAddress())
                        ? createWorkRequest.getCreateWorkInput().getDisputedataInformation().getDisputerEmailAddress() : ""));

        if(createWorkRequest.getCreateWorkInput().getAccountInformation()
                .getServiceProviderType().equalsIgnoreCase(DISPUTE_INTERNAL_CUSTOMER_INDICATOR)){
             requestMetadata.add(new RequestMetadata(SERVICE_PROVIDER_TYPE, DISPUTE_INTERNAL_CUSTOMER));
        }else {
             requestMetadata.add(new RequestMetadata(SERVICE_PROVIDER_TYPE, DISPUTE_EXTERNAL_CUSTOMER));
        }
        return requestMetadata;
    }

    @Override
    public ServiceProvider createServiceProvider(CreateWorkRequest createWorkRequest) {

        ServiceProviderInformation serviceProviderInformation = createWorkRequest
                .getCreateWorkInput().getAccountInformation().getServiceProviderInformation();

        Branch serviceProviderBranch = new Branch(serviceProviderInformation.getServiceProviderBranch(), null, serviceProviderInformation.getServiceProviderBankName());
        Account serviceProvideAccount = new Account(serviceProviderBranch, serviceProviderInformation.getServiceProviderAccountType(),
                ServiceRequestUtil.stripLeadingZeros(serviceProviderInformation.getServiceProviderAccountNumber()),
                serviceProviderInformation.getServiceProviderAccountName());

        ServiceProvider serviceProvider = new ServiceProvider();
        serviceProvider.setAccount(serviceProvideAccount);

        return serviceProvider;
    }

    private ContactDetails buildContactDetails(CustomerEnrichInformation enrichInformation) {

        PhoneNumber mobile = null;
        final String code = "";
        if (isNotBlank(enrichInformation.getCustomerCellNumber())) {
            mobile = new PhoneNumber(code, enrichInformation.getCustomerCellNumber());
        }
        PhoneNumber work = null;
        if (isNotBlank(enrichInformation.getCustomerWorkNumber())) {
            work = new PhoneNumber(code, enrichInformation.getCustomerWorkNumber());
        }
        return new ContactDetails(work, mobile, enrichInformation.getCustomerEmail());
    }

    private String formatAmount(BigDecimal amount) {
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#0.00", formatSymbols);
        return df.format(amount);
    }
}
