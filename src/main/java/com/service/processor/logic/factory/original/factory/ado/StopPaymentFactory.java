package za.co.fnb.plexus.services.request.businesslogic.factory.ado;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.util.common.IntegrationLayerUtil;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.ado.stop.payment.*;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.math.BigDecimal;
import java.math.BigInteger;

import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.ABBREVIATED_NAME;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.USER_CODE;

@Component
@RefreshScope
public class StopPaymentFactory {

    @Value("${plexus.tds.sender.id}") String senderID;
    @Value("${plexus.tds.channel.id}") String channelID;
    @Value("${plexus.debit.order.dispute.operating.country.code}") String countryCode;
    @Value("${plexus.dispute.status.update.request.requested.response.mode}") String requestedResponseMode;

    @Autowired
    private RequestRepository requestRepository;

    public CreateStopPaymentRequest createStopPaymentRequest(final long workItemId){

        Request request = requestRepository.findByWorkItemId(workItemId);

        CreateStopPaymentRequest createStopPaymentRequest = new CreateStopPaymentRequest();

        setGroupHeader(createStopPaymentRequest);
        setEnterpriseMessageHeader(createStopPaymentRequest);
        setCustomerTransactionInformation(createStopPaymentRequest, request);
        setDisputeTransactionInformationList(createStopPaymentRequest,request);

        return createStopPaymentRequest;
    }

    private void setGroupHeader(CreateStopPaymentRequest createStopPaymentRequest){
        GroupHeader groupHeader = new GroupHeader();
        groupHeader.setOperatingCountryCode(countryCode);
        groupHeader.setMessageId(IntegrationLayerUtil.generateMessageID());
        groupHeader.setChannelId(channelID);
        groupHeader.setDateTimeStamp(IntegrationLayerUtil.getCurrentXMLGregorianCalendarDateTime());
        groupHeader.setRequestedResponseMode(requestedResponseMode);
        createStopPaymentRequest.setGroupHeader(groupHeader);
    }

    private void setEnterpriseMessageHeader(CreateStopPaymentRequest createStopPaymentRequest){
        String messageId = IntegrationLayerUtil.generateMessageID();

        EnterpriseMessageHeader header = new EnterpriseMessageHeader();
        header.setSenderID(senderID);
        header.setMessageID(messageId);
        header.setTimestamp(IntegrationLayerUtil.getCurrentXMLGregorianCalendarDateTime());
        header.setCorrelationID(IntegrationLayerUtil.buildCorrelationID(senderID, messageId));
        createStopPaymentRequest.setHeader(header);
    }

    private void setCustomerTransactionInformation(CreateStopPaymentRequest createStopPaymentRequest, Request request){
        CustomerTransactionInformation customerTransactionInformation = new CustomerTransactionInformation();
        customerTransactionInformation.setCustomerProductCode(request.metaDataValueByName("productType"));
        customerTransactionInformation.setCustomerAccountNumber(request.getServiceRecipient().getAccount().getAccountNumber());
        customerTransactionInformation.setTransactionEffectiveDate(new BigInteger(request.metaDataValueByName("disputedDebitOrderDate")));
        customerTransactionInformation.setTransactionTraceIndicator(request.metaDataValueByName("disputeSequenceNumber"));
        customerTransactionInformation.setTransactionUserIndicator(request.metaDataValueByName("userAbbrIndicator"));
        customerTransactionInformation.setTransactionUserName(request.metaDataValueByName(ABBREVIATED_NAME));
        customerTransactionInformation.setTransactionUserCode(request.metaDataValueByName(USER_CODE));
        customerTransactionInformation.setTransactionChargeCode(request.metaDataValueByName("chargeCode"));
        customerTransactionInformation.setTransactionRemarks(request.getFirstComment().getComment());
        createStopPaymentRequest.setCustomerTransactionInformation(customerTransactionInformation);
    }

    private void setDisputeTransactionInformationList(CreateStopPaymentRequest createStopPaymentRequest, Request request){
        DisputeTransactionInformation disputeTransactionInformation = new DisputeTransactionInformation();
        disputeTransactionInformation.setStopTransactionReference(request.getExternalReferenceNumber());
        disputeTransactionInformation.setTransactionAmountLow(new BigDecimal(request.metaDataValueByName("transactionAmountLow")));
        disputeTransactionInformation.setTransactionAmountHigh(new BigDecimal(request.metaDataValueByName("transactionAmountHigh")));
        disputeTransactionInformation.setStopTransactionReason(request.metaDataValueByName("disputeReasonCode"));
        disputeTransactionInformation.setStopTransactionCurrency(request.metaDataValueByName("disputedDebitOrderCurrency"));
        createStopPaymentRequest.setDisputeTransactionInformation(disputeTransactionInformation);
    }
}
