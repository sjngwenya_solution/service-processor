package za.co.fnb.plexus.services.request.businesslogic.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.businesslogic.handler.RequestHandler;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import java.util.ArrayList;
import java.util.List;

import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;

@Component
public class RequestHandlerFactory {

    @Autowired
    private List<RequestHandler> requestHandlers = new ArrayList<>();

    public RequestHandler handler(RequestType requestType) {
        RequestHandler handler = requestHandlers.stream()
                .filter(requestHandler -> requestType == requestHandler.handles())
                .findFirst().get();

        if(handler == null)
            throw new ServiceException(INVALID_REQUEST,
                    "Not service request template defined for request type  " + requestType);

        return handler;
    }
}
