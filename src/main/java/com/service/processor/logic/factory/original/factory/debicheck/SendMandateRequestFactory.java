package za.co.fnb.plexus.services.request.businesslogic.factory.debicheck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.util.common.WorkdayCalculator;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.party.Person;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.ServiceProvider;
import za.co.fnb.plexus.services.request.dto.mandate.sendrequest.CollectionDetailsDTO;
import za.co.fnb.plexus.services.request.dto.mandate.sendrequest.MandateDTO;
import za.co.fnb.plexus.services.request.dto.mandate.sendrequest.ProductDTO;
import za.co.fnb.plexus.services.request.dto.mandate.sendrequest.SourceDTO;
import za.co.fnb.plexus.services.request.dto.mandate.sendrequest.party.AccountDTO;
import za.co.fnb.plexus.services.request.dto.mandate.sendrequest.party.CreditorDTO;
import za.co.fnb.plexus.services.request.dto.mandate.sendrequest.party.DebtorDTO;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.*;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static za.co.fnb.plexus.services.request.businesslogic.util.mandate.MandateRequestType.getDCREType;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;

@Component
public class SendMandateRequestFactory {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private WorkdayCalculator workdayCalculator;

    @Value("${mandate.request.expires.days:4}")
    private Integer mandateExpireInDays;

    public MandateDTO createMandateRequestDomainFactory(DomainEvent domainEvent, String productCode) {
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());

        MandateDTO mandateDTO = new MandateDTO();
        mandateDTO.setMandateDate(new Date());
        mandateDTO.setEndDate(workdayCalculator.calculateEndDate(mandateExpireInDays));
        mandateDTO.setApplicationNumber( request.metaDataValueByName(APPLICATION_NUMBER));
        mandateDTO.setMpsIndicator(Boolean.parseBoolean(request.metaDataValueByName(MPS_INDICATOR)));
        mandateDTO.setMandateReferenceNo(request.metaDataValueByName(MANDATE_CONTRACT_REFERENCE_NO));
        mandateDTO.setMandateContractReference(request.metaDataValueByName(MANDATE_CONTRACT_REFERENCE_NO));
        mandateDTO.setApplicationType(getDCREType(request.metaDataValueByName(APPLICATION_TYPE)));
        mandateDTO.setRequestType(getDCREType(request.metaDataValueByName(APPLICATION_TYPE)));

        CollectionDetailsDTO collectionDetails = createCollectionDetailsDTO(request);
        mandateDTO.setCollectionDetails(collectionDetails);

        CreditorDTO creditor = createCreditorDTO(request);
        mandateDTO.setCreditor(creditor);

        DebtorDTO debtor = createDebtorDTO(request);
        mandateDTO.setDebtor(debtor);

        SourceDTO sourceDTO = new SourceDTO();
        sourceDTO.setChannel(ofNullable(request.getChannel()).isPresent()
                ? request.getChannel().getCode() : "HUB");
        sourceDTO.setSource(request.metaDataValueByName(APPLICATION_SOURCE));
        mandateDTO.setSource(sourceDTO);

        ProductDTO product = new ProductDTO();
        product.setType(productCode);
        mandateDTO.getProducts().add(product);

        Set<ProductDTO> products = new HashSet<>();
        products.add(product);
        mandateDTO.setProducts(products);

        return mandateDTO;
    }

    private CollectionDetailsDTO createCollectionDetailsDTO(Request request) {
        CollectionDetailsDTO collectionDetails = new CollectionDetailsDTO();
        collectionDetails.setCollectionDay(request.metaDataValueByName(COLLECTION_DAY));
        collectionDetails.setMaximumCollectionCurrency(request.metaDataValueByName(MAX_COLLECTION_CURRENCY));
        collectionDetails.setMaximumCollectionAmount(request.metaDataValueByName(LIMIT_AMOUNT)); //Overridden as per VX instruction
        collectionDetails.setInstalment(request.metaDataValueByName(COLLECTION_AMOUNT));
        return collectionDetails;
    }

    private CreditorDTO createCreditorDTO(Request request) {

        ServiceProvider serviceProvider = request.getServiceProvider();

        CreditorDTO creditor  = new CreditorDTO ();
        creditor.setAbbreviatedName(request.metaDataValueByName(CREDITOR_ABB_NAME));
        creditor.setName(request.metaDataValueByName(CREDITOR_NAME));
        creditor.setCompanyId(request.metaDataValueByName(COMPANY_ID));
        creditor.setFrRating(request.metaDataValueByName(FR_RATING)); //must be integer

        AccountDTO creditorAccount = new AccountDTO();
        creditorAccount.setAccountName(serviceProvider.getAccount().getAccountName());
        creditorAccount.setAccountNumber(serviceProvider.getAccount().getAccountNumber());
        creditorAccount.setAccountType(request.metaDataValueByName(PRODUCT_ID));
        creditorAccount.setBranchCode(serviceProvider.getAccount().getBranch().getCode());

        ProductDTO creditorProduct = new ProductDTO();
        creditorProduct.setCode(request.metaDataValueByName(CREDIT_ACC_SUB_PRODUCT_CODE));

        creditor.setAccount(creditorAccount);
        return creditor;
    }

    private DebtorDTO createDebtorDTO(Request request) {

        Person customer = (Person) request.getFirstCustomer().getParty();

        DebtorDTO debtor = new DebtorDTO();
        debtor.setAbbreviatedName(customer.getAlsoKnownAs());
        debtor.setCompanyId(request.metaDataValueByName(COMPANY_ID));
        debtor.setUcn(customer.getUcn());
        debtor.setIdNumber(customer.getIdentityNumber());
        debtor.setIdType(customer.getIdentityDocumentType());

        String customerName = isEmpty(customer.getAlsoKnownAs()) ? customer.getFullName() : customer.getAlsoKnownAs();
        debtor.setName(customerName);

        debtor.setVip(Boolean.parseBoolean(request.metaDataValueByName(CIS_VIP_INDICATOR)));
        debtor.setFrRating(request.metaDataValueByName(FR_RATING));

        ofNullable(customer.getContactDetails()).ifPresent(contactDetails -> {
            debtor.setEmail(contactDetails.getEmailAddress());
            ofNullable(contactDetails.getMobile()).ifPresent(phoneNumber -> debtor.setMobileNumber(phoneNumber.getPhoneNumberAsString()));
            ofNullable(contactDetails.getWork()).ifPresent(phoneNumber -> debtor.setPhoneNumber(phoneNumber.getPhoneNumberAsString()));
        });

        of(request.getServiceRecipient().getAccount()).ifPresent(account -> {
            AccountDTO debtorAccount = new AccountDTO();
            debtorAccount.setAccountName(account.getAccountName());
            debtorAccount.setAccountNumber(account.getAccountNumber());
            debtorAccount.setAccountType(request.metaDataValueByName(PRODUCT_ID));
            debtorAccount.setBranchCode(account.getBranch().getCode());

            ProductDTO product = new ProductDTO();
            product.setCode(request.metaDataValueByName(PRODUCT_ID));

            debtorAccount.setProduct(product);
            debtor.setAccount(debtorAccount);
        });
        return debtor;
    }
}