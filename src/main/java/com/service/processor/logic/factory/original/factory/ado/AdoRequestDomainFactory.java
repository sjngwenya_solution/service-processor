package za.co.fnb.plexus.services.request.businesslogic.factory.ado;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.businesslogic.factory.request.ServiceRequestDomainFactory;
import za.co.fnb.plexus.services.request.domain.Channel;
import za.co.fnb.plexus.services.request.domain.Comment;
import za.co.fnb.plexus.services.request.domain.party.Account;
import za.co.fnb.plexus.services.request.domain.party.Customer;

import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;
import za.co.fnb.plexus.services.request.domain.request.RequestType;
import za.co.fnb.plexus.services.request.domain.request.ServiceProvider;
import za.co.fnb.plexus.services.request.dto.dispute.AdoInputDTO;
import za.co.fnb.plexus.services.request.dto.dispute.AfterDebitOrderRequestDTO;
import za.co.fnb.plexus.services.request.infrastructure.adapter.CustomerServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.CustomerRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.ServiceRequestTypeRepository;

import java.util.*;

import static org.apache.commons.lang3.StringUtils.leftPad;
import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.NOT_FOUND;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;

@Component
public class AdoRequestDomainFactory extends ServiceRequestDomainFactory<AdoInputDTO> {

    @Autowired
    public AdoRequestDomainFactory(CustomerServiceAdapter customerServiceAdapter,
                                   ServiceRequestTypeRepository typeRepository,
                                   CustomerRepository customerRepository) {
        super(customerServiceAdapter, typeRepository, customerRepository);
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.ADO;
    }

    @Override
    public Channel getChanelId(AdoInputDTO inputDTO) {
        return null; //Add chanel in request metadata.
    }

    @Override
    public void postRequestCreation(AdoInputDTO inputDTO, Request request) {
        Comment comment = new Comment(inputDTO.getPayload().getComments());
        Set<Comment> comments = new LinkedHashSet<>();
        comments.add(comment);
        request.setComments(comments);
    }

    @Override
    public String getExternalReference(AdoInputDTO inputDTO) {
        return inputDTO.getPayload().getDebitOrderReference();
    }

    @Override
    public Set<Customer> createCustomers(AdoInputDTO adoInputDTO) {
        Optional<Customer> optionalParty = createCustomer(adoInputDTO.customerUcn());

        if(!optionalParty.isPresent())
            throw new ServiceException(NOT_FOUND, "Customer not found for ucn " + adoInputDTO.customerUcn());

        return Collections.singleton(optionalParty.get());
    }

    @Override
    public Set<RequestMetadata> createMetadata(AdoInputDTO inputDTO) {

        AfterDebitOrderRequestDTO payload = inputDTO.getPayload();

        Set<RequestMetadata> requestMetadata = new HashSet<>();
        requestMetadata.add(new RequestMetadata(FILE_NAME, inputDTO.getMetadata().get(AdoInputDTO.FILE_NAME)));
        requestMetadata.add(new RequestMetadata(CLIENT_TIE_BREAKER, payload.getCustomerTieBreaker()));
        requestMetadata.add(new RequestMetadata(CUSTOMER_KEY_DATE,payload.getKeyDate()));
        requestMetadata.add(new RequestMetadata(DISPUTED_COMPANY_NAME, payload.getCompanyName()));
        requestMetadata.add(new RequestMetadata(DISPUTED_SEQUENCE_NUMBER, payload.getSequenceNumber()));
        requestMetadata.add(new RequestMetadata(DISPUTED_DEBIT_ORDER_DATE,payload.getDebitOrderDate()));
        requestMetadata.add(new RequestMetadata(DISPUTED_DEBIT_ORDER_CURRENCY,payload.getCurrencyIndicator()));
        requestMetadata.add(new RequestMetadata(DISPUTED_DEBIT_ORDER_AMOUNT,payload.getDebitOrderAmount()));
        requestMetadata.add(new RequestMetadata(REASON_CODE_DESCRIPTION, payload.getReasonCodeDescription()));
        requestMetadata.add(new RequestMetadata(PRODUCT_TYPE,payload.getProductType()));
        requestMetadata.add(new RequestMetadata(USER_CODE,payload.getUserCode()));
        requestMetadata.add(new RequestMetadata(AFRICA_SUB_INDICATOR,payload.getAfricaSubIndicator()));
        requestMetadata.add(new RequestMetadata(ABBREVIATED_NAME, payload.getAbbreviatedName()));
        requestMetadata.add(new RequestMetadata(USER_ABBR_INDICATOR, payload.getUserAbbrIndicator()));
        requestMetadata.add(new RequestMetadata(CHARGE_CODE,payload.getChargeCode()));
        requestMetadata.add(new RequestMetadata(SOURCE_ID,payload.getSourceID()));
        requestMetadata.add(new RequestMetadata(NUMBER_OF_TRANSACTIONS, payload.getNumberOfTransactions()));
        requestMetadata.add(new RequestMetadata(SERVICE_INSTRUCTION,payload.getServiceInstruction()));
        requestMetadata.add(new RequestMetadata(TRANSACTION_AMOUNT_LOW, payload.getTransactionAmountLow()));
        requestMetadata.add(new RequestMetadata(TRANSACTION_AMOUNT_HIGH,payload.getTransactionAmountHigh()));
        requestMetadata.add(new RequestMetadata(PRODUCT_REQUESTED,payload.getProductRequested()));
        requestMetadata.add(new RequestMetadata(SUB_PRODUCT, payload.getSubProduct()));
        requestMetadata.add(new RequestMetadata(PRODUCT_INSTRUMENT, payload.getProductInstrument()));
        requestMetadata.add(new RequestMetadata(CUSTOMER_TYPE_INDICATOR, payload.getCustomerTypeIndicator()));
        requestMetadata.add(new RequestMetadata(DISPUTED_REASON_CODE,
                leftPad(payload.getDisputeReasonCode(), 4, "0")));

        if(STOP_PAYMENT_INSTRUCTION.equals(payload.getServiceInstruction()) ||
                STOP_AND_REVERSE_PAYMENT_INSTRUCTION.equals(payload.getServiceInstruction())) {

            requestMetadata.add(new RequestMetadata(STOP_PAYMENT_STATUS_DESCRIPTION, " "));
            requestMetadata.add(new RequestMetadata(STOP_PAYMENT_ACTION_DATE, " "));
        }

        if(PAYMENT_REVERSAL_INSTRUCTION.equals(payload.getServiceInstruction()) ||
                STOP_AND_REVERSE_PAYMENT_INSTRUCTION.equals(payload.getServiceInstruction())) {

            requestMetadata.add(new RequestMetadata(REVERSE_PAYMENT_STATUS_DESCRIPTION, " "));
            requestMetadata.add(new RequestMetadata(REVERSE_PAYMENT_ACTION_DATE, " "));
        }

        return requestMetadata;
    }

    @Override
    public ServiceProvider createServiceProvider(AdoInputDTO adoInputDTO) {
        return null;
    }

    @Override
    public Account createClientAccount(AdoInputDTO adoInputDTO) {
        return new Account(null, null, adoInputDTO.getPayload().getAccountNumber(), null);
    }
}
