package za.co.fnb.plexus.services.request.businesslogic.factory.request;


import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.domain.Address;
import za.co.fnb.plexus.services.request.domain.Channel;
import za.co.fnb.plexus.services.request.domain.PhoneNumber;
import za.co.fnb.plexus.services.request.domain.party.*;
import za.co.fnb.plexus.services.request.domain.request.*;
import za.co.fnb.plexus.services.request.dto.customer.CustomerProfile;

import za.co.fnb.plexus.services.request.infrastructure.adapter.CustomerServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.CustomerRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.ServiceRequestTypeRepository;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static java.lang.Long.valueOf;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;

public abstract class ServiceRequestDomainFactory<InputDto> {

    private CustomerServiceAdapter customerServiceAdapter;
    private ServiceRequestTypeRepository typeRepository;
    private CustomerRepository customerRepository;

    public ServiceRequestDomainFactory(CustomerServiceAdapter customerServiceAdapter,
                                       ServiceRequestTypeRepository typeRepository,
                                       CustomerRepository customerRepository) {
        this.customerServiceAdapter = customerServiceAdapter;
        this.customerRepository = customerRepository;
        this.typeRepository = typeRepository;
    }

    public Request createServiceRequestDomainModel(InputDto inputDto) {

        Request serviceRequest = new Request(getChanelId(inputDto), getExternalReference(inputDto));
        serviceRequest.setStatus(RequestStatus.PENDING);
        serviceRequest.setRequestMetadata(createMetadata(inputDto));
        serviceRequest.setServiceRecipient(createRecipient(inputDto));
        serviceRequest.setServiceProvider(createServiceProvider(inputDto));

        ServiceRequestType serviceRequestType = typeRepository.findByCode(getRequestType().name());

        if(Objects.isNull(serviceRequestType)) {
            throw new ServiceException(INVALID_REQUEST, "Service Request Type does not exist");
        }

        if(!serviceRequestType.getActive()) {
            throw new ServiceException(INVALID_REQUEST, "Service Request Type Inactive");
        }

        serviceRequest.setServiceRequestType(serviceRequestType);
        postRequestCreation(inputDto, serviceRequest);

        return serviceRequest;
    }

    public ServiceRecipient createRecipient(InputDto inputDto) {
        ServiceRecipient recipient = new ServiceRecipient();
        recipient.setCustomers( createCustomers(inputDto) );
        recipient.setAccount( createClientAccount(inputDto) );
        return recipient;
    }

    public abstract RequestType getRequestType();
    public abstract Channel getChanelId(InputDto inputDto);
    public abstract String getExternalReference(InputDto inputDto);
    public abstract Account createClientAccount(InputDto inputDto); //TODO : Future development. Also look up account.

    public abstract void postRequestCreation(InputDto inputDto, Request request);

    public abstract Set<Customer> createCustomers(InputDto inputDto);
    public abstract Set<RequestMetadata> createMetadata(InputDto inputDto);
    public abstract ServiceProvider createServiceProvider(InputDto inputDto);

    protected Optional<Customer> createCustomer(String ucn) {
        try {
            CustomerProfile customerProfile = customerServiceAdapter.customerSearchByUcn(valueOf(ucn));
            return createCustomer(customerProfile);
        }catch (Exception e) {
            return customerRepository.findByPartyUcn(ucn).findFirst();
        }
    }

    protected Optional<Customer> createCustomer(CustomerProfile customerProfile) {
        Optional<Customer> optionalCustomer = customerRepository.findByPartyUcn(customerProfile.getCustomer().getUcnNo())
                .findFirst();
        Customer customer = populate(optionalCustomer.orElse(new Customer()), customerProfile);
        return of(customer);
    }

    private Customer populate(Customer customer,  CustomerProfile customerProfile) {

        za.co.fnb.plexus.services.request.dto.keystone.hogan.Customer cisCustomer = ofNullable(customerProfile)
                .map(profile -> profile.getCustomer())
                .orElse(null);

        if(! isNull(cisCustomer)) {

            Person person;
            if(isNull(customer.person()))
                person = (Person) Party.createParty(cisCustomer.getFirstName(), cisCustomer.getSurname(), cisCustomer.getIdNumber(),
                        cisCustomer.getIdType(), cisCustomer.getTitle(), cisCustomer.getUcnNo());
            else {
                person = customer.person();
                person.setTitle(cisCustomer.getTitle());
                person.setSurname(cisCustomer.getSurname());
                person.setFirstName(cisCustomer.getFirstName());
                person.setIdentityNumber(cisCustomer.getIdNumber());
                person.setIdentityDocumentType(cisCustomer.getIdType());
            }

            Optional<Address> optionalAddress = ofNullable(customerProfile.getCustomer().getResidentialAddress())
                    .map(address -> new Address(address.getAddressLine1(), address.getSuburb(), address.getCity(),
                                    isNotBlank(address.getPostalCode())? valueOf(address.getPostalCode()): null,
                                    address.getCountryCode(), "Residential"));

            optionalAddress.ifPresent(updateAddress -> {
                if(nonNull(person.getPhysicalAddress())) {
                    person.getPhysicalAddress().update(updateAddress);
                }else {
                    person.getAddresses().add(updateAddress);
                }
            });

            person.setAlsoKnownAs(cisCustomer.getCustomerFullName());

            ContactDetails contactDetails = isNull(person.getContactDetails()) ? new ContactDetails() :
                    person.getContactDetails();
            createContactDetails(contactDetails, customerProfile);

            person.setContactDetails(contactDetails);
            customer.updateParty(person);
        }
        return customer;
    }

    private void createContactDetails(ContactDetails contactDetails, CustomerProfile customerProfile) {

        PhoneNumber mobile = null;
        final String code = "";
        if (isNotBlank(customerProfile.getCustomer().getCellPhoneNo())) {
            mobile = new PhoneNumber(code, customerProfile.getCustomer().getCellPhoneNo());
        }

        PhoneNumber work = null;
        if (isNotBlank(customerProfile.getCustomer().getPhoneNoPersonal())) {
            work = new PhoneNumber(code, customerProfile.getCustomer().getPhoneNoPersonal());
        }
        contactDetails.setWork(work);
        contactDetails.setMobile(mobile);
        contactDetails.setEmailAddress(customerProfile.getCustomer().getEmail());
    }
}
