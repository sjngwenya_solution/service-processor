package za.co.fnb.plexus.services.request.businesslogic.factory.request;

import za.co.fnb.plexus.services.request.businesslogic.validation.RequestValidator;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import java.util.HashMap;

public abstract class RequestAbstractFactory {

    private AuditLogFactory logFactory = new AuditLogFactory();

    public AuditLogFactory auditLogFactory() {
        return logFactory;
    }

    public abstract RequestType requestType();

    public abstract RequestValidator getValidator();

    public abstract ServiceRequestDomainFactory getDomainFactory();

    public abstract HashMap<String, Object> createKeyFieldsFactory(Request request);

}
