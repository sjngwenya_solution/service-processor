package za.co.fnb.plexus.services.request.businesslogic.factory.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.factory.post40.Post40RequestDomainFactory;
import za.co.fnb.plexus.services.request.businesslogic.validation.Post40RequestValidator;
import za.co.fnb.plexus.services.request.businesslogic.validation.RequestValidator;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import java.util.HashMap;

@Component
public class Post40RequestFactory extends RequestAbstractFactory {

    @Autowired private Post40RequestValidator validator;
    @Autowired private Post40RequestDomainFactory domainFactory;

    @Override
    public RequestType requestType() {
        return RequestType.DOD;
    }

    @Override
    public RequestValidator getValidator() {
        return validator;
    }

    @Override
    public ServiceRequestDomainFactory getDomainFactory() {
        return domainFactory;
    }

    @Override
    public HashMap<String, Object> createKeyFieldsFactory(Request request) {
        HashMap<String, Object> keyFields = new HashMap<>();
        keyFields.put("WIID", request.getWorkItemId());
        keyFields.put("CUSTOMER_IDENTIFIER", request.getFirstCustomer().person().getUcn());
        keyFields.put("SERVICE_PROVIDER_INDICATOR", request.metaDataValueByName("serviceProviderType"));
        return keyFields;
    }
}
