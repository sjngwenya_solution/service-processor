package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor;

import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;

@FunctionalInterface
public interface AlternativeFlowSupport {

    EventExecutionResponse processAlternative(DomainEvent event);
}
