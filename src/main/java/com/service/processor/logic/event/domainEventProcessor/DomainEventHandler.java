package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronization;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.base.exception.ServiceExceptionType;
import za.co.fnb.plexus.services.request.businesslogic.service.SchedulingService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.EventStatus;
import za.co.fnb.plexus.services.request.infrastructure.ExecutionUtil;
import za.co.fnb.plexus.services.request.infrastructure.repository.EventRepository;

import java.util.function.BiFunction;

import static java.util.Optional.of;
import static org.springframework.core.ResolvableType.forClassWithGenerics;
import static org.springframework.transaction.support.TransactionSynchronizationManager.isActualTransactionActive;
import static org.springframework.transaction.support.TransactionSynchronizationManager.registerSynchronization;
import static za.co.fnb.plexus.services.request.domain.events.EventStatus.SUCCESSFUL;

@Component
@RequiredArgsConstructor
public class DomainEventHandler {

    private Logger loggingClassLogger = LoggerFactory.getLogger(DomainEventHandler.class);

    @NonNull
    private ExecutionUtil executionUtil;

    @NonNull
    private EventRepository eventRepository;

    @EventListener
    public void handleEvent(final DomainEvent domainEvent) {
        //Create event in DB (NB: This needs to be part of 'this' transaction)
        final DomainEvent event = eventRepository.save(domainEvent);

        //May not execute complete event.
        if (SUCCESSFUL == domainEvent.getStatus()) return;

        if(isActualTransactionActive())
            registerSynchronization(new TransactionSynchronization() {
                //This means, only execute the event when the current transaction is committed,
                @Override
                public void afterCommit() {
                    executeEvent(event);
                }
            });
        else
            executeEvent(event);
    }

    private final BiFunction<ApplicationContext, DomainEvent, Boolean> executeProcessor = (ctx, event) -> {

        ResolvableType processorType = forClassWithGenerics(EventProcessor.class, event.getClass());
        EventProcessor processor = (EventProcessor) ctx.getBeanProvider(processorType).getObject();

        of(processor.execute(event)).ifPresent(eventExecutionResponse -> {
            if (EventExecutionResponse.SCHEDULED == eventExecutionResponse) {
                event.scheduled();
            }else if (EventExecutionResponse.RESCHEDULED == eventExecutionResponse) {
                event.rescheduled();
            }else
                event.completed();
        });

        return true;
    };

    private final BiFunction<ApplicationContext, DomainEvent, Boolean> executeAlternativeFlow = (ctx, event) -> {

        ResolvableType processorType = forClassWithGenerics(EventProcessor.class, event.getClass());
        EventProcessor processor = (EventProcessor) ctx.getBeanProvider(processorType).getObject();

        if(AlternativeFlowSupport.class.isInstance( processor )) {

            of(AlternativeFlowSupport.class.cast(processor)
                    .processAlternative(event))
                    .ifPresent(eventExecutionResponse -> {

                        if (EventExecutionResponse.SCHEDULED == eventExecutionResponse) {
                            event.scheduled();
                        }else if (EventExecutionResponse.RESCHEDULED == eventExecutionResponse) {
                            event.rescheduled();}
                        else
                            event.completed();
                    });

            return true;
        }
        throw new ServiceException(ServiceExceptionType.INCOMPLETE_REQUEST, "Processor does not support failover.");
    };


    private final BiFunction<ApplicationContext, DomainEvent, Boolean> processEvent = (context, event) -> {

        final EventRepository eventRepository = context.getBean(EventRepository.class);
        final ExecutionUtil util = context.getBean(ExecutionUtil.class);

        try {
            util.executeInNew(executeProcessor, event);
        }catch (Throwable e) {
            loggingClassLogger.error(e.getMessage(), e);

            String msg = StringUtils.substring(e.getMessage(), 0, 254);

            //Should retry or fail?
            if(event.getRetryCount() >= 3) {

                try {
                    //Try alternative flow first.
                    util.executeInNew(executeAlternativeFlow, event);
                } catch (Exception ex) { }

                //if Alternative was not successful, fail.
                if (!(SUCCESSFUL == event.getStatus() || EventStatus.SCHEDULED == event.getStatus()))
                    event.failed(msg, "Possible Null pointer exception");

            } else {
                //Retry later
                final SchedulingService schedulingService = context.getBean(SchedulingService.class);
                event.retry(msg);
                //Create schedule to fire schedule event
                schedulingService.scheduleJobNextHour(event.getClass().getSimpleName(), event.getStatus().name());
            }
        }finally {
            eventRepository.save(event);
        }
        return true;
    };

    public void executeEvent(DomainEvent event) {
        //Because the current transaction has been committed, we need to we need to start a new transaction
        //for the event updates to be pushed to the database... 'optimise' with caution !!!.
        executionUtil.executeInNew(processEvent, event);
    }
}