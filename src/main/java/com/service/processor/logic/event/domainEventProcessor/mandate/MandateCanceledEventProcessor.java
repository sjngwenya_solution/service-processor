package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateCanceledDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.infrastructure.adapter.UpdateDebitOrderDetailsAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Optional;

@Component
public class MandateCanceledEventProcessor extends IDSUpdatingProcessor<MandateCanceledDomainEvent> {


    public MandateCanceledEventProcessor(@NonNull CommandService commandService,
                                         @NonNull RequestRepository requestRepository,
                                         @NonNull UpdateDebitOrderDetailsAdapter updateDebitOrderDetailsAdapter,
                                         @NonNull EventService eventService) {

        super(commandService, requestRepository, updateDebitOrderDetailsAdapter, eventService);
    }

    @Override
    protected Optional<Integer> getNewDebitOrderDate(DomainEvent domainEvent) {
        return Optional.empty();
    }

    @Override
    protected RequestStatus getRequestStatus() {
        return RequestStatus.COMPLETED;
    }

    @Override
    protected QueueStatus.DebiCheck getNewWorkFlowStatus() {
        return QueueStatus.DebiCheck.Request_Successful;
    }
}
