package za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.postProcessor;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import za.co.fnb.cbs.common.business.shared.dto.WorkItemDto;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.EventProcessor.EventPostProcessor;
import za.co.fnb.plexus.services.request.businesslogic.factory.post40.Post40StatusUpdateFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.dispute.statusupdate.v1.StatusUpdateRequestDTO;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;
import za.co.fnb.plexus.services.request.events.WorkCreatedEventDTO;
import za.co.fnb.plexus.services.request.infrastructure.adapter.ActiveMQAdapter;
import za.co.fnb.plexus.services.request.infrastructure.adapter.PlexusWorkflowAdapter;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.StatusUpdateRequest;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Disputes.Awaiting_Feedback_Off_us;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Disputes.Awaiting_Feedback_On_US;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalWithNamespace;
import static za.co.fnb.plexus.services.request.domain.task.Action.POST40_STATUS_UPDATE;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.DISPUTE_INTERNAL_CUSTOMER;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.SERVICE_PROVIDER_TYPE;

@Component
@RequiredArgsConstructor
public class Post40WorkItemCreatedEventPostProcessor implements EventPostProcessor<WorkCreatedEventDTO> {

    @NonNull private TaskService taskService;
    @NonNull private AuditLogService logService;
    @NonNull private ActiveMQAdapter activeMQAdapter;
    @NonNull private PlexusWorkflowAdapter workflowAdapter;
    @NonNull private Post40StatusUpdateFactory statusUpdateFactory;

    @Override
    public void eventHandled(WorkCreatedEventDTO event, Request request) {
        ofNullable(workflowAdapter.getWorkItemsById(event.getWorkItemId()))
            .ifPresent(workItemDto -> {

                //Determine customer type
                boolean isInternalCustomer = equalsIgnoreCase(DISPUTE_INTERNAL_CUSTOMER,
                        request.metaDataValueByName(SERVICE_PROVIDER_TYPE));

                //Determine new status
                String newStatus = isInternalCustomer ? Awaiting_Feedback_On_US.getStatus() : Awaiting_Feedback_Off_us.getStatus();

                WorkItemDto updatedWorkItem;
                WorkItemDto currentWorkItem = workflowAdapter.getWorkItemsById(event.getWorkItemId());
                if(!StringUtils.equals(currentWorkItem.getStatus(), newStatus)) {
                    //Update status [This needs to be async]
                    updatedWorkItem = workflowAdapter.changeWorkItemStatus(request.getWorkItemId(), newStatus, newStatus);
                }else {
                    updatedWorkItem = currentWorkItem;
                }

                StatusUpdateRequestDTO statusUpdateRequestDTO = statusUpdateFactory
                        .generateStatusUpdateRequest(request.getWorkItemId(), workItemDto.getCurrentQueueName(), updatedWorkItem);

                //Open service request
                request.open();

                //Create the checklist
                taskService.createNewListWithTask(request.getWorkItemId(), statusUpdateRequestDTO.getCorrelationID(),
                        POST40_STATUS_UPDATE);

                //Create message
                String statusUpdateRequestXML = marshalWithNamespace(statusUpdateRequestDTO.getStatusUpdateRequest());
                activeMQAdapter.sendDisputeStatusUpdate(statusUpdateRequestXML, statusUpdateRequestDTO.getCorrelationID());

                logService.log(ServiceRequestAuditDTO.withReference(statusUpdateRequestXML,
                        StatusUpdateRequest.getAuditLogType(), RequestType.DOD, request.getWorkItemId(),
                        request.getExternalReferenceNumber()));
            });
    }
}
