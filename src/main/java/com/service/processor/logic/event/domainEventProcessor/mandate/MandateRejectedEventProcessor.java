package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRejectedDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestCompletedDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;

@Component
public class MandateRejectedEventProcessor extends EventProcessor<MandateRejectedDomainEvent> {

    private EventService eventService;
    public MandateRejectedEventProcessor(@NonNull CommandService commandService,
                                         @NonNull RequestRepository requestRepository,
                                         @NonNull EventService eventService) {
        super(commandService, requestRepository);
        this.eventService = eventService;
    }
    @Override
    protected EventExecutionResponse process(DomainEvent domainEvent) {
        changeWorkFlowStatus(QueueStatus.DebiCheck.Auth_Rejected.name(), domainEvent.getWorkItemId());
        changeRequestStatus(RequestStatus.FAILED, domainEvent.getWorkItemId());

        eventService.send(new MandateRequestCompletedDomainEvent(domainEvent.getWorkItemId()));

        return SUCCESSFUL;
    }
}
