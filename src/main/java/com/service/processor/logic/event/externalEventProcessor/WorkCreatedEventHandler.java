package za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.EventProcessor.EventHandler;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.EventProcessor.EventPostProcessor;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.postProcessor.AdoWorkItemCreatedEventPostProcessor;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.postProcessor.DebiCheckMandateWorkItemCreatedEventPostProcessor;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.postProcessor.Post40WorkItemCreatedEventPostProcessor;
import za.co.fnb.plexus.services.request.businesslogic.factory.request.AuditLogFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;
import za.co.fnb.plexus.services.request.events.WorkCreatedEventDTO;

import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalObjectToJson;

@Component
public class WorkCreatedEventHandler implements EventHandler<WorkCreatedEventDTO> {

    @Autowired private AuditLogService logService;

    @Autowired private DebiCheckMandateWorkItemCreatedEventPostProcessor debiCheck;
    @Autowired private Post40WorkItemCreatedEventPostProcessor post40;
    @Autowired private AdoWorkItemCreatedEventPostProcessor ado;

    private AuditLogFactory logFactory = new AuditLogFactory();

    @Override
    public Optional<EventPostProcessor> handle(WorkCreatedEventDTO event,
                                               RequestType requestType,
                                               Request request) {
        //Link request to work item.
        request.setWorkItemId(event.getWorkItemId());
        logService.log(logFactory.workItemCreatedLog(requestType, request, marshalObjectToJson(event)));

        switch (requestType) {
            case DCM: return of(debiCheck);
            case DOD: return of(post40);
            case ADO: return of(ado);
            default: return empty();
        }
    }
}
