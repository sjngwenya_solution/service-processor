package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.ado;

import lombok.NonNull;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.SynchronizeAction;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.domain.task.Tasklist;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Ado.Failed_Request;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;

public abstract class FailedExecutionProcessor<T extends DomainEvent>  extends EventProcessor implements SynchronizeAction {

    private TaskService taskService;

    public FailedExecutionProcessor(@NonNull CommandService commandService,
                                    @NonNull RequestRepository requestRepository,
                                    @NonNull TaskService taskService) {
        super(commandService, requestRepository);
        this.taskService = taskService;
    }

    @Override
    protected EventExecutionResponse process(DomainEvent requestEvent) {

        if(! taskService.executeFunction(requestEvent.getWorkItemId(), Tasklist::anyPending) ) {
            //Signal plexus
            changeWorkFlowStatus( Failed_Request.name(), requestEvent.getWorkItemId());
            changeRequestStatus(RequestStatus.FAILED, requestEvent.getWorkItemId());
        }
        return SUCCESSFUL;
    }
}
