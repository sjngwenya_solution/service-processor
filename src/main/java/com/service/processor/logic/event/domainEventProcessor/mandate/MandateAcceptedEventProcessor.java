package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.AlternativeFlowSupport;
import za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateAcceptedDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestCompletedDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.infrastructure.adapter.UpdateDebitOrderDetailsAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Optional;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INCOMPLETE_REQUEST;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.DebiCheck.Request_Successful;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.COLLECTION_DAY;

@Component
public class MandateAcceptedEventProcessor extends IDSUpdatingProcessor<MandateAcceptedDomainEvent> implements AlternativeFlowSupport {

    public MandateAcceptedEventProcessor(@NonNull CommandService commandService,
                                         @NonNull RequestRepository requestRepository,
                                         @NonNull UpdateDebitOrderDetailsAdapter updateDebitOrderDetailsAdapter,
                                         @NonNull EventService eventService) {

        super(commandService, requestRepository, updateDebitOrderDetailsAdapter, eventService);
    }

    @Override
    protected Optional<Integer> getNewDebitOrderDate(DomainEvent domainEvent) {
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());

        String newDOD = findValue(request.getRequestMetadata(), COLLECTION_DAY);

        if(isEmpty(newDOD))
            throw new ServiceException(INCOMPLETE_REQUEST, String.format("Invalid value for mandate DOD [%s]", newDOD));

        return Optional.of(Integer.parseInt(newDOD));
    }

    @Override
    protected RequestStatus getRequestStatus() {
        return RequestStatus.COMPLETED;
    }

    @Override
    protected QueueStatus.DebiCheck getNewWorkFlowStatus() {
        return Request_Successful;
    }

    @Override
    public EventExecutionResponse processAlternative(DomainEvent domainEvent) {
        changeWorkFlowStatus(QueueStatus.DebiCheck.Host_Update_Failed.name(), domainEvent.getWorkItemId());
        changeRequestStatus(RequestStatus.FAILED, domainEvent.getWorkItemId());

        eventService.send(new MandateRequestCompletedDomainEvent(domainEvent.getWorkItemId()));

        return SUCCESSFUL;
    }

    private <T> T findValue(Set<RequestMetadata> requestMetadata, final String key) {
        return (T) requestMetadata.stream()
                .filter(metadata -> key.equals(metadata.getName()))
                .findFirst().map(RequestMetadata::getValue).get();
    }
}
