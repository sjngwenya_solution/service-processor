package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.InflightMandateReceivedDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateDuplicateDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Collections;
import java.util.Set;

import static java.lang.String.valueOf;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;
import static za.co.fnb.plexus.services.request.domain.request.RequestStatus.PENDING;
import static za.co.fnb.plexus.services.request.domain.request.RequestStatus.openRequestStatuses;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.INFLIGHT_INDICATOR;

@Component
public class InflightMandateReceivedEventProcessor extends EventProcessor<InflightMandateReceivedDomainEvent> {

    private EventService eventService;
    public InflightMandateReceivedEventProcessor(@NonNull CommandService commandService,
                                                 @NonNull RequestRepository requestRepository,
                                                 @NonNull EventService eventService) {
        super(commandService, requestRepository);
        this.eventService = eventService;
    }

    @Override
    protected synchronized EventExecutionResponse process(DomainEvent domainEvent) {
        //Load 'current' current request (pending) and open it.
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());

        //Set inflight indicator.
        requestRepository.updateRequestMetaData(request.getWorkItemId(), INFLIGHT_INDICATOR,
                valueOf(requestRepository.findByExternalReferenceNumberAndStatus(request.getExternalReferenceNumber(),
                        openRequestStatuses()).iterator().next().getWorkItemId()));

        //Find duplicate requests (should be one at a time)
        Set<Request> duplicateRequests = request.filterCurrent(requestRepository.findByExternalReferenceNumberAndStatus(
                request.getExternalReferenceNumber(), Collections.singletonList(PENDING)));

        //Mark as duplicate.
        request.duplicates( duplicateRequests );

        //Update workflow
        duplicateRequests.forEach( duplicateRequest -> {
            eventService.send(new MandateDuplicateDomainEvent(duplicateRequest.getWorkItemId())); });

        return SUCCESSFUL;
    }
}
