package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.InflightMandateReceivedDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateReceivedDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestScheduledDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestedDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.infrastructure.repository.EventRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Set;
import java.util.stream.Collectors;

import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;
import static za.co.fnb.plexus.services.request.domain.events.EventStatus.openEvents;
import static za.co.fnb.plexus.services.request.domain.request.RequestStatus.openRequestStatuses;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.INFLIGHT_INDICATOR;

@Component
public class MandateReceivedEventProcessor extends EventProcessor<MandateReceivedDomainEvent> {

    private EventService eventService;
    private EventRepository eventRepository;
    public MandateReceivedEventProcessor(@NonNull CommandService commandService,
                                         @NonNull RequestRepository requestRepository,
                                         @NonNull EventService eventService,
                                         @NonNull EventRepository eventRepository) {
        super(commandService, requestRepository);
        this.eventService = eventService;
        this.eventRepository = eventRepository;
    }

    @Override
    protected EventExecutionResponse process(DomainEvent domainEvent) {

        //Get pending request
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());

        //Get open requests
        Set<Request> openRequests = requestRepository.findByExternalReferenceNumberAndStatus(
                request.getExternalReferenceNumber(), openRequestStatuses());

        DomainEvent event;
        if(openRequests.isEmpty() || isStillPendingExecution(openRequests) )
            event = new MandateRequestScheduledDomainEvent(domainEvent.getWorkItemId());
         else
            event = new InflightMandateReceivedDomainEvent(domainEvent.getWorkItemId());

        eventService.send(event);

        return SUCCESSFUL;
    }

    private boolean isStillPendingExecution(Set<Request> duplicateRequests) {
        //Find current event and disable.
        return duplicateRequests.stream()
                .map(r -> eventRepository.findByWorkItemIdAndClassNameAndStatusIn(r.getWorkItemId(),
                        MandateRequestedDomainEvent.class.getSimpleName(), openEvents())
                        .collect(Collectors.toList()))
                .flatMap(eventLists -> eventLists.stream())
                .findAny().isPresent();
    }
}
