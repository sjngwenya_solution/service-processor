package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;

import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller;
import za.co.fnb.plexus.services.request.businesslogic.handler.helper.debicheck.DebiCheckResponseCode;

import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateExceptionDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestCompletedDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.infrastructure.repository.LookupDataRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Objects;
import java.util.Set;

import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.DebiCheck.Error_in_Request;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.DebiCheck.Request_Failed;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.DEBICHECK_RESPONSE_CODES;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.RESPONSE_DESCRIPTION;

@Component
public class MandateExceptionEventProcessor extends EventProcessor<MandateExceptionDomainEvent> {

    private LookupDataRepository lookupDataRepository;
    private DebiCheckResponseCode responseCode;
    private EventService eventService;

    public MandateExceptionEventProcessor(@NonNull CommandService commandService,
                                          @NonNull RequestRepository requestRepository,
                                          @NonNull LookupDataRepository lookupDataRepository,
                                          @NonNull EventService eventService) {
        super(commandService, requestRepository);
        this.lookupDataRepository = lookupDataRepository;
        this.eventService = eventService;
    }

    @Override
    protected EventExecutionResponse process(DomainEvent domainEvent) {
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());
        String errorCode = findValue(request.getRequestMetadata(), RESPONSE_DESCRIPTION);

        lookupDataRepository.findTopByActiveAndLookupKey(true, DEBICHECK_RESPONSE_CODES).ifPresent(lookupData ->
                responseCode = ServiceRequestMarshaller.unmarshallJsonFile(lookupData.getData(), DebiCheckResponseCode.class)
        );

        if(Objects.isNull(responseCode))
            throw new ServiceException(INVALID_REQUEST, DEBICHECK_RESPONSE_CODES + " lookup data not defined.");

        String destQueue = responseCode.getResponseCodes().contains(errorCode) ? Error_in_Request.name() : Request_Failed.name();

        changeWorkFlowStatus(destQueue, domainEvent.getWorkItemId());
        changeRequestStatus(RequestStatus.FAILED, domainEvent.getWorkItemId());

        eventService.send(new MandateRequestCompletedDomainEvent(domainEvent.getWorkItemId()));

        return SUCCESSFUL;
    }

    private <T> T findValue(Set<RequestMetadata> requestMetadata, final String key) {
        return (T) requestMetadata.stream()
                .filter(metadata -> key.equals(metadata.getName()))
                .findFirst().map(RequestMetadata::getValue).get();
    }
}
