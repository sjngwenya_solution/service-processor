package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.ado;

import lombok.NonNull;
import za.co.fnb.plexus.services.request.businesslogic.command.CommandFactory;
import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.SynchronizeAction;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.RequestService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.MainframeInteractionServiceBean;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Collections;
import java.util.function.Predicate;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Ado.*;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SCHEDULED;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.SERVICE_INSTRUCTION;

public abstract class FunctionExecutionProcessor<T extends DomainEvent> extends EventProcessor implements SynchronizeAction {

    private MainframeInteractionServiceBean scheduleService;
    private RequestService requestService;
    private CommandFactory commandFactory;

    public FunctionExecutionProcessor(@NonNull CommandService commandService,
                                      @NonNull RequestRepository requestRepository,
                                      @NonNull MainframeInteractionServiceBean scheduleService,
                                      @NonNull RequestService requestService,
                                      @NonNull CommandFactory commandFactory) {
        super(commandService, requestRepository);

        this.scheduleService = scheduleService;
        this.requestService = requestService;
        this.commandFactory = commandFactory;
    }

    @Override
    protected final EventExecutionResponse process(DomainEvent event) {

        //Load service request
        String instruction = requestService.findMetadataValue(event.getWorkItemId(), SERVICE_INSTRUCTION);

        //Check if if stop/reverse is required
        if ( prerequisitePredicate().test(instruction)) {
            //Invoke MF if needs be.
            ServiceCommand command = commandFactory.create(getMainframeFunction(), event.getWorkItemId());
            EventExecutionResponse serviceResponse = scheduleService.execute(command);

            if (SUCCESSFUL == serviceResponse) {
                changeWorkFlowStatus(Service_Requested.name(), event.getWorkItemId());
            }

            if(SCHEDULED == serviceResponse) {
                changeWorkFlowStatus(Scheduled.name(), event.getWorkItemId(),
                        Collections.singleton(Retry_Scheduled.name()));
            }
            return serviceResponse;
        }
        return null;
    }

    protected abstract ServiceCommand.MainframeFunction getMainframeFunction();

    protected abstract Predicate<String> prerequisitePredicate();
}
