package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.NonNull;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestCompletedDomainEvent;
import za.co.fnb.plexus.services.request.domain.party.Account;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.dto.mandate.DebitOrderUpdateDetails;
import za.co.fnb.plexus.services.request.infrastructure.adapter.UpdateDebitOrderDetailsAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Optional;

import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.UPDATE_IDS_DATE;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.UPDATE_IDS_STATUS;

public abstract class IDSUpdatingProcessor<T extends DomainEvent> extends EventProcessor {

    private UpdateDebitOrderDetailsAdapter updateDebitOrderDetailsAdapter;
    protected EventService eventService;

    public IDSUpdatingProcessor(@NonNull CommandService commandService,
                                @NonNull RequestRepository requestRepository,
                                @NonNull UpdateDebitOrderDetailsAdapter updateDebitOrderDetailsAdapter,
                                @NonNull EventService eventService) {

        super(commandService, requestRepository);
        this.updateDebitOrderDetailsAdapter = updateDebitOrderDetailsAdapter;
        this.eventService = eventService;
    }

    @Override
    protected EventExecutionResponse process(DomainEvent domainEvent) {
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());

        getNewDebitOrderDate(domainEvent).ifPresent(newDay -> {
            Account account = request.getServiceProvider().getAccount();
            updateDebitOrderDetailsAdapter.update(new DebitOrderUpdateDetails(account.getAccountNumber(), newDay));

            requestRepository.updateRequestMetaData(domainEvent.getWorkItemId(), UPDATE_IDS_STATUS,
                    RequestStatus.COMPLETED.getCode(), UPDATE_IDS_DATE);
        });

        changeWorkFlowStatus(getNewWorkFlowStatus().name(), domainEvent.getWorkItemId());
        changeRequestStatus(getRequestStatus(), domainEvent.getWorkItemId());

        eventService.send(new MandateRequestCompletedDomainEvent(domainEvent.getWorkItemId()));

        return EventExecutionResponse.SUCCESSFUL;
    }

    protected abstract Optional<Integer> getNewDebitOrderDate(DomainEvent domainEvent);

    protected abstract RequestStatus getRequestStatus();

    protected abstract QueueStatus.DebiCheck getNewWorkFlowStatus();
}
