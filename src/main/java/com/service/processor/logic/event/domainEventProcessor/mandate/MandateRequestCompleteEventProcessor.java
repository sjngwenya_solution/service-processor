package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateReceivedDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestCompletedDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;
import static za.co.fnb.plexus.services.request.domain.request.RequestStatus.PENDING;

@Component
public class MandateRequestCompleteEventProcessor extends EventProcessor<MandateRequestCompletedDomainEvent> {

    private EventService eventService;
    public MandateRequestCompleteEventProcessor(@NonNull CommandService commandService,
                                                @NonNull RequestRepository requestRepository,
                                                @NonNull EventService eventService) {
        super(commandService, requestRepository);
        this.eventService = eventService;
    }

    @Override
    protected EventExecutionResponse process(DomainEvent domainEvent) {
        //Load 'current' complete request.
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());

        //Find duplicate requests (should be one at a time)
        Set<Request> pendingRequests = requestRepository.findByExternalReferenceNumberAndStatus(
                request.getExternalReferenceNumber(), singletonList(PENDING))
                .stream().filter(req -> ! Objects.equals(req.getWorkItemId(), domainEvent.getWorkItemId()))
                .collect(Collectors.toSet());

        if( !pendingRequests.isEmpty() )
        {
            Long workItemId;
            if(pendingRequests.size() == 1)
                workItemId = pendingRequests.iterator().next().getWorkItemId();
            else {
                Request latestPending = pendingRequests.iterator().next();
                workItemId = latestPending.getWorkItemId();
                request.duplicates( latestPending.filterCurrent(pendingRequests) );
            }
            eventService.send(new MandateReceivedDomainEvent(workItemId));
        }
        return SUCCESSFUL;
    }
}
