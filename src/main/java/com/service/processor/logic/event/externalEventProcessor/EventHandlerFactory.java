package za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.EventProcessor.EventHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;
import static org.springframework.core.ResolvableType.forClassWithGenerics;

@Component
public class EventHandlerFactory {

    @Autowired
    private ApplicationContext context;

    private Map<Class, EventHandler> eventHandlerMap = new HashMap<>();

    public <E> EventHandler handler(Class<E> eventClass) {

        ResolvableType processorType = forClassWithGenerics(EventHandler.class, eventClass);
        EventHandler handler = (EventHandler) context.getBeanProvider(processorType).getObject();

        return ofNullable(handler).orElse((event, requestType1, request) -> Optional.empty());
    }
}
