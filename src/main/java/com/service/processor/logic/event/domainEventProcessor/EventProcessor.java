package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.commands.ChangeProcessStatusCommand;
import za.co.fnb.plexus.services.request.commands.ChangeRequestStatusCommand;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.Objects.nonNull;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.CacheBasedLock.acquireLock;

@RequiredArgsConstructor
public abstract class EventProcessor<T extends DomainEvent> {

    @NonNull
    protected CommandService commandService;

    @NonNull
    protected RequestRepository requestRepository;

    public final EventExecutionResponse execute(DomainEvent event) {

        if( SynchronizeAction.class.isInstance( this )) {

            ReentrantLock acquiredLock = acquireLock(event.getWorkItemId());
            try {
                acquiredLock.lock();

                return process(event);
            } finally {

                if(nonNull(acquiredLock)) {
                    acquiredLock.unlock();
                }
            }
        }else {
            return process(event);
        }
    }

    protected void changeWorkFlowStatus(String status, Long workItemId) {
       changeWorkFlowStatus(status, workItemId, Collections.emptyList());
    }

    protected void changeWorkFlowStatus(String status, Long workItemId, Collection noOverridesStates) {
        // Fire command to
        ChangeProcessStatusCommand command = new ChangeProcessStatusCommand(workItemId, status, noOverridesStates);
        commandService.dispatch(command);
    }

    protected void changeRequestStatus(RequestStatus status, Long workItemId) {
        // Change status
        ChangeRequestStatusCommand command = new ChangeRequestStatusCommand(workItemId, status.name());
        commandService.dispatch(command);
    }

    protected abstract EventExecutionResponse process(DomainEvent domainEvent);
}
