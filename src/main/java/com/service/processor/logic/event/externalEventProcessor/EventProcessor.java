package za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;
import za.co.fnb.plexus.services.request.events.ExternalEvent;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import javax.transaction.Transactional;

import java.util.Optional;

import static za.co.fnb.plexus.services.request.dto.domain.request.RequestType.valueOf;

@Component
public class EventProcessor<E> {

    @Autowired
    private EventHandlerFactory handlerFactory;
    @Autowired
    private RequestRepository repository;

    @Transactional
    public void processByWI(ExternalEvent<E> externalEvent) {
        Request request = repository.findByWorkItemId(externalEvent.getRequestReferenceId());
        process(externalEvent, request);
    }

    @Transactional
    public void processByID(ExternalEvent<E> externalEvent) {
        Request request = repository.findById(externalEvent.getRequestReferenceId()).get();
        process(externalEvent, request);
    }

    private void process(ExternalEvent<E> externalEvent, Request request) {
        if(request == null)
            return;

        RequestType requestType = valueOf(request.getServiceRequestType().getCode());

        handlerFactory.handler(externalEvent.eventClass())
                .handle(externalEvent.getEventPayload(), requestType, request)
                .ifPresent(pProcessor -> EventPostProcessor.class.cast(pProcessor)
                        .eventHandled(externalEvent.getEventPayload(), request));

        repository.save(request);
    }

    @FunctionalInterface
    public interface EventHandler<E> {
        Optional<EventPostProcessor> handle(E event, RequestType requestType, Request request);
    }

    @FunctionalInterface
    public interface EventPostProcessor<E> {
        void eventHandled(E event, Request request);
    }
}
