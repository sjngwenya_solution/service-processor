package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestCompletedDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateSubmissionFailedDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.DebiCheck.Error_in_Request;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.DebiCheck.Request_Failed;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.RESPONSE_CODE;

@Component
public class MandateSubmissionFailedEventProcessor extends EventProcessor<MandateSubmissionFailedDomainEvent> {

    private EventService eventService;
    public MandateSubmissionFailedEventProcessor(@NonNull CommandService commandService,
                                                 @NonNull RequestRepository requestRepository,
                                                 @NonNull EventService eventService) {
        super(commandService, requestRepository);
        this.eventService = eventService;
    }

    @Override
    protected EventExecutionResponse process(DomainEvent domainEvent) {
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());
        String errorCode = findValue(request.getRequestMetadata(), RESPONSE_CODE);

        String destQueue = ERROR_IN_REQUEST_CODES.contains(errorCode) ? Error_in_Request.name() : Request_Failed.name();

        changeWorkFlowStatus(destQueue, domainEvent.getWorkItemId());
        changeRequestStatus(RequestStatus.FAILED, domainEvent.getWorkItemId());

        eventService.send(new MandateRequestCompletedDomainEvent(domainEvent.getWorkItemId()));

        return SUCCESSFUL;
    }

    private <T> T findValue(Set<RequestMetadata> requestMetadata, final String key) {
        return (T) requestMetadata.stream()
                .filter(metadata -> key.equals(metadata.getName()))
                .findFirst().map(RequestMetadata::getValue).get();
    }

    //TODO : @SPha move these to look up data
    public static final List<String> ERROR_IN_REQUEST_CODES = Arrays
            .asList("900012", "900010", "901052", "AC02", "910003", "AC06", "900003", "901115", "901068", "901067", "901103", "900003");
}
