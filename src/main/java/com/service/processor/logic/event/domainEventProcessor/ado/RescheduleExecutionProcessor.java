package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.ado;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionCallback;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.SynchronizeAction;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.SchedulingService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.infrastructure.TransactionUtil;
import za.co.fnb.plexus.services.request.infrastructure.repository.EventRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Ado.Retry_Scheduled;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;

@Component
public abstract class RescheduleExecutionProcessor<T extends DomainEvent> extends EventProcessor implements SynchronizeAction {

    private TransactionUtil transactionUtil;
    private EventRepository eventRepository;
    private SchedulingService schedulingService;

    @Autowired
    public RescheduleExecutionProcessor(@NonNull CommandService commandService,
                                        @NonNull RequestRepository requestRepository,
                                        @NonNull EventRepository eventRepository,
                                        @NonNull SchedulingService schedulingService,
                                        @NonNull TransactionUtil transactionUtil) {
        super(commandService, requestRepository);
        this.transactionUtil = transactionUtil;
        this.eventRepository = eventRepository;
        this.schedulingService = schedulingService;

    }

    @Override
    protected EventExecutionResponse process(DomainEvent requestEvent) {

        transactionUtil.executeInTransaction( (TransactionCallback<Object>) status -> {

            //Create scheduled event
            DomainEvent newEvent = createNextEvent(requestEvent.getWorkItemId());
            newEvent.scheduled();
            eventRepository.save(newEvent);

            //Create Job to execute
            schedulingService.scheduleJobNextHour(scheduleName());

            //Signal plexus
            changeWorkFlowStatus( Retry_Scheduled.name(), requestEvent.getWorkItemId());

            return true;
        });

        return SUCCESSFUL;
    }

    protected abstract DomainEvent createNextEvent(Long workItem);

    protected abstract String scheduleName();
}
