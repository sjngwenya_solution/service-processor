package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.factory.debicheck.SendMandateRequestFactory;
import za.co.fnb.plexus.services.request.businesslogic.util.mandate.MandateRequestType;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestedDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;
import za.co.fnb.plexus.services.request.dto.mandate.sendrequest.MandateDTO;
import za.co.fnb.plexus.services.request.infrastructure.adapter.MandateServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.*;
import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.*;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.DebiCheck.Service_Request_Maintenance;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalObjectToJson;
import static za.co.fnb.plexus.services.request.businesslogic.util.mandate.MandateRequestType.*;
import static za.co.fnb.plexus.services.request.domain.request.RequestStatus.openRequestStatuses;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;

@Component
public class MandateRequestedEventProcessor extends EventProcessor<MandateRequestedDomainEvent> implements InitializingBean {

    private static final String PRODUCT_CODE = "RF";

    private Map<MandateRequestType, MandateRequestStrategy> strategyContext = new HashMap<>();

    @Autowired
    private SendMandateRequestFactory factory;

    @Autowired
    private MandateServiceAdapter adapter;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    protected TaskService taskService;

    public MandateRequestedEventProcessor(@NonNull CommandService commandService,
                                          @NonNull RequestRepository requestRepository) {
        super(commandService, requestRepository);
    }

    @Override
    protected EventExecutionResponse process(DomainEvent domainEvent) {

        //Load 'this' request.
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());

        //Load 'duplicate' requests.
        Set<Request> duplicateRequests = requestRepository.findByExternalReferenceNumberAndStatus(
                request.getExternalReferenceNumber(), openRequestStatuses())
                .stream().filter(req -> ! Objects.equals(req.getWorkItemId(), domainEvent.getWorkItemId()))
                .collect(Collectors.toSet());

        //Check if even can run or wait for in-flight
        if(duplicateRequests.isEmpty())
        {
            //check inFlight request from mandate service
            if(adapter.isExistingMandateInFlight(request.getExternalReferenceNumber())){
                //Set inflight indicator.
                requestRepository.updateRequestMetaData(request.getWorkItemId(), SF_PENDING_INFLIGHT_INDICATOR,SF_PENDING_REQUEST,SF_PENDING_REQUEST_DATE);
                return RESCHEDULED;
            }

            MandateDTO mandate = factory.createMandateRequestDomainFactory(domainEvent, PRODUCT_CODE);
            MandateRequestType type = getByDCRERequestType(mandate.getRequestType());
            MandateRequestStrategy strategy = strategyContext.get(type);

            CompiledMandateRequest compiledRequest = strategy.compileRequest(mandate);

            String executionResponse = compiledRequest.getRequestFunction().apply(adapter, mandate);

            //Audit request
            compiledRequest.getAuditLog().setWorkItemId(domainEvent.getWorkItemId());
            auditLogService.log(compiledRequest.getAuditLog());

            //Audit response
            auditLogService.log(new ServiceRequestAuditDTO(executionResponse, MandateRequestStatusCode.getAuditLogType(),
                    RequestType.DCM,  domainEvent.getWorkItemId()));

            changeWorkFlowStatus(compiledRequest.getNewStatus(), domainEvent.getWorkItemId());

            return SUCCESSFUL;
        }else{
            //Means there is still an outstanding request. Do nothing.
            //Perhaps audit?
            return SCHEDULED;
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        strategyContext.put(UPDATE_BANK, MandateAmendmentRequest.create());
        strategyContext.put(UPDATE_DATE, MandateAmendmentRequest.create());
        strategyContext.put(CANCEL_MANDATE, MandateCancelRequest.create());
    }

    interface MandateRequestStrategy {
        CompiledMandateRequest compileRequest(MandateDTO mandate);
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    static class MandateAmendmentRequest implements MandateRequestStrategy {

        public static MandateAmendmentRequest create() {
            return new MandateAmendmentRequest();
        }

        @Override
        public CompiledMandateRequest compileRequest(MandateDTO mandate) {
            String payLoad = marshalObjectToJson(mandate);
            ServiceRequestAuditDTO dcm = new ServiceRequestAuditDTO(payLoad, SendRequestToMandateService.getAuditLogType(),
                    RequestType.DCM);

            return new CompiledMandateRequest(dcm, (mandateServiceAdapter, mandateDTO) ->
                    mandateServiceAdapter.sendMandate(mandate));
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    static class MandateCancelRequest implements MandateRequestStrategy {

        public static MandateCancelRequest create() {
            return new MandateCancelRequest();
        }

        @Override
        public CompiledMandateRequest compileRequest(MandateDTO mandate) {
            return new CompiledMandateRequest(new ServiceRequestAuditDTO(CancelMandateRequest.getAuditLogType(), RequestType.DCM),
                    (mandateServiceAdapter, mandateDTO) -> mandateServiceAdapter.cancelMandate(mandate.getApplicationNumber()));
        }
    }

    @Data
    @RequiredArgsConstructor
    static class CompiledMandateRequest {
        private String newStatus = Service_Request_Maintenance.name();

        @NonNull private ServiceRequestAuditDTO auditLog;
        @NonNull private BiFunction<MandateServiceAdapter, MandateDTO, String> requestFunction;
    }
}
