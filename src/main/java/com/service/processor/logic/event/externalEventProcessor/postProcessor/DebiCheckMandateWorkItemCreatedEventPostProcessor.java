package za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.postProcessor;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.factory.ServiceRequestEventFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.events.WorkCreatedEventDTO;

import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.MANDATE_REQUEST;

@Component
@RequiredArgsConstructor
public class DebiCheckMandateWorkItemCreatedEventPostProcessor implements EventProcessor.EventPostProcessor<WorkCreatedEventDTO> {

    @Autowired private EventService eventService;
    @Autowired private ServiceRequestEventFactory eventFactory;

    @Override
    public void eventHandled(WorkCreatedEventDTO event, Request request) {
        eventFactory.createEvents(MANDATE_REQUEST, request.getWorkItemId())
                .stream()
                .forEach(eventService::send);
    }
}
