package za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.cbs.common.business.shared.dto.WorkItemDto;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.EventProcessor.EventHandler;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.EventProcessor.EventPostProcessor;
import za.co.fnb.plexus.services.request.businesslogic.factory.post40.Post40StatusUpdateFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus;
import za.co.fnb.plexus.services.request.commands.ChangeRequestStatusCommand;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.task.Action;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.dispute.statusupdate.v1.StatusUpdateRequestDTO;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;
import za.co.fnb.plexus.services.request.events.StatusChangedEventDTO;
import za.co.fnb.plexus.services.request.infrastructure.adapter.ActiveMQAdapter;
import za.co.fnb.plexus.services.request.infrastructure.adapter.PlexusWorkflowAdapter;

import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.StatusUpdateEvent;
import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.StatusUpdateRequest;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Disputes.Invalid_Mandate_Refund;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Disputes.Valid_Mandate_Received;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalObjectToJson;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalWithNamespace;
import static za.co.fnb.plexus.services.request.domain.request.RequestStatus.COMPLETED;
import static za.co.fnb.plexus.services.request.domain.task.Action.*;

@Component
public class StatusChangedEventHandler  implements EventHandler<StatusChangedEventDTO> {

    @Autowired private TaskService taskService;
    @Autowired private CommandService commandService;
    @Autowired private ActiveMQAdapter activeMQAdapter;
    @Autowired private AuditLogService auditLogService;
    @Autowired private Post40StatusUpdateFactory updateFactory;
    @Autowired private PlexusWorkflowAdapter plexusWorkflowAdapter;

    @Override
    public Optional<EventPostProcessor> handle(StatusChangedEventDTO event,
                                               RequestType requestType,
                                               Request request) {

        //Log event received
        auditLogService.log(new ServiceRequestAuditDTO(marshalObjectToJson(event), StatusUpdateEvent.getAuditLogType(),
                requestType, request.getWorkItemId()));

        return (requestType == RequestType.DOD) ? of(DODProcessor) : empty();
    }

    private EventPostProcessor<StatusChangedEventDTO> DODProcessor = (event, request) -> {

        WorkItemDto updateWorkItemDto = plexusWorkflowAdapter.getWorkItemsById(request.getWorkItemId());

        StatusUpdateRequestDTO statusUpdateDTO = updateFactory.generateStatusUpdateRequest(request.getWorkItemId(),
                event.getPreviousQueue(), updateWorkItemDto);

        QueueStatus.Disputes status = QueueStatus.Disputes.getQueueStatusByStatus(updateWorkItemDto.getStatus());

        Action action;
        switch (status) {
            case Awaiting_Feedback_On_US:
            case Awaiting_Feedback_Off_us:
                action = POST40_STATUS_UPDATE_MADATEREQ;
                break;
            case Dispute_Received:
                action = POST40_STATUS_UPDATE_DISPLOGGED;
                break;
            default:
                action = POST40_STATUS_UPDATE;
        }

        taskService.createListIfNotExistWithTask(request.getWorkItemId(), action, statusUpdateDTO.getCorrelationID());
        String statusUpdateRequestXML = marshalWithNamespace(statusUpdateDTO.getStatusUpdateRequest());

        auditLogService.log(new ServiceRequestAuditDTO(statusUpdateRequestXML, StatusUpdateRequest.getAuditLogType(),
                RequestType.DOD, request.getWorkItemId()));
        activeMQAdapter.sendDisputeStatusUpdate(statusUpdateRequestXML, statusUpdateDTO.getCorrelationID());

        if(status == Valid_Mandate_Received || status == Invalid_Mandate_Refund) {
            commandService.dispatch(new ChangeRequestStatusCommand(request.getWorkItemId(), COMPLETED.name()));
        }
    };
}
