package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;


import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateDuplicateDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.DUPLICATE;

@Component
public class MandateDuplicateEventProcessor  extends EventProcessor<MandateDuplicateDomainEvent> {

    public MandateDuplicateEventProcessor(@NonNull CommandService commandService,
                                          @NonNull RequestRepository requestRepository) {
        super(commandService, requestRepository);
    }

    @Override
    protected EventExecutionResponse process(DomainEvent domainEvent) {
        changeWorkFlowStatus(QueueStatus.DebiCheck.Duplicate_Request.name(), domainEvent.getWorkItemId());
        changeRequestStatus(RequestStatus.DUPLICATE, domainEvent.getWorkItemId());
        return DUPLICATE;
    }
}
