package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.ado.FailedExecutionProcessor;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.ado.RescheduleExecutionProcessor;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.ado.SuccessfulExecutionProcessor;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.SchedulingService;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.domain.events.*;
import za.co.fnb.plexus.services.request.domain.events.ado.*;
import za.co.fnb.plexus.services.request.infrastructure.TransactionUtil;
import za.co.fnb.plexus.services.request.infrastructure.repository.EventRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

@Component
public class EventProcessorConfig {

    @Autowired CommandService commandService;
    @Autowired TransactionUtil transactionUtil;
    @Autowired TaskService taskService;

    @Autowired EventRepository eventRepository;
    @Autowired SchedulingService schedulingService;
    @Autowired RequestRepository requestRepository;

    @Configuration
    class ProcessorDeclaration {

        @Bean
        public FailedExecutionProcessor<ReversePaymentFailedDomainEvent> reversePaymentFailed() {
              return new FailedExecutionProcessor<ReversePaymentFailedDomainEvent>(commandService, requestRepository, taskService) {
                  @Override
                  public String toString() {
                      return "ReversePaymentFailedDomainEventProcessor";
                  }
              };
        }

        @Bean
        public FailedExecutionProcessor<StopPaymentFailedDomainEvent> stopPaymentFailed() {
            return new FailedExecutionProcessor<StopPaymentFailedDomainEvent>(commandService, requestRepository, taskService) {
                @Override
                public String toString() {
                    return "StopPaymentFailedDomainEventProcessor";
                }
            };
        }

        @Bean
        public SuccessfulExecutionProcessor<PaymentReversedDomainEvent> reversePaymentSuccessful() {
            return new SuccessfulExecutionProcessor<PaymentReversedDomainEvent>(commandService, requestRepository, taskService) {
                @Override
                public String toString() {
                    return "PaymentReversedDomainEventProcessor";
                }
            };
        }

        @Bean
        public SuccessfulExecutionProcessor<PaymentStoppedDomainEvent> stopPaymentSuccessful() {
            return new SuccessfulExecutionProcessor<PaymentStoppedDomainEvent>(commandService, requestRepository, taskService) {
                @Override
                public String toString() {
                    return "PaymentStoppedDomainEventProcessor";
                }
            };
        }

        @Bean
        public RescheduleExecutionProcessor<ReversePaymentRescheduledEvent> reversePaymentReschedule() {
            return new RescheduleExecutionProcessor<ReversePaymentRescheduledEvent>(commandService, requestRepository,
                    eventRepository, schedulingService, transactionUtil) {

                @Override
                protected DomainEvent createNextEvent(Long workItem) {
                    return new ReversePaymentRequestedDomainEvent(workItem);
                }

                @Override
                protected String scheduleName() {
                    return "ReversePaymentRequestedDomainEvent";
                }

                @Override
                public String toString() {
                    return "ReversePaymentRescheduleEventProcessor";
                }
            };
        }

        @Bean
        public RescheduleExecutionProcessor<StopPaymentRescheduledDomainEvent> stopPaymentReschedule() {
            return new RescheduleExecutionProcessor<StopPaymentRescheduledDomainEvent>(commandService, requestRepository,
                    eventRepository, schedulingService, transactionUtil) {

                @Override
                protected DomainEvent createNextEvent(Long workItem) {
                    return new StopPaymentRequestedDomainEvent(workItem);
                }

                @Override
                protected String scheduleName() {
                    return "StopPaymentRequestedDomainEvent";
                }

                @Override
                public String toString() {
                    return "StopPaymentRescheduleDomainEventProcessor";
                }
            };
        }

        }
    }