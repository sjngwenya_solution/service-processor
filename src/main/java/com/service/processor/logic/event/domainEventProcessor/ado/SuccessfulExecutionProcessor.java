package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.ado;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.SynchronizeAction;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.domain.task.Tasklist;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Optional;
import java.util.function.Function;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Ado.Failed_Request;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Ado.Request_Processed;
import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;

public abstract class SuccessfulExecutionProcessor<T extends DomainEvent> extends EventProcessor implements SynchronizeAction {

    private TaskService taskService;

    @Autowired
    public SuccessfulExecutionProcessor(@NonNull CommandService commandService,
                                        @NonNull RequestRepository requestRepository,
                                        @NonNull TaskService taskService) {
        super(commandService, requestRepository);
        this.taskService = taskService;
    }

    @Override
    public EventExecutionResponse process(DomainEvent requestEvent) {
        taskService.executeFunction(requestEvent.getWorkItemId(), CHECK_TRANSITION)
                .ifPresent(transition -> {
                    changeWorkFlowStatus(transition, requestEvent.getWorkItemId());
                    changeRequestStatus(RequestStatus.COMPLETED, requestEvent.getWorkItemId());
                });
        return SUCCESSFUL;
    }

    private static final Function<Tasklist, Optional<String>> CHECK_TRANSITION = checklist -> {
        if(checklist.isComplete())
            return of(Request_Processed.name());

        if(checklist.anyFailed() && !checklist.anyPending())
            return of(Failed_Request.name());

        return empty();
    };
}
