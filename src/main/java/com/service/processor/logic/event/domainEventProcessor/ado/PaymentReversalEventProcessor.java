package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.ado;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.command.CommandFactory;
import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.RequestService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.MainframeInteractionServiceBean;
import za.co.fnb.plexus.services.request.domain.events.ado.ReversePaymentRequestedDomainEvent;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.function.Predicate;

import static za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction.PAYMENT_REVERSAL;
import static za.co.fnb.plexus.services.request.businesslogic.util.ado.ADOServiceActionUtil.reversePaymentRequired;

@Component
public class PaymentReversalEventProcessor extends FunctionExecutionProcessor<ReversePaymentRequestedDomainEvent> {

    @Autowired
    public PaymentReversalEventProcessor(@NonNull CommandService commandService,
                                         @NonNull RequestRepository requestRepository,
                                         @NonNull RequestService requestService,
                                         @NonNull MainframeInteractionServiceBean scheduleService,
                                         @NonNull CommandFactory commandFactory) {
        super(commandService, requestRepository, scheduleService, requestService, commandFactory);
    }

    @Override
    protected MainframeFunction getMainframeFunction() {
        return PAYMENT_REVERSAL;
    }

    @Override
    public Predicate<String> prerequisitePredicate() {
        return serviceInstruction -> reversePaymentRequired(serviceInstruction);
    }
}
