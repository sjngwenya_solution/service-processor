package za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.postProcessor;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.EventProcessor.EventPostProcessor;
import za.co.fnb.plexus.services.request.businesslogic.factory.ServiceRequestEventFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.ado.ReversePaymentRequestedDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.ado.StopPaymentRequestedDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.events.WorkCreatedEventDTO;

import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.SERVICE_INSTRUCTION;

@Component
@RequiredArgsConstructor
public class AdoWorkItemCreatedEventPostProcessor implements EventPostProcessor<WorkCreatedEventDTO> {

    private static final List<Class> TASK_CREATING_EVENTS =
            asList(ReversePaymentRequestedDomainEvent.class, StopPaymentRequestedDomainEvent.class);

    @Autowired private TaskService taskService;
    @Autowired private EventService eventService;
    @Autowired private ServiceRequestEventFactory eventFactory;

    @Override
    public void eventHandled(WorkCreatedEventDTO event, Request request) {

        String instruction = request.getMetaDataByName(SERVICE_INSTRUCTION).get().getValue();

        Set<DomainEvent> domainEvents = eventFactory.createEvents(instruction, request.getWorkItemId());

        Long numberOfRequiredTasks = domainEvents.stream()
                .filter(domainEvent -> TASK_CREATING_EVENTS.contains(domainEvent.getClass()))
                .count();

        //Open service request
        request.open();

        //Create a checklist
        taskService.createList(request.getWorkItemId(), numberOfRequiredTasks);

        //Fire events
        domainEvents.stream().forEach(eventService::send);
    }
}
