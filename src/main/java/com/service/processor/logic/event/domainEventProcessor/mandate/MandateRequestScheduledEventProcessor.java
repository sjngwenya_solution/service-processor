package za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.mandate;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionCallback;
import za.co.fnb.plexus.services.request.businesslogic.event.domainEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateDuplicateDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestScheduledDomainEvent;
import za.co.fnb.plexus.services.request.domain.events.mandate.MandateRequestedDomainEvent;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.infrastructure.TransactionUtil;
import za.co.fnb.plexus.services.request.infrastructure.repository.EventRepository;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse.SUCCESSFUL;
import static za.co.fnb.plexus.services.request.domain.events.EventStatus.openEvents;
import static za.co.fnb.plexus.services.request.domain.request.RequestStatus.openRequestStatuses;

@Component
public class MandateRequestScheduledEventProcessor extends EventProcessor<MandateRequestScheduledDomainEvent> {

    private EventService eventService;
    private EventRepository eventRepository;
    private TransactionUtil transactionUtil;

    public MandateRequestScheduledEventProcessor(@NonNull CommandService commandService,
                                                 @NonNull RequestRepository requestRepository,
                                                 @NonNull EventRepository eventRepository,
                                                 @NonNull TransactionUtil transactionUtil,
                                                 @NonNull EventService eventService) {
        super(commandService, requestRepository);
        this.transactionUtil = transactionUtil;
        this.eventRepository = eventRepository;
        this.eventService = eventService;
    }

    @Override
    protected synchronized EventExecutionResponse process(DomainEvent domainEvent) {

        //Load 'current' request (pending) and open it.
        Request request = requestRepository.findByWorkItemId(domainEvent.getWorkItemId());
        request.open();

        //Find duplicate requests (should be one at a time)
        Set<Request> duplicateRequests = request.filterCurrent(
                requestRepository.findByExternalReferenceNumberAndStatus(request.getExternalReferenceNumber(),
                        openRequestStatuses()));

        //Cancel scheduled events (should be one)
        List<DomainEvent> domainEvents = cancelScheduledEvents(duplicateRequests);

        //Create scheduled event
        DomainEvent newEvent = new MandateRequestedDomainEvent(domainEvent.getWorkItemId());
        newEvent.scheduled();
        domainEvents.add(newEvent);

        //Save events
        eventRepository.saveAll( domainEvents );

        //Duplicate requests
        request.duplicates( duplicateRequests );
        duplicateRequests.stream().forEach(duplicateRequest ->
                eventService.send(new MandateDuplicateDomainEvent(duplicateRequest.getWorkItemId())));

        return SUCCESSFUL;
    }

    private List<DomainEvent> cancelScheduledEvents(Set<Request> duplicateRequests) {
        //Find current event and disable.
        List<DomainEvent> domainEvents = duplicateRequests.stream()
                .map(r -> eventRepository.findByWorkItemIdAndClassNameAndStatusIn(r.getWorkItemId(),
                        MandateRequestedDomainEvent.class.getSimpleName(), openEvents())
                        .collect(Collectors.toList()))
                .flatMap(eventLists -> eventLists.stream())
                .map(e -> {
                    e.canceled();
                    return e;
                }).collect(Collectors.toList());

        return domainEvents;
    }
}


