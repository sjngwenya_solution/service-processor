package za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado;

import za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType;
import za.co.fnb.plexus.services.request.dto.ado.paymentreversal.PaymentReversalResponse;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshall;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;

public class ReversePaymentResponse extends AdoResponse {

    private PaymentReversalResponse response;

    public ReversePaymentResponse(String payload) {
        super(payload);

        response = unmarshall( payload,
                PaymentReversalResponse.class);
    }

    @Override
    String getCorrelationId() {
        return response.getHeader().getCorrelationID();
    }

    @Override
    String getReferenceId() {
        return response.getHeader().getReferenceID();
    }

    @Override
    String getHeaderReplyStatus() {
        return response.getHeader().getResult().getStatus().name();
    }

    @Override
    String getHeaderReplyCode() {
        return response.getHeader().getResult().getCode();
    }

    @Override
    String getHeaderReplyStatusDescription() {
        return response.getHeader().getResult().getDescription();
    }

    @Override
    String getPayloadReplyCode() {
        return String.valueOf(response.getPaymentReversalReply().getReplyCode());
    }

    @Override
    String getPayloadReplyDescription() {
        return response.getPaymentReversalReply().getErrorDescription();
    }

    @Override
    String getMetadataStatusKey() {
        return REVERSE_PAYMENT_STATUS_DESCRIPTION;
    }

    @Override
    String getMetadataStatusDateKey() {
        return REVERSE_PAYMENT_ACTION_DATE;
    }

    @Override
    String getSuccessEventCode() {
        return PAYMENT_REVERSAL_RESPONSE_SUCCESSFUL;
    }

    @Override
    String getFailedEventCode() {
        return PAYMENT_REVERSAL_RESPONSE_FAILED;
    }

    @Override
    String getRetryEventCode() {
        return PAYMENT_REVERSAL_RESPONSE_RESCHEDULE;
    }

    @Override
    String getAuditType() {
        return AuditLogType.PaymentReversalResponse.getAuditLogType();
    }
}
