package za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;

import static za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado.AdoResponseState.*;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class AdoResponseContext {

    @NonNull
    private AdoResponseState state;
    @NonNull
    private AdoResponse responseDto;

    public static AdoResponseContext create(IncomingResponse incomingResponse) {

        AdoResponse responseDto = null;

        if(STOP_PAYMENT_RESPONSE.equals(incomingResponse.getParam(ADO_RESPONSE_TYPE))) {
            responseDto = new StopPaymentResponse( (String) incomingResponse.getPayload() );
        }else if(PAYMENT_REVERSAL_RESPONSE.equals(incomingResponse.getParam(ADO_RESPONSE_TYPE))) {
            responseDto = new ReversePaymentResponse( (String) incomingResponse.getPayload() );
        }

        //FIXME : Clean this up
        AdoResponseState responseState =
                (responseDto.isSuccessful()) ? SUCCESS_STATE : (responseDto.isIlError()) ? IL_ERROR_STATE : MF_ERROR_STATE;

        return new AdoResponseContext(responseState, responseDto);
    }

    public AdoResponseData processResponse() {

        AdoResponseData responseData = state.extractDetails(responseDto);

        responseData.setMetadataStatusKey(responseDto.getMetadataStatusKey());
        responseData.setMetadataStatusDateKey(responseDto.getMetadataStatusDateKey());

        responseData.setSuccessEventCode(responseDto.getSuccessEventCode());
        responseData.setFailedEventCode(responseDto.getFailedEventCode());
        responseData.setRetryEventCode(responseDto.getRetryEventCode());

        responseData.setAuditDTO(responseDto.getLog());

        return responseData;
    }
}
