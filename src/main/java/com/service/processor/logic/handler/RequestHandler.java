package za.co.fnb.plexus.services.request.businesslogic.handler;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.factory.request.AuditLogFactory;
import za.co.fnb.plexus.services.request.businesslogic.factory.request.RequestAbstractFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;
import za.co.fnb.plexus.services.request.businesslogic.validation.RequestValidator;
import za.co.fnb.plexus.services.request.commands.CreateWorkCommand;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Map;
import java.util.Optional;

import static java.util.Optional.of;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalObjectToJson;

@RequiredArgsConstructor
public abstract class RequestHandler<RequestDTO> {

    @NonNull private CommandService commandService;

    @NonNull protected AuditLogService logService;
    @NonNull protected RequestAbstractFactory factory;
    @NonNull protected RequestRepository requestRepository;

    public RequestType handles() {
        return factory.requestType();
    }

    public Optional<Request> createRequest(String serializedRequest, Map<String, String> parameters) {

        RequestDTO requestDTO = transform(serializedRequest);
        //Validate Request
        RequestValidator validator = factory.getValidator();
        Notification validationResult = validator.validate(requestDTO);

        AuditLogFactory logFactory = factory.auditLogFactory();
        //Handle validation errors
        if(validationResult.hasErrors()) {
            logService.log(
                logFactory.validationFailedLog(factory.requestType(), marshalObjectToJson(validationResult), parameters,
                        handleValidationErrors(requestDTO, parameters, validationResult))
            );
            return Optional.empty();
        }

        //Create service request
        Request request = factory.getDomainFactory().createServiceRequestDomainModel(requestDTO);

        //Save request
        Optional<Request> savedRequest = of(requestRepository.save(request));

        //Audit work request
        logService.log(
            logFactory.serviceRequestLog(factory.requestType(), request, serializedRequest, parameters)
        );

        //Issue Create work command
        CreateWorkCommand createWorkCommand = createWorkCommand(request);
        commandService.dispatch(createWorkCommand);

        onServiceRequestCreated(request, requestDTO);

        return savedRequest;
    }

    private CreateWorkCommand createWorkCommand(Request request) {
        RequestType requestType = factory.requestType();

        return CreateWorkCommand.builder()
                .requestId(request.getId())
                .workItemType(requestType.getWorkItemTem())
                .workItemReference(request.getPrimaryAccount().getAccountNumber())
                .keyFields(factory.createKeyFieldsFactory(request))
                .build();
    }

    protected abstract RequestDTO transform(Object requestObject);

    protected abstract String handleValidationErrors(RequestDTO requestDTO, Map<String, String> parameters,
                                         Notification validationResults);

    /**
     * This is where you send a response if using request/response via async RPC.
     *
     * @param request - The created service request domain model.
     * @param requestDTO - The Request DTO.
     */
    protected void onServiceRequestCreated(Request request, RequestDTO requestDTO){}

    public abstract void handleResponse(IncomingResponse response);
}
