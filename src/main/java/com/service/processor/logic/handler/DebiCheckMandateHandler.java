package za.co.fnb.plexus.services.request.businesslogic.handler;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.base.exception.ServiceExceptionType;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.businesslogic.factory.ServiceRequestEventFactory;
import za.co.fnb.plexus.services.request.businesslogic.factory.request.DebiCheckMandateRequestFactory;
import za.co.fnb.plexus.services.request.businesslogic.handler.helper.debicheck.DebiCheckResponseProcessor;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestMetadata;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.mandate.creatework.DebitCheckMandateRequest;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;
import static za.co.fnb.plexus.services.request.businesslogic.handler.helper.debicheck.DebiCheckResponseProcessor.create;
import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.MandateServiceResponse;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshall;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshallJsonFile;
import static za.co.fnb.plexus.services.request.domain.request.RequestStatus.openRequestStatuses;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;

@Component
public class DebiCheckMandateHandler extends RequestHandler<DebitCheckMandateRequest> {

    private ServiceRequestEventFactory eventFactory;
    private EventService eventService;

    public DebiCheckMandateHandler(@NonNull EventService eventService,
                                   @NonNull AuditLogService logService,
                                   @NonNull CommandService commandService,
                                   @NonNull RequestRepository requestRepository,
                                   @NonNull DebiCheckMandateRequestFactory factory,
                                   @NonNull ServiceRequestEventFactory eventFactory) {

        super(commandService, logService, factory, requestRepository);
        this.eventFactory = eventFactory;
        this.eventService = eventService;
    }

    @Override
    protected DebitCheckMandateRequest transform(Object requestObject) {
        String payload = ((String) requestObject);
        if (payload.startsWith("<") || payload.contains("xml")) {
            return unmarshall((String) requestObject, DebitCheckMandateRequest.class);
        } else {
            return unmarshallJsonFile((String) requestObject, DebitCheckMandateRequest.class);
        }
    }

    @Override
    protected String handleValidationErrors(DebitCheckMandateRequest requestDTO,
                                            Map<String, String> parameters,
                                            Notification validationResults) {
        return requestDTO.getApplicationDetails().getApplicationNumber();
    }

    @Override
    public void handleResponse(IncomingResponse response) {
        DebiCheckResponseProcessor responseProcessor = create(response);

        Set<Request> openServiceRequests = requestRepository
                .findByExternalReferenceNumberAndStatus(responseProcessor.getApplicationNumber(), openRequestStatuses());

        if (openServiceRequests.size() > 1)
            throw new ServiceException(ServiceExceptionType.INVALID_REQUEST,
                    format("Multiple open requests for application number [ %s ]", responseProcessor.getApplicationNumber()));

        Optional<Request> optionalRequest = openServiceRequests.stream().findFirst();

        if (optionalRequest.isPresent()) {
            Request request = optionalRequest.get();

            //Save response details
            Set<RequestMetadata> requestMetadata = Stream.of(new RequestMetadata(RESPONSE_CODE, responseProcessor.getMandateDTO().getReason()),
                    new RequestMetadata(RESPONSE_DESCRIPTION, responseProcessor.getMandateDTO().getReasonDescription()),
                    new RequestMetadata(RESPONSE_STATUS, responseProcessor.getMandateDTO().getStatus()))
                    .collect(Collectors.toSet());
            requestRepository.updateRequestMetaData(request.getWorkItemId(), requestMetadata, RESPONSE_DATE);

            //Fire events for response
            eventFactory.createEvents(responseProcessor.getEventKey(request.metaDataValueByName(APPLICATION_TYPE)),
                    request.getWorkItemId())
                    .stream().forEach(eventService::send);

            //Audit
            logService.log(ServiceRequestAuditDTO.withReference((String) response.getPayload(),
                    MandateServiceResponse.getAuditLogType(),  factory.requestType(), request.getWorkItemId(),
                    request.getExternalReferenceNumber()));

            return;
        }
        throw new ServiceException(ServiceExceptionType.INVALID_REQUEST,
                format("No Open or Pending Request found for mandate response with Application number [ %s ]", responseProcessor.getApplicationNumber()));
    }
}
