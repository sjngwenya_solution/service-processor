
package za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "DuplicateRequest",
    "ServiceUnavailable",
    "BadReplay"
})
public class AdoResponseCode {

    @JsonProperty("DuplicateRequest")
    private List<String> duplicateRequest = null;
    @JsonProperty("ServiceUnavailable")
    private List<String> serviceUnavailable = null;
    @JsonProperty("BadReplay")
    private List<String> badReplay = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

}
