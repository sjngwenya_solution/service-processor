package za.co.fnb.plexus.services.request.businesslogic.handler.helper.debicheck;

import org.apache.commons.lang3.StringUtils;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.IntegrationLayerUtil.SUCCESS_RESULT;

public abstract class DebiCheckResponse<T> {

    private String responseAsString;

    public DebiCheckResponse(String payload) {
        this.responseAsString = payload;
    }

    public boolean isSuccessful() {
        if(SUCCESS_RESULT.equals(getHeaderReplyStatus()))
            return true;
        else
            return false;
    }

    public boolean isIlError() {
        return StringUtils.isEmpty(getPayloadReplyCode())
                || "null".equalsIgnoreCase(getPayloadReplyCode());
    }

    public ServiceRequestAuditDTO getLog() {
        ServiceRequestAuditDTO auditDTO = new ServiceRequestAuditDTO(responseAsString, getAuditType(), RequestType.DCM);
        auditDTO.setCorrelationID(getCorrelationId());
        auditDTO.setReference(getReferenceId());
        return auditDTO;
    }

    abstract String getCorrelationId();

    abstract String getReferenceId();

    abstract String getHeaderReplyStatus();

    abstract String getHeaderReplyCode();

    abstract String getHeaderReplyStatusDescription();

    abstract String getPayloadReplyCode();

    abstract String getPayloadReplyDescription();

    abstract String getMetadataStatusKey();

    abstract String getMetadataStatusDateKey();

    abstract String getSuccessEventCode();

    abstract String getFailedEventCode();

    abstract String getRetryEventCode();

    abstract String getAuditType();
}