package za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado;

import za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType;
import za.co.fnb.plexus.services.request.dto.ado.stop.payment.CreateStopPaymentResponse;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshall;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;

public class StopPaymentResponse extends AdoResponse {

    protected CreateStopPaymentResponse response;

    public StopPaymentResponse(String payload) {
        super(payload);

        response = unmarshall( payload,
                CreateStopPaymentResponse.class);
    }

    @Override
    String getCorrelationId() {
        return response.getHeader().getCorrelationID();
    }

    @Override
    String getReferenceId() {
        return  response.getHeader().getReferenceID();
    }

    @Override
    String getHeaderReplyStatus() {
        return response.getHeader().getResult().getStatus().name();
    }

    @Override
    String getHeaderReplyCode() {
        return response.getHeader().getResult().getCode();
    }

    @Override
    String getHeaderReplyStatusDescription() {
        return response.getHeader().getResult().getDescription();
    }

    @Override
    String getPayloadReplyCode() {
        return String.valueOf(response.getStopPaymentReply().getReplyCode());
    }

    @Override
    String getPayloadReplyDescription() {
        return response.getStopPaymentReply().getErrorDescription();
    }

    @Override
    String getMetadataStatusKey() {
        return STOP_PAYMENT_STATUS_DESCRIPTION;
    }

    @Override
    String getMetadataStatusDateKey() {
        return STOP_PAYMENT_ACTION_DATE;
    }

    @Override
    String getSuccessEventCode() {
        return STOP_PAYMENT_RESPONSE_SUCCESSFUL;
    }

    @Override
    String getFailedEventCode() {
        return STOP_PAYMENT_RESPONSE_FAILED;
    }

    @Override
    String getRetryEventCode() {
        return STOP_PAYMENT_RESPONSE_RESCHEDULE;
    }

    @Override
    String getAuditType() {
        return  AuditLogType.CreateStopPaymentResponse.getAuditLogType();
    }
}
