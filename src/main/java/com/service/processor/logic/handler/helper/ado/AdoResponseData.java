package za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado;

import lombok.Data;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;

@Data
public class AdoResponseData {

    private String correlationId;

    private String status;
    private String code;
    private String description;

    private String metadataStatusKey;
    private String metadataStatusDateKey;

    private String successEventCode;
    private String failedEventCode;
    private String retryEventCode;

    private ServiceRequestAuditDTO auditDTO;


    public AdoResponseData(String correlationId, String status, String code, String description) {
        this.correlationId = correlationId;
        this.status = status;
        this.code = code;
        this.description = description;
    }

    public ServiceRequestAuditDTO getAuditDTO(Long workItemId) {
        auditDTO.setWorkItemId(workItemId);
        return auditDTO;
    }
}
