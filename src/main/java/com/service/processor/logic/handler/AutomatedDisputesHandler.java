package za.co.fnb.plexus.services.request.businesslogic.handler;

import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.businesslogic.factory.ServiceRequestEventFactory;
import za.co.fnb.plexus.services.request.businesslogic.factory.ado.AdoResponseMessageProcessor;
import za.co.fnb.plexus.services.request.businesslogic.factory.ado.AdoResponseMessageProcessor.OutcomeDetails;
import za.co.fnb.plexus.services.request.businesslogic.factory.request.AdoRequestFactory;
import za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado.AdoResponseContext;
import za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado.AdoResponseData;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.dispute.AdoInputDTO;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Map;
import java.util.function.BiFunction;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshallJsonFile;

@Component
public class AutomatedDisputesHandler extends RequestHandler<AdoInputDTO> {

    private TaskService taskService;
    private EventService eventService;
    private AdoResponseMessageProcessor processor;
    private ServiceRequestEventFactory eventFactory;

    private BiFunction<AdoResponseMessageProcessor.OUTCOME, String, Long> outcomeProcessor = (outcome, correlation) -> {
        switch (outcome) {
            case SUCCESS: return taskService.completeTask(correlation);
            case RETRY: return taskService.retryable(correlation);
            default: return taskService.failTask(correlation);
        }
    };

    public AutomatedDisputesHandler(ServiceRequestEventFactory eventFactory,
                                     AdoResponseMessageProcessor processor,
                                     RequestRepository requestRepository,
                                     CommandService commandService,
                                     AuditLogService logService,
                                     EventService eventService,
                                     AdoRequestFactory factory,
                                     TaskService taskService) {

        super(commandService, logService, factory, requestRepository);
        this.eventService = eventService;
        this.eventFactory = eventFactory;
        this.taskService = taskService;
        this.processor = processor;
    }

    @Override
    protected AdoInputDTO transform(Object requestObject) {
        return unmarshallJsonFile((String) requestObject, AdoInputDTO.class);
    }

    @Override
    protected String handleValidationErrors(AdoInputDTO afterDebitOrderRequestDTO,
                                            Map<String, String> parameters,
                                            Notification validationResults) {
        return afterDebitOrderRequestDTO.getMetadataValue(AdoInputDTO.FILE_NAME);
    }

    @Override
    public void handleResponse(IncomingResponse response) {

        AdoResponseContext context = AdoResponseContext.create(response);
        AdoResponseData responseData = context.processResponse();

        OutcomeDetails outcomeDetails = processor.processResponse(responseData);

        //Check if its success or failure, update relevant action
        Long workItemId = outcomeProcessor.apply(outcomeDetails.getOutcome(), responseData.getCorrelationId());

        //Update meta data here.
        // [Chances are we will get ObjectOptimisticLockingFailureException but that should be okay because the message will be re-tried ]
        requestRepository.updateRequestMetaData(workItemId, responseData.getMetadataStatusKey(),
                responseData.getDescription(),
                responseData.getMetadataStatusDateKey());

        //Generate appropriate events and fire them off.
        eventFactory.createEvents(outcomeDetails.getEventCode(), workItemId)
                .stream().forEach(eventService::send);

        //perform audit logs
        logService.log( responseData.getAuditDTO( workItemId ));
    }
}
