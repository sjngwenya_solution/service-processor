package za.co.fnb.plexus.services.request.businesslogic.handler.helper.ado;

import org.apache.commons.lang3.StringUtils;

import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.ADO_SUCCESS_RESPONSE_DESCRIPTION;

@FunctionalInterface
public interface AdoResponseState {

    AdoResponseState SUCCESS_STATE = response -> new AdoResponseData(response.getCorrelationId(),
            response.getHeaderReplyStatus(),
            response.getPayloadReplyCode(),
            ADO_SUCCESS_RESPONSE_DESCRIPTION);

    AdoResponseState MF_ERROR_STATE = response -> new AdoResponseData(response.getCorrelationId(),
            response.getHeaderReplyStatus(),
            response.getPayloadReplyCode(),
            response.getPayloadReplyDescription());

    AdoResponseState IL_ERROR_STATE = response -> new AdoResponseData(response.getCorrelationId(),
            response.getHeaderReplyStatus(),
            response.getHeaderReplyCode(),
            StringUtils.substring( response.getHeaderReplyStatusDescription(), 0, 40));

    AdoResponseData extractDetails(AdoResponse response);
}


