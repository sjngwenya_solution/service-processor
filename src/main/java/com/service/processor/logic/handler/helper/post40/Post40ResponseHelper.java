package za.co.fnb.plexus.services.request.businesslogic.handler.helper.post40;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import za.co.fnb.cbs.common.business.shared.dto.WorkItemDto;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.businesslogic.factory.post40.Post40StatusUpdateFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType;
import za.co.fnb.plexus.services.request.domain.task.Action;
import za.co.fnb.plexus.services.request.domain.task.Tasklist;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.dispute.statusupdate.v1.StatusUpdateRequestDTO;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;
import za.co.fnb.plexus.services.request.infrastructure.adapter.ActiveMQAdapter;
import za.co.fnb.plexus.services.request.infrastructure.adapter.PlexusWorkflowAdapter;

import java.util.function.BiFunction;

import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.*;
import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.StatusUpdateRequest;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueName.DISP_New;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Disputes.Awaiting_Feedback_On_US;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.QueueStatus.Disputes.Dispute_Received;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalWithNamespace;
import static za.co.fnb.plexus.services.request.domain.task.Action.POST40_STATUS_UPDATE_DISPLOGGED;
import static za.co.fnb.plexus.services.request.domain.task.Action.POST40_STATUS_UPDATE_MADATEREQ;

@Component
@RequiredArgsConstructor
public class Post40ResponseHelper {
    private static final BiFunction<Tasklist, String, Action> getCheckListItemActionWithCorrelationID = (checkList, itemCorrelationId) ->
            checkList.getListTasks().stream().filter(action -> itemCorrelationId.equals(action.getTaskReference())).findFirst().get().getAction();

    @NonNull private TaskService taskService;
    @NonNull private AuditLogService logService;
    @NonNull private ActiveMQAdapter activeMQAdapter;
    @NonNull private PlexusWorkflowAdapter plexusWorkflowAdapter;
    @NonNull private Post40StatusUpdateFactory post40StatusUpdateFactory;

    public void statusUpdateResponse(Post40ResponseData response) {

        try {
            long workItemID = taskService.retrieveWorkItemID(response.getCorrelationID());
            Action statusUpdateAction = taskService.executeFunction(workItemID, response.getCorrelationID(), getCheckListItemActionWithCorrelationID);

            if (statusUpdateAction != null) {
                switch (statusUpdateAction) {
                    case POST40_STATUS_UPDATE:
                    case SLA_VIOLATED_STATUS_UPDATE:
                        processStatusUpdateResponse(response, response.getPayload());
                        break;
                    case POST40_STATUS_UPDATE_DISPLOGGED:
                        processStatusUpdateDispLoggedResponse(response, response.getPayload());
                        break;
                    case POST40_STATUS_UPDATE_MADATEREQ:
                        processStatusUpdateMandateReqResponse(response, response.getPayload());
                        break;
                }
            }
        } catch (ServiceException e) {
            auditResponse(StatusUpdateResponseProcessingFailed, null, response.getPayload(),
                    response.getCorrelationID());
        }
    }

    private void processStatusUpdateResponse(Post40ResponseData responseHelper, String payload) {
        if (responseHelper.isSuccess()) {
            Long workItemId = taskService.completeTask(responseHelper.getCorrelationID());
            auditResponse(StatusUpdateResponseSuccess, workItemId, payload, responseHelper.getCorrelationID());

        } else if (responseHelper.isOutOfSync()) {
            Long workItemID = taskService.failTask(responseHelper.getCorrelationID());
            auditResponse(StatusUpdateOutOfSync, workItemID, payload, responseHelper.getCorrelationID());

            sendStatusUpdateRequestOutOfSync(workItemID, POST40_STATUS_UPDATE_DISPLOGGED, "",
                    Dispute_Received.getStatus());
        } else {
            Long workItemID = taskService.failTask(responseHelper.getCorrelationID());
            auditResponse(StatusUpdateResponseFailure, workItemID, payload, responseHelper.getCorrelationID());
        }
    }

    private void processStatusUpdateDispLoggedResponse(Post40ResponseData responseHelper, String payload) {
        if (responseHelper.isSuccess()) {
            long workItemID = taskService.completeTask(responseHelper.getCorrelationID());
            auditResponse(StatusUpdateResponseSuccess, workItemID, payload, responseHelper.getCorrelationID());

            sendStatusUpdateRequestOutOfSync(workItemID, POST40_STATUS_UPDATE_MADATEREQ, DISP_New.getQueueName(),
                    Awaiting_Feedback_On_US.getStatus());
        } else {
            Long workItemID = taskService.failTask(responseHelper.getCorrelationID());
            auditResponse(StatusUpdateResponseFailure, workItemID, payload, responseHelper.getCorrelationID());
        }
    }

    private void processStatusUpdateMandateReqResponse(Post40ResponseData responseHelper, String payload) {
        if (responseHelper.isSuccess()) {
            Long workItemID = taskService.completeTask(responseHelper.getCorrelationID());
            auditResponse(StatusUpdateResponseSuccess, workItemID, payload, responseHelper.getCorrelationID());
        } else {
            Long workItemID = taskService.failTask(responseHelper.getCorrelationID());
            auditResponse(StatusUpdateResponseFailure, workItemID, payload, responseHelper.getCorrelationID());
        }
    }

    private void sendStatusUpdateRequestOutOfSync(Long workItemID, Action action, String previousQueue, String newStatus) {

        WorkItemDto workItemDto = plexusWorkflowAdapter.getWorkItemsById(workItemID);
        workItemDto.setStatus(newStatus);

        StatusUpdateRequestDTO statusUpdateDTO = post40StatusUpdateFactory.generateStatusUpdateRequest(workItemID,
                previousQueue, workItemDto);
        String statusUpdateRequestXML = marshalWithNamespace(statusUpdateDTO.getStatusUpdateRequest());

        taskService.createListIfNotExistWithTask(workItemID, action, statusUpdateDTO.getCorrelationID());
        activeMQAdapter.sendDisputeStatusUpdate(statusUpdateRequestXML, statusUpdateDTO.getCorrelationID());

        auditResponse(StatusUpdateRequest, workItemID, statusUpdateRequestXML, statusUpdateDTO.getCorrelationID());
    }

    private void auditResponse(AuditLogType logType, Long workItemId, String payload, String correlationId) {
        ServiceRequestAuditDTO auditDTO = new ServiceRequestAuditDTO(payload, logType.getAuditLogType(),
                RequestType.DOD, workItemId);
        auditDTO.setCorrelationID(correlationId);

        logService.log(auditDTO);
    }
}
