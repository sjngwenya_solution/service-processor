package za.co.fnb.plexus.services.request.businesslogic.handler.helper.debicheck;

import lombok.Data;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.mandate.response.ApplicationTypeState;
import za.co.fnb.plexus.services.request.dto.mandate.response.MandateResponseDTO;

import static java.util.Optional.ofNullable;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshall;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshallJsonFile;
import static za.co.fnb.plexus.services.request.businesslogic.util.mandate.MandateRequestType.isAmendmentType;
import static za.co.fnb.plexus.services.request.dto.mandate.response.MandateResponseDTO.MandateStatus.*;

@Data
public class DebiCheckResponseProcessor {

    private static final ApplicationTypeState AMENDMENT = new ApplicationTypeState.AmendmentApplication();
    private static final ApplicationTypeState CANCELLATION = new ApplicationTypeState.CancellationApplication();

    private MandateResponseDTO mandateDTO;
    private ServiceRequestAuditDTO auditDTO;

    public DebiCheckResponseProcessor() { }

    public static DebiCheckResponseProcessor create(IncomingResponse response) {
        DebiCheckResponseProcessor data = new DebiCheckResponseProcessor();
        data.extractResponse(response);
        return data;
    }

    public String getApplicationNumber() {
        return mandateDTO.getApplicationNumber();
    }

    public String getEventKey(String requestType) {
        return ofNullable(( (isAmendmentType(requestType)) ? AMENDMENT : CANCELLATION)
                .getEventKey(mandateDTO)).orElse(UPDATE_FAILED.getStatus());
    }

    private void extractResponse(IncomingResponse response) {
       String payload = (String) response.getPayload();

       if(payload.startsWith("<") || payload.contains("xml"))
            this.mandateDTO = unmarshall(payload, MandateResponseDTO.class);
       else
           this.mandateDTO = unmarshallJsonFile(payload, MandateResponseDTO.class);
    }
}
