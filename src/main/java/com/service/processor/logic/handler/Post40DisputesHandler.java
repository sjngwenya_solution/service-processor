package za.co.fnb.plexus.services.request.businesslogic.handler;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.businesslogic.factory.post40.CreateWorkFactory;
import za.co.fnb.plexus.services.request.businesslogic.factory.request.Post40RequestFactory;
import za.co.fnb.plexus.services.request.businesslogic.handler.helper.post40.Post40ResponseData;
import za.co.fnb.plexus.services.request.businesslogic.handler.helper.post40.Post40ResponseHelper;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkInput;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkRequest;
import za.co.fnb.plexus.services.request.dto.dispute.statusupdate.v1.StatusUpdateResponse;
import za.co.fnb.plexus.services.request.infrastructure.adapter.ActiveMQAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Map;

import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.ServiceResponse;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalWithNamespace;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshall;
import static za.co.fnb.plexus.services.request.businesslogic.validation.RequestValidator.ErrorCodes.DUPLICATE_REQUEST;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.CORRELATION_ID;
import static za.co.fnb.plexus.services.request.dto.common.ServiceRequestUtil.buildExternalReferenceNumber;

@Component
public class Post40DisputesHandler extends RequestHandler<CreateWorkRequest> {

    private ActiveMQAdapter activeMQAdapter;
    private CreateWorkFactory createWorkFactory;
    private Post40ResponseHelper post40ResponseHelper;

    public Post40DisputesHandler(@NonNull Post40ResponseHelper post40ResponseHelper,
                                 @NonNull RequestRepository requestRepository,
                                 @NonNull CreateWorkFactory createWorkFactory,
                                 @NonNull ActiveMQAdapter activeMQAdapter,
                                 @NonNull CommandService commandService,
                                 @NonNull Post40RequestFactory factory,
                                 @NonNull AuditLogService logService) {

        super(commandService, logService, factory, requestRepository);
        this.post40ResponseHelper = post40ResponseHelper;
        this.createWorkFactory = createWorkFactory;
        this.activeMQAdapter = activeMQAdapter;
    }

    @Override
    protected CreateWorkRequest transform(Object requestObject) {
        return unmarshall((String) requestObject, CreateWorkRequest.class);
    }

    @Override
    public void handleResponse(IncomingResponse response) {
        StatusUpdateResponse statusUpdateResponse = unmarshall((String) response.getPayload(),
                StatusUpdateResponse.class);

        Post40ResponseData responseData = Post40ResponseData
                .create(statusUpdateResponse, (String) response.getPayload());
        post40ResponseHelper.statusUpdateResponse(responseData);
    }

    @Override
    protected String handleValidationErrors(CreateWorkRequest createWorkRequest, Map<String, String> parameters,
                                Notification validationResults) {

        CreateWorkInput.CustomerInformation customerInformation = createWorkRequest.getCreateWorkInput()
                .getCustomerInformation();

        Object responseMessage;
        if(isDuplicateRequest(validationResults)) {
            responseMessage = createWorkFactory.createDuplicateCreateWorkResponse(parameters.get(CORRELATION_ID));
        }else {
            responseMessage = createWorkFactory.createFailureCreateWorkResponse
                    (validationResults, parameters.get(CORRELATION_ID));
        }
        String failureCreateWorkResponseXml = marshalWithNamespace(responseMessage);

        //Send response.
        activeMQAdapter.sendDisputeCreateWorkResponse(failureCreateWorkResponseXml, parameters.get(CORRELATION_ID));

        String referenceNumber = buildExternalReferenceNumber(customerInformation.getCustomerAccountNumber(),
                customerInformation.getCustomerKeyDate(), customerInformation.getClientItemTiebreaker());

        logService.log(   new ServiceRequestAuditDTO(failureCreateWorkResponseXml, ServiceResponse.getAuditLogType(),
                factory.requestType(), null, parameters.get(CORRELATION_ID), referenceNumber));

        //Return audit reference number
        return referenceNumber;
    }

    @Override
    protected void onServiceRequestCreated(Request request, CreateWorkRequest createWorkRequest) {
        String createWorkResponseXML = marshalWithNamespace(createWorkFactory.createCreateWorkResponse(request.getId(),
                createWorkRequest.getHeader().getCorrelationID()));

        ServiceRequestAuditDTO auditDTO = new ServiceRequestAuditDTO(createWorkResponseXML,
                ServiceResponse.getAuditLogType(), factory.requestType());
        auditDTO.setCorrelationID(createWorkRequest.getHeader().getCorrelationID());
        auditDTO.setReference(request.getExternalReferenceNumber());

        logService.log(auditDTO);

        activeMQAdapter.sendDisputeCreateWorkResponse(createWorkResponseXML,
                createWorkRequest.getHeader().getCorrelationID());
    }

    private boolean isDuplicateRequest(Notification validationResults) {
        return validationResults.getErrors().stream()
                .allMatch(error -> DUPLICATE_REQUEST.equals(error.getCode()));
    }
}
