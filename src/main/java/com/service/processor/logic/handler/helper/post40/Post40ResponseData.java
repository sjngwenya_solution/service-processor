package za.co.fnb.plexus.services.request.businesslogic.handler.helper.post40;

import lombok.Data;
import za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller;
import za.co.fnb.plexus.services.request.dto.dispute.statusupdate.v1.StatusUpdateResponse;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.IntegrationLayerUtil.OUT_OF_SYNC_ERROR_CODE;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.IntegrationLayerUtil.SUCCESS_RESULT;

@Data
public class Post40ResponseData {

    private String correlationID;
    private String resultStatus;
    private String replyCode;

    private String payload;

    public static Post40ResponseData create(StatusUpdateResponse statusUpdateResponse, String rawPayload) {
        Post40ResponseData helper = new Post40ResponseData();
        helper.extractResponseStatusUpdateResponse(statusUpdateResponse, rawPayload);
        return helper;
    }

    public boolean isSuccess() {
        if (SUCCESS_RESULT.equals(resultStatus)) {
            return true;
        } else return false;
    }

    public boolean isOutOfSync() {
        if (OUT_OF_SYNC_ERROR_CODE.equalsIgnoreCase(replyCode)) {
            return true;
        }  return false;
    }


    private void extractResponseStatusUpdateResponse(StatusUpdateResponse statusUpdateResponse, String rawPayload) {
        correlationID = statusUpdateResponse.getHeader().getCorrelationID();
        resultStatus = statusUpdateResponse.getHeader().getResult().getStatus().name();
        replyCode = String.valueOf(statusUpdateResponse.getStatusUpdateReply().getReplyCode());

        payload = rawPayload;
    }
}
