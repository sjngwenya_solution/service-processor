package com.service.processor.logic.template;

public enum TemplateMethodResult {
    Continue,
    Stop;
}
