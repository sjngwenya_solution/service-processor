package com.service.processor.logic.template;


import com.service.processor.dto.BookRequest;
import com.service.processor.logic.factory.RequestDomainFactory;
import com.service.processor.logic.factory.book.BookFactory;
import com.service.processor.logic.utils.Notification;
import com.service.processor.logic.utils.ServiceMarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import static com.service.processor.logic.template.RequestTemplate.REQUEST_TYPE.BOOK_STORE;

@Component
@RefreshScope
public class BookStoreTemplate<DTO> extends  RequestTemplate {

    @Autowired
    private BookFactory bookFactory;


    @Override
    public REQUEST_TYPE getRequestType() {
        return BOOK_STORE;
    }

    @Override
    protected Object unmarshalRequestPayload(Object bookRequest) {
        return ServiceMarshaller.unmarshallJsonFile(bookRequest.toString(), BookRequest.class);
    }

    @Override
    protected Notification validateRequest(Object object) {
        return validator.validateBookRequest( ((BookRequest) object) );
    }

    @Override
    protected TemplateMethodResult reportValidationErrorIfExists(Object object, Notification validationResults, String requestReference) {
        System.out.println( "****************** book service *************");
        if (!validationResults.hasErrors()) return TemplateMethodResult.Continue;

       /* Request serviceRequest = getDomainFactory().createServiceRequestDomainModel(requestDTO );
        serviceRequest.setStatus(FAILED_VALIDATION);
        requestRepository.save(serviceRequest);

        ServiceRequestAuditDTO ado = new ServiceRequestAuditDTO();
        ado.setPayload(validationResults.getFirstErrorMessage());
        ado.setPayloadType(AdoValidationErrors.getAuditLogType());
        ado.setExternalReferenceNumber( ((AdoInputDTO) requestDTO).getPayload().getDebitOrderReference() );
        ado.setCorrelationID(String.valueOf(serviceRequest.getId())); //Only reference we have, perhaps rename the column?
        auditLogService.logAfterDebitOrderTransaction(ado);*/
        System.out.println( "****************** Audit book service **************"+validationResults.getFirstErrorMessage());
        return TemplateMethodResult.Stop;
    }

    @Override
    protected TemplateMethodResult checkRequestDuplicates(Object object, String requestReference) {
        return null;
    }

    @Override
    protected RequestDomainFactory getDomainFactory() {
        return bookFactory;
    }
}
