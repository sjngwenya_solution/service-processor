package com.service.processor.logic.template;


import com.service.processor.domain.Book;
import com.service.processor.domain.service.Service;
import com.service.processor.infastructure.repository.ServiceRepository;
import com.service.processor.logic.factory.RequestDomainFactory;
import com.service.processor.logic.service.io.IncomingRequest;
import com.service.processor.logic.utils.Notification;
import com.service.processor.logic.validation.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.jms.support.JmsHeaders.CORRELATION_ID;

public abstract class RequestTemplate<DTO> {
    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    RequestValidator validator;

    public void process(IncomingRequest ir) {

        DTO dto = unmarshalRequestPayload(ir.getPayload());
        Notification validationResults = validateRequest(dto);

        if (TemplateMethodResult.Stop.equals(reportValidationErrorIfExists(dto, validationResults, ir.getParam(CORRELATION_ID)))
                || TemplateMethodResult.Stop.equals(checkRequestDuplicates(dto, ir.getParam(CORRELATION_ID)))) {
            return;
        }


        Service service = getDomainFactory().createDomainModel(dto);

    }

    private void saveRequest(Service request, Object requestPayload, String requestReference) {

        Service savedRequest = serviceRepository.save(request);

        auditRequest(savedRequest, requestPayload, requestReference);
    }

    public abstract REQUEST_TYPE getRequestType();

    public enum REQUEST_TYPE {
        BOOK_STORE("BOOK_STORE", "Book Store");

        String type;
        String description;

        REQUEST_TYPE(String type, String description) {
            this.type = type;
            this.description = description;
        }

        public String getType() {
            return type;
        }

        public String getDescription() {
            return description;
        }
    }

    protected abstract DTO unmarshalRequestPayload(Object request);

    protected abstract Notification validateRequest(DTO dto);


    protected abstract TemplateMethodResult reportValidationErrorIfExists(DTO dto,
                                                                          Notification validationResults,
                                                                          String requestReference);
    protected abstract TemplateMethodResult checkRequestDuplicates(DTO dto, String requestReference);

    protected abstract RequestDomainFactory getDomainFactory();
    protected void auditRequest(Service service, Object payload, String requestReference) {
    }
}
