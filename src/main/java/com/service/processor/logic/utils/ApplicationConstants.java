package com.service.processor.logic.utils;

public final class ApplicationConstants {

    public static final String PRICE = "price";
    public static final String QUANTITY= "quantity";
    public static final String TITLE= "title";
    public static final String TYPE = "type";

}
