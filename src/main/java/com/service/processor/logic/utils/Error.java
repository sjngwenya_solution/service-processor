package com.service.processor.logic.utils;


public class Error {
    private String code;
    private String description;

    public Error(String description) {
        this("", description);
    }

    public Error(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public boolean isValid() {
        if (this.getDescription() != null || this.getDescription() != "") {
            return true;
        }
        return false;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            Error error = (Error) o;
            if (this.code != null) {
                if (this.code.equals(error.code)) {
                    return this.description != null ? this.description.equals(error.description) : error.description == null;
                }
            } else if (error.code == null) {
                return this.description != null ? this.description.equals(error.description) : error.description == null;
            }

            return false;
        } else {
            return false;
        }
    }

    public String toString() {
        return "Error{code='" + this.code + '\'' + ", description='" + this.description + '\'' + '}';
    }
}
