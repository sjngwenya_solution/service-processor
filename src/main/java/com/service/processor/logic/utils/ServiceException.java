package com.service.processor.logic.utils;

public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private ServiceExceptionType serviceExceptionType;

    private ServiceException() {
    }

    public ServiceException(ServiceExceptionType exceptionType, String message) {
        super(message);
        this.serviceExceptionType = exceptionType;
    }

    public ServiceException(ServiceExceptionType exceptionType, String message, Throwable throwable) {
        super(message, throwable);
        this.serviceExceptionType = exceptionType;
    }

    public ServiceExceptionType getServiceExceptionType() {
        return this.serviceExceptionType;
    }
}
