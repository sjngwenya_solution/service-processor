package com.service.processor.logic.utils;



import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class Notification {
    private Set<Error> errors = new HashSet();

    public Notification() {
    }

    public void addError(String errorDescription) {
        if (errorDescription!=null || errorDescription !="") {
            this.errors.add(new Error(errorDescription));
        }

    }

    public void addError(String errorCode, String errorDescription) {
        if (errorDescription!=null || errorDescription !="") {
            this.errors.add(new Error(errorCode, errorDescription));
        }

    }

  /*  public void addError(Exception exception) {
        this.addError((Exception)exception, (String)null);
    }*/

    public void addError(Error error) {
        if (error != null) {
            this.errors.add(error);
        }

    }

    public void addErrors(Notification notification) {
        if (notification.hasErrors()) {
            this.errors.addAll(notification.getErrors());
        }

    }

    public void addErrors(List<Error> errors) {
        if (errors != null && !errors.isEmpty() && !this.errors.containsAll(errors)) {
            this.errors.addAll(errors);
        }

    }

  /*  public void addError(Exception exception, String errorDescription) {
        if (exception != null) {
            Throwable rootCause = ExceptionUtils.getRootCause(exception);
            Exception cause = rootCause != null ? (Exception)rootCause : exception;
            if (StringUtils.isNotBlank(errorDescription)) {
                this.errors.add(new Error(errorDescription));
            } else if (StringUtils.isBlank(cause.getMessage())) {
                this.errors.add(new Error(ExceptionUtils.getStackTrace(cause)));
            } else if (StringUtils.isNotBlank(cause.getMessage())) {
                this.errors.add(new Error(cause.getMessage()));
            }

        }
    }*/

    public Set<Error> getErrors() {
        return this.errors;
    }

    public boolean hasErrors() {
        return !this.errors.isEmpty();
    }

    public boolean hasError(String errorMessage) {
        if (!this.hasErrors()) {
            return false;
        } else {
            AtomicBoolean hasError = new AtomicBoolean(false);
            this.errors.parallelStream().forEach((error) -> {
                if (error.getDescription().equalsIgnoreCase(errorMessage)) {
                    hasError.set(true);
                }

            });
            return hasError.get();
        }
    }

    public boolean hasNoErrors() {
        return !this.hasErrors();
    }

    public Error getFirstError() {
        return this.hasErrors() ? (Error)this.getErrors().iterator().next() : null;
    }

    public String getFirstErrorMessage() {
        return this.hasErrors() ? ((Error)this.getErrors().iterator().next()).getDescription() : null;
    }
}
