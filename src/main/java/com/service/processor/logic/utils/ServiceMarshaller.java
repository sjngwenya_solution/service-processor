package com.service.processor.logic.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.experimental.UtilityClass;

import javax.xml.bind.JAXB;
import java.io.StringWriter;

@UtilityClass
public class ServiceMarshaller {

    public static <T> T unmarshall(String xmlString, Class<T> clazz) {
        try {

            XmlMapper mapper = new XmlMapper();
            return mapper.readValue(xmlString, clazz);

        } catch (Exception e) {
            throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, e.getMessage(), e);
        }
    }

    public static <T> T unmarshallIgnoreUnknownFields(String xmlString, Class<T> clazz) {
        try {

            XmlMapper mapper = new XmlMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return mapper.readValue(xmlString, clazz);

        } catch (Exception e) {
            throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, e.getMessage(), e);
        }
    }

    public static String marshal(Object object) {

        try {
            XmlMapper mapper = new XmlMapper();
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new ServiceException(ServiceExceptionType.UNKNOWN, e.getMessage(), e);
        }
    }

    public static <T> String marshalWithNamespace(T input) {
        StringWriter writer = new StringWriter();
        JAXB.marshal(input, writer);
        return writer.toString();
    }
    public static <T> T unmarshallJsonFile(String jsonString, Class<T> clazz) {
        try {

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return objectMapper.readValue(jsonString, clazz);

        } catch (Exception e) {
            throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, e.getMessage(), e);
        }
    }
    public static <T> String marshalObjectToJson(T input) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(input);

        } catch (Exception e) {
            throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, e.getMessage(), e);
        }
    }
}
