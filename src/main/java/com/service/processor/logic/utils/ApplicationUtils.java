package com.service.processor.logic.utils;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

public class ApplicationUtils {

    public static String formatAmount(double amount) {
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#0.00", formatSymbols);
        return decimalFormat.format(amount);
    }

    private boolean isAmount() {
        String string = "1010.55";
        return string.matches("-?\\d+(\\.\\d+)?");
    }

    public void interest(String interestRate) {
        if (interestRate != null || interestRate != "") {
            String iRate = "";
            interestRate = interestRate.replace(".", "");
            if (interestRate.startsWith("1")) {
                iRate = interestRate.length() > 1 ? interestRate.substring(0, 2) + "." + interestRate.substring(2) :
                        interestRate.substring(0, 1);
            }
            if (String.valueOf(interestRate.charAt(0)).matches("^[2-9]")) {
                iRate = interestRate.substring(0, 1) + "." + interestRate.substring(1);
            }
        }
    }

}
