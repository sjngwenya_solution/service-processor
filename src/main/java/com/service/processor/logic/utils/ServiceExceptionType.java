package com.service.processor.logic.utils;

public enum  ServiceExceptionType {
    DUPLICATE,
    INCOMPLETE_REQUEST,
    INVALID_REQUEST,
    NOT_FOUND,
    UNKNOWN,
    ASYNC_PROCESSING,
    ASYNC_FAILED,
    AUTHENTICATION;

    private ServiceExceptionType() {
    }
}
