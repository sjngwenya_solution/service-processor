package za.co.fnb.plexus.services.request.businesslogic.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkInput;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkInput.CustomerInformation;
import za.co.fnb.plexus.services.request.dto.dispute.creatework.v1.CreateWorkRequest;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.List;
import java.util.Set;

import static java.util.Objects.nonNull;
import static za.co.fnb.plexus.services.request.businesslogic.validation.RequestValidator.ErrorCodes.DUPLICATE_REQUEST;
import static za.co.fnb.plexus.services.request.dto.common.ServiceRequestUtil.buildExternalReferenceNumber;


@Component
public class Post40RequestValidator extends RequestValidator<CreateWorkRequest> {

    @Value("${plexus.debit.order.dispute.vaild.service.provider.types}")
    private List<String> validServiceProviderTypes;

    @Autowired RequestRepository requestRepository;

    @Override
    protected void runRules(Notification notification, CreateWorkRequest createWorkRequest) {

        runDataValidation(notification, createWorkRequest);

        if(notification.hasNoErrors()) {
            runDuplicateValidation(notification, createWorkRequest.getCreateWorkInput().getCustomerInformation());
        }
    }

    private void runDataValidation(Notification notification, CreateWorkRequest createWorkRequest) {
        if (createWorkRequest.getCreateWorkInput() == null) {
            notification.addError("");
        }

        if (StringUtils.isBlank(createWorkRequest.getCreateWorkInput().getChannelId())) {
            notification.addError("Channel id required.");
        }

        //Customer Information Validation
        CustomerInformation customerInformation = createWorkRequest.getCreateWorkInput().getCustomerInformation();
        if (nonNull(customerInformation)) {
            if (StringUtils.isBlank(customerInformation.getCustomerIDType())) {
                notification.addError("Customer ID type required");
            }
            if (StringUtils.isBlank(customerInformation.getCustomerProductCode())) {
                notification.addError("Customer product code required");
            }
            if (StringUtils.isBlank(customerInformation.getCustomerAccountNumber())) {
                notification.addError("Customer account number required");
            }
            if (customerInformation.getCustomerKeyDate() == null) {
                notification.addError("Customer keydate required");
            }
            if (customerInformation.getClientItemTiebreaker() == null) {
                notification.addError("Customer TieBreaker required");
            }
        }else {
            notification.addError("Customer information required");
        }

        //Dispute Data Information Validation
        CreateWorkInput.DisputedataInformation disputeDataInformation = createWorkRequest.getCreateWorkInput().getDisputedataInformation();
        if (nonNull(disputeDataInformation)) {
            if (StringUtils.isBlank(disputeDataInformation.getDisputedCompanyName())) {
                notification.addError("Dispute company name required.");
            }
            if (StringUtils.isBlank(disputeDataInformation.getDisputedDebitOrderReference())) {
                notification.addError("Dispute debit order reference required.");
            }
            if (disputeDataInformation.getDisputedDebitOrderDate() == null) {
                notification.addError("Dispute debit order date required.");
            }
            if (StringUtils.isBlank(disputeDataInformation.getDisputedDebitOrderCurrency())) {
                notification.addError("Dispute debit order currency required.");
            }
            if (disputeDataInformation.getDisputedDebitOrderAmount() == null) {
                notification.addError("Dispute debit order amount required.");
            }
            if (StringUtils.isBlank(disputeDataInformation.getDisputeReasonCode())) {
                notification.addError("Dispute reason code required.");
            }
            if (StringUtils.isBlank(disputeDataInformation.getDisputeReasonCodeDescription())) {
                notification.addError("Dispute reason code description required.");
            }
        }else {
            notification.addError("Dispute data required");
        }

        //Customer Enrich Information Validation
        CreateWorkInput.CustomerEnrichInformation enrichInformation = createWorkRequest.getCreateWorkInput().getCustomerEnrichInformation();
        if (nonNull(enrichInformation)) {
            if (StringUtils.isBlank(enrichInformation.getUniqueCustomerNumber())) {
                notification.addError("UCN required");
            }
            if (StringUtils.isBlank(enrichInformation.getName())) {
                notification.addError("Customer name required");
            }
        }else {
            notification.addError("Enrichment Information required");
        }

        //Account Information Validation
        CreateWorkInput.AccountInformation accountInformation = createWorkRequest.getCreateWorkInput().getAccountInformation();
        if (nonNull(accountInformation)) {
            if (accountInformation.getDisputerInformation() == null) {
                notification.addError("Account information required.");
            }
            if (StringUtils.isBlank(accountInformation.getDisputerInformation().getDisputerAccountName())) {
                notification.addError("Disputer Account name required.");
            }
            if (StringUtils.isBlank(accountInformation.getDisputerInformation().getDisputerAccountType())) {
                notification.addError("Disputer account type required.");
            }
            if (StringUtils.isBlank(accountInformation.getDisputerInformation().getDisputerBranch())) {
                notification.addError("Disputer branch required.");
            }
            if (StringUtils.isBlank(accountInformation.getDisputerInformation().getDisputerBankName())) {
                notification.addError("Disputer bank name required.");
            }

            //Service provider details
            if (accountInformation.getServiceProviderInformation() == null) {
                notification.addError("Service provider details required.");
            }else {
                if (StringUtils.isBlank(accountInformation.getServiceProviderInformation().getServiceProviderAccountName())) {
                    notification.addError("Service provider account name required.");
                }
                if (StringUtils.isBlank(accountInformation.getServiceProviderInformation().getServiceProviderAccountNumber())) {
                    notification.addError("Service provider account number required.");
                }
                if (StringUtils.isBlank(accountInformation.getServiceProviderInformation().getServiceProviderAccountType())) {
                    notification.addError("Service provider account type required.");
                }
                if (StringUtils.isBlank(accountInformation.getServiceProviderInformation().getServiceProviderBranch())) {
                    notification.addError("Service provider branch required.");
                }
                if (StringUtils.isBlank(accountInformation.getServiceProviderInformation().getServiceProviderBankName())) {
                    notification.addError("Service provider bank name required.");
                }
                if (StringUtils.isBlank(accountInformation.getServiceProviderType()) ||
                        !validServiceProviderTypes.contains(accountInformation.getServiceProviderType().toUpperCase())) {
                    notification.addError("Invalid Service Provider Type");
                }
            }

        }else {
            notification.addError("Account information required");
        }
    }

    private void runDuplicateValidation(Notification notification, CustomerInformation customerInformation) {
        String externalReferenceNumber = buildExternalReferenceNumber(customerInformation.getCustomerAccountNumber(),
                        customerInformation.getCustomerKeyDate(), customerInformation.getClientItemTiebreaker());

        Set<Request> requestSet = requestRepository.findByExternalReferenceNumberAndStatus(externalReferenceNumber,
                RequestStatus.openRequestStatuses());

        if (nonNull(requestSet) && !requestSet.isEmpty()) {
            notification.addError(DUPLICATE_REQUEST , "Duplicate request");
        }
    }
}
