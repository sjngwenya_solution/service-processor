package za.co.fnb.plexus.services.request.businesslogic.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.dto.notification.DocumentNotification;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;
@Component
public class DocumentNotificationValidator {
    @Autowired
    Validator validator;
    public Notification validateDocumentNotification(DocumentNotification documentNotification) {
        Notification notification = new Notification();

        Set<ConstraintViolation<DocumentNotification>> violations = validator.validate(documentNotification);
        violations.stream().map(ConstraintViolation::getMessage).forEach(notification::addError);
        return notification;

    }
}
