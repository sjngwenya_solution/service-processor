package com.service.processor.logic.validation;

import com.service.processor.dto.BookRequest;
import com.service.processor.logic.utils.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Component
public class RequestValidator {

    @Autowired
    Validator validator;

    public Notification validateBookRequest(BookRequest bookRequest) {
        Notification notification = new Notification();

        Set<ConstraintViolation<BookRequest>> violations = validator.validate(bookRequest);
        violations.stream().map(ConstraintViolation::getMessage).forEach(notification::addError);
        return notification;
    }

}
