package za.co.fnb.plexus.services.request.businesslogic.validation;

import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.dto.mandate.creatework.DebitCheckMandateRequest;

import java.util.Arrays;

@Component
public class DebiCheckMandateRequestValidator extends RequestValidator<DebitCheckMandateRequest> {

    @Override
    protected void runRules(Notification validationErrors, DebitCheckMandateRequest debitCheckMandateRequest) {

        notEmptyValidation("Application type", debitCheckMandateRequest.getApplicationDetails().getApplicationType(), validationErrors);
        notEmptyValidation("Company ID", debitCheckMandateRequest.getApplicationDetails().getCompanyId(), validationErrors);
        notEmptyValidation("Product ID", debitCheckMandateRequest.getApplicationDetails().getProduct(), validationErrors);
        notEmptyValidation("Sub product", debitCheckMandateRequest.getApplicationDetails().getSubProduct(), validationErrors);
        notEmptyValidation("Application source", debitCheckMandateRequest.getApplicationDetails().getApplicationSource(), validationErrors);
        notEmptyValidation("Application number", debitCheckMandateRequest.getApplicationDetails().getApplicationNumber(), validationErrors);
        notEmptyValidation("Limit amount", debitCheckMandateRequest.getApplicationDetails().getLimitAmount(), validationErrors);
        notEmptyValidation("Application channel", debitCheckMandateRequest.getApplicationDetails().getApplicationChannel(), validationErrors);

        notEmptyValidation("Creditor account number", debitCheckMandateRequest.getCreditorDetails().getCreditorAccountNumber(), validationErrors);
        notEmptyValidation("Creditor name", debitCheckMandateRequest.getCreditorDetails().getCreditorName(), validationErrors);
        notEmptyValidation("Creditor Abb name",debitCheckMandateRequest.getCreditorDetails().getCreditorAbbName(), validationErrors);
        notEmptyValidation("Creditor account type", debitCheckMandateRequest.getCreditorDetails().getCreditorAccountType(), validationErrors);
        notEmptyValidation("Creditor branch", debitCheckMandateRequest.getCreditorDetails().getCreditorBranch(), validationErrors);

        notEmptyValidation("Collection currency", debitCheckMandateRequest.getCollectionDetails().getMaximumCollectionCurrency(), validationErrors);
        notEmptyValidation("Collection day", debitCheckMandateRequest.getCollectionDetails().getCollectionDay(), validationErrors);

        notEmptyValidation("Customer number",debitCheckMandateRequest.getDebtorDetails().getCustomerNumber(), validationErrors);
        notEmptyValidation("Debtor account number", debitCheckMandateRequest.getDebtorDetails().getDebtorAccountNo(), validationErrors);
        notEmptyValidation("Debtor debtor branch", debitCheckMandateRequest.getDebtorDetails().getDebtorBranch(), validationErrors);

        valueInRange("Application Type",  debitCheckMandateRequest.getApplicationDetails().getApplicationType(),
                Arrays.asList("MNT-DATE", "MND-CNCL"), validationErrors);
    }
}
