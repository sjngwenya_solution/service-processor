package za.co.fnb.plexus.services.request.businesslogic.validation;

import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.dto.dispute.AdoInputDTO;
import za.co.fnb.plexus.services.request.dto.dispute.AfterDebitOrderRequestDTO;

import java.util.Arrays;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.PAYMENT_REVERSAL_INSTRUCTION;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.STOP_AND_REVERSE_PAYMENT_INSTRUCTION;

@Component
public class AdoRequestValidator extends RequestValidator<AdoInputDTO>{
    
    @Override
    protected void runRules(Notification validationErrors, AdoInputDTO adoInputDTO) {
        AfterDebitOrderRequestDTO afterDebitOrderRequestDTO = adoInputDTO.getPayload();
        
        if (equalsIgnoreCase(PAYMENT_REVERSAL_INSTRUCTION, afterDebitOrderRequestDTO.getServiceInstruction())
                || equalsIgnoreCase(STOP_AND_REVERSE_PAYMENT_INSTRUCTION, afterDebitOrderRequestDTO.getServiceInstruction())) {
            notEmptyValidation("Customer Type", afterDebitOrderRequestDTO.getCustomerTypeIndicator(), validationErrors);
        }

        notEmptyValidation("Request Instruction", afterDebitOrderRequestDTO.getServiceInstruction(), validationErrors);
        notEmptyValidation("Account number", afterDebitOrderRequestDTO.getAccountNumber(), validationErrors);
        notEmptyValidation("Unique Customer number", afterDebitOrderRequestDTO.getUniqueCustomerNumber(), validationErrors);
        notEmptyValidation("Debit order date", afterDebitOrderRequestDTO.getDebitOrderDate(), validationErrors);
        notEmptyValidation("Tran amount low", afterDebitOrderRequestDTO.getTransactionAmountLow(), validationErrors);
        notEmptyValidation("Tran amount high", afterDebitOrderRequestDTO.getTransactionAmountHigh(), validationErrors);
        notEmptyValidation("Reason code description", afterDebitOrderRequestDTO.getReasonCodeDescription(), validationErrors);
        notEmptyValidation("Debit order amount", afterDebitOrderRequestDTO.getDebitOrderAmount(), validationErrors);
        notEmptyValidation("Debit order reference", afterDebitOrderRequestDTO.getDebitOrderReference(), validationErrors);
        notEmptyValidation("Sequence number", afterDebitOrderRequestDTO.getSequenceNumber(), validationErrors);
        notEmptyValidation("Product type", afterDebitOrderRequestDTO.getProductType(), validationErrors);
        notEmptyValidation("Sub product", afterDebitOrderRequestDTO.getSubProduct(), validationErrors);
        notEmptyValidation("Product requested", afterDebitOrderRequestDTO.getProductRequested(), validationErrors);
        notEmptyValidation("Product Instrument", afterDebitOrderRequestDTO.getProductInstrument(), validationErrors);
        notEmptyValidation("Dispute reason code", afterDebitOrderRequestDTO.getDisputeReasonCode(), validationErrors);
        notEmptyValidation("Reason code description", afterDebitOrderRequestDTO.getReasonCodeDescription(), validationErrors);
        notEmptyValidation("Africa sub indicator", afterDebitOrderRequestDTO.getAfricaSubIndicator(), validationErrors);
        notEmptyValidation("User abbr Indicator", afterDebitOrderRequestDTO.getUserAbbrIndicator(), validationErrors);
        notEmptyValidation("Charge code", afterDebitOrderRequestDTO.getChargeCode(), validationErrors);
        notEmptyValidation("Source id", afterDebitOrderRequestDTO.getSourceID(), validationErrors);
        notEmptyValidation("Customer type Id", afterDebitOrderRequestDTO.getCustomerTypeIndicator(), validationErrors);
        notEmptyValidation("customer tie breaker", afterDebitOrderRequestDTO.getCustomerTieBreaker(), validationErrors);
        notEmptyValidation("Nu ber of transactions", afterDebitOrderRequestDTO.getNumberOfTransactions(), validationErrors);
        notEmptyValidation("Key date", afterDebitOrderRequestDTO.getKeyDate(), validationErrors);
        notEmptyValidation("Company name", afterDebitOrderRequestDTO.getCompanyName(), validationErrors);

        decimalTypeValidation("Transaction Amt High", afterDebitOrderRequestDTO.getTransactionAmountHigh(), validationErrors);
        decimalTypeValidation("Transaction Amt Low", afterDebitOrderRequestDTO.getTransactionAmountLow(), validationErrors);
        decimalTypeValidation("Debit order Amt", afterDebitOrderRequestDTO.getDebitOrderAmount(), validationErrors);

        integerTypeValidation("Debit order date", afterDebitOrderRequestDTO.getDebitOrderDate(), validationErrors);
        integerTypeValidation("Customer tie breaker", afterDebitOrderRequestDTO.getCustomerTieBreaker(), validationErrors);
        integerTypeValidation("Number of transactions", afterDebitOrderRequestDTO.getNumberOfTransactions(), validationErrors);
        integerTypeValidation("Key date", afterDebitOrderRequestDTO.getKeyDate(), validationErrors);

        valueInRange("Request Instruction", afterDebitOrderRequestDTO.getServiceInstruction(),
                Arrays.asList("SP", "P", "S"), validationErrors);
    }
}
