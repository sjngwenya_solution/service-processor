package za.co.fnb.plexus.services.request.businesslogic.command;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import za.co.fnb.cbs.common.business.shared.dto.WorkItemDto;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.base.exception.ServiceExceptionType;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.commands.ChangeProcessStatusCommand;
import za.co.fnb.plexus.services.request.infrastructure.adapter.PlexusWorkflowAdapter;

import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

@Component
public class ChangeProcessStatusCommandHandler extends CommandHandler<ChangeProcessStatusCommand> {

    private PlexusWorkflowAdapter workflowAdapter;

    public ChangeProcessStatusCommandHandler(@NonNull CommandService commandService,
                                             @NonNull PlexusWorkflowAdapter workflowAdapter) {
        super(commandService);
        this.workflowAdapter = workflowAdapter;
    }

    @Override
    public void handle(ChangeProcessStatusCommand applicationCommand) {
        WorkItemDto workItemDto = workflowAdapter.getWorkItemsById(applicationCommand.getWorkItemReference());

        if (workItemDto == null)
            throw new ServiceException(ServiceExceptionType.NOT_FOUND, "WorkItem not found for ID" + applicationCommand.getWorkItemReference());

        Optional<String> match = applicationCommand.getSkipUpdateForStates()
                .stream()
                .filter(s -> equalsIgnoreCase(s, workItemDto.getStatus()))
                .findFirst();

        if (match.isPresent())
            return;

        if (!StringUtils.equals(workItemDto.getStatus(), applicationCommand.getStatus())) {
            workflowAdapter.changeWorkItemStatus(applicationCommand.getWorkItemReference(), applicationCommand.getStatus(),
                    applicationCommand.getStatus());
        }
    }
}
