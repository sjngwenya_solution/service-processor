package za.co.fnb.plexus.services.request.businesslogic.command;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.commands.ChangeRequestStatusCommand;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;

@Component
public class ChangeRequestStatusCommandHandler extends CommandHandler<ChangeRequestStatusCommand> {

    private  RequestRepository requestRepository;
    public ChangeRequestStatusCommandHandler(@NonNull CommandService commandService,
                                             @NonNull RequestRepository requestRepository) {
        super(commandService);
        this.requestRepository = requestRepository;
    }

    @Override
    public void handle(ChangeRequestStatusCommand applicationCommand) {

        Request request = requestRepository.findByWorkItemId(applicationCommand.getWorkItemReference());
        RequestStatus status = RequestStatus.valueOf(applicationCommand.getRequestStatus());

        switch (status) {
            case OPEN : request.open();
                break;
            case FAILED: request.fail();
                break;
            case CANCELLED : request.cancel();
                break;
            case COMPLETED : request.complete();
                break;
            case DUPLICATE : request.duplicate();
                break;
            case FAILED_VALIDATION : request.failedValidation();
                break;
            default:
                throw new ServiceException(INVALID_REQUEST, "Status not mapped to request " + status);
        }
        requestRepository.save(request);
    }
}
