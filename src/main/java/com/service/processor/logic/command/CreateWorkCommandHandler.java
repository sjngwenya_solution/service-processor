package za.co.fnb.plexus.services.request.businesslogic.command;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.commands.CreateWorkCommand;
import za.co.fnb.plexus.services.request.infrastructure.adapter.ActiveMQAdapter;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalObjectToJson;

@Component
public class CreateWorkCommandHandler extends CommandHandler<CreateWorkCommand>{

    private String queueName;
    private ActiveMQAdapter activeMQAdapter;
    public CreateWorkCommandHandler(@NonNull CommandService commandService,
                                    @NonNull ActiveMQAdapter activeMQAdapter,
                                    @Value("${plexus.work_item.create.request.queue.name}") String queueName){
        super(commandService);
        this.activeMQAdapter = activeMQAdapter;
        this.queueName = queueName;
    }

    @Override
    public void handle(CreateWorkCommand applicationCommand) {
        activeMQAdapter.sendTransactedMessage(queueName, marshalObjectToJson(applicationCommand));
    }
}
