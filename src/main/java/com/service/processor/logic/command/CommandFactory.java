package za.co.fnb.plexus.services.request.businesslogic.command;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.base.exception.ServiceExceptionType;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction;
import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.ReversePaymentCommand;
import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.StopPaymentCommand;
import za.co.fnb.plexus.services.request.businesslogic.factory.ado.PaymentReversalFactory;
import za.co.fnb.plexus.services.request.businesslogic.factory.ado.StopPaymentFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.infrastructure.adapter.ActiveMQAdapter;

import java.util.HashMap;
import java.util.Map;

import static za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction.PAYMENT_REVERSAL;
import static za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction.STOP_PAYMENT;

@Component
@RequiredArgsConstructor
public class CommandFactory {

    @NonNull
    protected TaskService taskService;
    @NonNull
    private PaymentReversalFactory reversalFactory;
    @NonNull
    private StopPaymentFactory stopPaymentFactory;
    @NonNull
    private ActiveMQAdapter activeMQAdapter;
    @NonNull
    private AuditLogService auditLogService;

    public ServiceCommand create(MainframeFunction mainframeFunction, long workItemId) {
        if(STOP_PAYMENT == mainframeFunction)
            return new StopPaymentCommand(workItemId, activeMQAdapter, taskService, stopPaymentFactory, auditLogService);

        if(PAYMENT_REVERSAL == mainframeFunction)
            return new ReversePaymentCommand(workItemId, activeMQAdapter, taskService, reversalFactory, auditLogService);

        throw new ServiceException(ServiceExceptionType.NOT_FOUND, "Action type not defined for ServiceCommand : " +
                mainframeFunction);
    }

    @UtilityClass
    public static class CommandToEventMapping {

        public static final Map<Class, String> EVENT_MAPPING = new HashMap<>();
        static {
            EVENT_MAPPING.put(ServiceCommand.StopPaymentCommand.class, "StopPaymentRequestedDomainEvent");
            EVENT_MAPPING.put(ServiceCommand.ReversePaymentCommand.class, "ReversePaymentRequestedDomainEvent");
        }

        public static <T> String getEventForCommand(Class<T> command) {
            return EVENT_MAPPING.get(command);
        }
    }
}
