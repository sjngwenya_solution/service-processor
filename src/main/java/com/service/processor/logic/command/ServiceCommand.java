package za.co.fnb.plexus.services.request.businesslogic.command;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType;
import za.co.fnb.plexus.services.request.businesslogic.factory.ado.PaymentReversalFactory;
import za.co.fnb.plexus.services.request.businesslogic.factory.ado.StopPaymentFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.domain.task.Action;
import za.co.fnb.plexus.services.request.dto.ado.paymentreversal.PaymentReversalRequest;
import za.co.fnb.plexus.services.request.dto.ado.stop.payment.CreateStopPaymentRequest;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;
import za.co.fnb.plexus.services.request.infrastructure.adapter.ActiveMQAdapter;

import static za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction.*;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.marshalWithNamespace;
import static za.co.fnb.plexus.services.request.domain.task.Action.REVERSE_PAYMENT;

@Getter
@RequiredArgsConstructor
public abstract class ServiceCommand {

    public enum MainframeFunction { STOP_PAYMENT, PAYMENT_REVERSAL }

    @NonNull
    private Long workItemReference;

    @NonNull
    protected ActiveMQAdapter mqAdapter;
    @NonNull
    private MainframeFunction mainframeFunction;
    @NonNull
    protected TaskService taskService;

    public abstract void execute();

    protected void addChecklistItem(Action actionPerformed, String correlationId) {
        taskService.createListIfNotExistWithTask(getWorkItemReference(), actionPerformed, correlationId);
    }

    public static class StopPaymentCommand extends ServiceCommand {

        private StopPaymentFactory stopFactory;
        private AuditLogService auditLogService;

        public StopPaymentCommand(@NonNull Long workItemReference,
                                  @NonNull ActiveMQAdapter mqAdapter,
                                  @NonNull TaskService taskService,
                                  StopPaymentFactory stopFactory,
                                  AuditLogService auditLogService) {

            super(workItemReference, mqAdapter, STOP_PAYMENT, taskService);
            this.stopFactory = stopFactory;
            this.auditLogService = auditLogService;
        }

        @Override
        public void execute() {
            CreateStopPaymentRequest stopPaymentRequest = stopFactory.createStopPaymentRequest(getWorkItemReference());
            String stopPaymentRequestXML = marshalWithNamespace(stopPaymentRequest);

            addChecklistItem(Action.STOP_PAYMENT, stopPaymentRequest.getHeader().getCorrelationID());
            mqAdapter.sendADOStopPaymentRequest(stopPaymentRequestXML, stopPaymentRequest.getHeader().getCorrelationID());

            auditLogService.log(new ServiceRequestAuditDTO(
                    stopPaymentRequestXML, AuditLogType.CreateStopPaymentRequest.getAuditLogType(), RequestType.ADO,
                    getWorkItemReference(),
                    stopPaymentRequest.getHeader().getReferenceID(), stopPaymentRequest.getHeader().getCorrelationID()));
        }
    }

    public static class ReversePaymentCommand extends ServiceCommand {

        private PaymentReversalFactory reversalFactory;
        private AuditLogService auditLogService;

        public ReversePaymentCommand(@NonNull Long workItemReference,
                                     @NonNull ActiveMQAdapter mqAdapter,
                                     @NonNull TaskService taskService,
                                     PaymentReversalFactory reversalFactory,
                                     AuditLogService auditLogService) {

            super(workItemReference, mqAdapter, PAYMENT_REVERSAL, taskService);
            this.reversalFactory = reversalFactory;
            this.auditLogService = auditLogService;
        }
        @Override
        public void execute() {
            PaymentReversalRequest reversalRequest = reversalFactory.createReversePaymentRequest(getWorkItemReference());
            String paymentReversalRequestXML =  marshalWithNamespace(reversalRequest);

            addChecklistItem(REVERSE_PAYMENT, reversalRequest.getHeader().getCorrelationID());
            mqAdapter.sendADOPaymentReversalRequest(paymentReversalRequestXML, reversalRequest.getHeader().getCorrelationID());

            auditLogService.log(new ServiceRequestAuditDTO(paymentReversalRequestXML,
                    AuditLogType.PaymentReversalRequest.getAuditLogType(), RequestType.ADO, getWorkItemReference(),
                    reversalRequest.getHeader().getReferenceID(), reversalRequest.getHeader().getCorrelationID()));
        }
    }
}
