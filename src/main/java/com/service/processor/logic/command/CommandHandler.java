package za.co.fnb.plexus.services.request.businesslogic.command;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.InitializingBean;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.commands.ApplicationCommand;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;

@RequiredArgsConstructor
public abstract class CommandHandler<T extends ApplicationCommand> implements InitializingBean {

    @NonNull protected CommandService commandService;

    public abstract void handle(T applicationCommand);

    @SneakyThrows
    void register() {
        ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
        Type[] arguments = parameterizedType.getActualTypeArguments();

        if(isNotEmpty(arguments))
        {
            Type argument = arguments[0];
            Class clazz = Class.forName(argument.getTypeName());
            commandService.registerHandler(clazz, this);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        register();
    }
}
