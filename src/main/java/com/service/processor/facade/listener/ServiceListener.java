package com.service.processor.facade.listener;



import com.service.processor.logic.service.RequestService;
import com.service.processor.logic.service.io.IncomingRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import static com.service.processor.logic.template.RequestTemplate.REQUEST_TYPE.BOOK_STORE;


@Component
public class ServiceListener {

    @Autowired
    private RequestService requestService;

    //@NewSpan
    @JmsListener(destination = "${create.books.service.queue.name}")
    public void booksRequest(String message) {
       requestService.processRequest(new IncomingRequest(message, BOOK_STORE));

    }


}
