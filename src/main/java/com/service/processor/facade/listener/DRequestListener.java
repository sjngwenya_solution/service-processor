package com.service.processor.facade.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.service.RequestService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingRequest;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.CORRELATION_ID;

@Component
public class DisputesRequestListener {

    @Autowired
    private RequestService requestService;

    @NewSpan
    @JmsListener(destination = "${plexus.after.debit.order.file.queue.name}",
            containerFactory = "lowConcurrencyContainerFactory")
    public void receiveAdoRequest(String request) {
        requestService.processServiceRequest(new IncomingRequest(request, RequestType.ADO));
    }

    @NewSpan
    @JmsListener(destination = "${plexus.debit.order.dispute.request.queue.name}")
    public void receivePost40Request(String message, @Header(JmsHeaders.CORRELATION_ID) final String correlationID) {
        IncomingRequest<String> incomingRequest = new IncomingRequest<>(message, RequestType.DOD);
        incomingRequest.addParam(CORRELATION_ID, correlationID);
        requestService.processServiceRequest(incomingRequest);
    }

}
