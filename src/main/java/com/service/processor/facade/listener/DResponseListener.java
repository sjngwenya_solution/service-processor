package com.service.processor.facade.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.service.RequestService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;

import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.*;
import static za.co.fnb.plexus.services.request.dto.domain.request.RequestType.ADO;
import static za.co.fnb.plexus.services.request.dto.domain.request.RequestType.DOD;

@Component
public class DisputesResponseListener {

    @Autowired
    private RequestService requestService;

    @NewSpan
    @JmsListener(destination = "${plexus.ado.payment.reversal.response.queue.name}",
            containerFactory = "lowConcurrencyContainerFactory")
    public void receivePaymentReversalResponse(String message){
        IncomingResponse incomingResponse = new IncomingResponse(message, ADO);
        incomingResponse.addParam(ADO_RESPONSE_TYPE, PAYMENT_REVERSAL_RESPONSE);

        requestService.processServiceResponse(incomingResponse);
    }

    @NewSpan
    @JmsListener(destination = "${plexus.ado.stop.payment.response.queue.name}",
            containerFactory = "lowConcurrencyContainerFactory")
    public void receiveStopPaymentResponse(String message){
        IncomingResponse incomingResponse = new IncomingResponse(message, ADO);
        incomingResponse.addParam(ADO_RESPONSE_TYPE, STOP_PAYMENT_RESPONSE);

        requestService.processServiceResponse(incomingResponse);
    }

    @NewSpan
    @JmsListener(destination = "${plexus.debit.order.dispute.status.update.reply.queue.name}")
    public void receivePost40StatusUpdateReply(String message, @Header(JmsHeaders.CORRELATION_ID) final String correlationID) {
        IncomingResponse incomingResponse = new IncomingResponse(message, DOD);
        incomingResponse.addParam(CORRELATION_ID, correlationID);

        requestService.processServiceResponse(incomingResponse);
    }
}
