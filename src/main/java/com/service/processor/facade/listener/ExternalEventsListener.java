package za.co.fnb.plexus.services.request.facade.listener;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.event.externalEventProcessor.EventProcessor;
import za.co.fnb.plexus.services.request.events.ExternalEvent;
import za.co.fnb.plexus.services.request.events.StatusChangedEventDTO;
import za.co.fnb.plexus.services.request.events.WorkCreatedEventDTO;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshallJsonFile;

@Component
@RequiredArgsConstructor
public class ExternalEventsListener {

    @NonNull private EventProcessor eventProcessor;

    @NewSpan
    @JmsListener(destination = "${plexus.work_item.status.changed.queue.name}")
    public void workItemStatusChanged(String message){
        StatusChangedEventDTO changedEventDTO = unmarshallJsonFile(message, StatusChangedEventDTO.class);
        eventProcessor.processByWI(new ExternalEvent<>(changedEventDTO.getWorkItemId(), changedEventDTO));
    }

    @NewSpan
    @JmsListener(destination = "${plexus.work_item.created.queue.name}")
    public void workItemCreated(String message){
        WorkCreatedEventDTO workCreatedEvent = unmarshallJsonFile(message, WorkCreatedEventDTO.class);
        eventProcessor.processByID(new ExternalEvent<>(workCreatedEvent.getRequestId(), workCreatedEvent));
    }
}
