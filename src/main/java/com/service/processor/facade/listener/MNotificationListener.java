package com.service.processor.facade.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.service.RequestService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingRequest;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import static za.co.fnb.plexus.services.request.businesslogic.service.io.AuditLogType.MandateServiceResponse;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.MANDATE_RESPONSE;

@Component
public class MandateNotificationListener {

    @Autowired
    private RequestService requestService;

    @NewSpan // Request coming from IDS via EMS
    @JmsListener(destination = "${plexus.debiCheck.mandate.request.incoming.queue.name}")
    public void receiveMandateRequest(String request) {
        requestService.processServiceRequest(new IncomingRequest(request, RequestType.DCM));
    }

    @NewSpan //Response coming from Mandate service <Topic for now>
    @JmsListener(destination = "${plexus.debiCheck.mandate.response.incoming.queue.name}")
    public void receiveMandateResponse(String response) {
        IncomingResponse incomingResponse = new IncomingResponse(response, RequestType.DCM);
        incomingResponse.addParam(MANDATE_RESPONSE, MandateServiceResponse.getAuditLogType());
        requestService.processServiceResponse(incomingResponse);
    }

}
