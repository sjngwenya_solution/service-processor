package com.service.processor.facade.listener;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.service.RequestService;

import java.nio.charset.Charset;

@Component
public class DocumentNotificationListener {

    @Autowired
    RequestService requestService;
    
    @NewSpan
    @JmsListener(destination = "${plexus.document.notification.queue.name}")

    public void receiveMessage(byte [] message) {
        requestService.documentNotificationReceived(StringUtils.toEncodedString(message, Charset.defaultCharset()));
    }
}
