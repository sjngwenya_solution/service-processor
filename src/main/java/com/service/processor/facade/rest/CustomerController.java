package za.co.fnb.plexus.services.request.facade.rest;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.fnb.plexus.services.facade.controller.Controller;
import za.co.fnb.plexus.services.facade.factory.ResponseFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.SupportingService;
import za.co.fnb.plexus.services.request.dto.customer.CustomerProfile;

import java.util.List;

import static java.util.Objects.nonNull;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/customer")
public class CustomerController extends Controller {

    @Autowired
    private SupportingService supportingService;

    @ApiOperation(value = "Search customer details by ID number or ucn ")
    @GetMapping
    public ResponseEntity customerSearchByIdNumber(@ApiParam("Id search returns a list of customer profiles")
                                                   @RequestParam(value = "idNumber", required = false) String idNumber,
                                                   @ApiParam("UCN search returns a unique of customer profile")
                                                   @RequestParam(value = "ucn", required = false) Long ucn) {

        if(StringUtils.isNotEmpty(idNumber)) {
            List<CustomerProfile> responseDTO = supportingService.customerSearchByIdNumber(idNumber);
            return ResponseFactory.createSuccessResponse(responseDTO);
        }

        if(nonNull(ucn)) {
            CustomerProfile responseDTO = supportingService.customerSearchByUcn(ucn);
            return ResponseFactory.createSuccessResponse(responseDTO);
        }
        return ResponseFactory.createExceptionResponse("Search parameter required", NOT_FOUND);
    }
}
