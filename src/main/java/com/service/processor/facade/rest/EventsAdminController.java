package za.co.fnb.plexus.services.request.facade.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.fnb.plexus.services.facade.controller.Controller;
import za.co.fnb.plexus.services.facade.factory.ResponseFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;

import java.util.List;

@RestController
@RequestMapping("/admin/event")
public class EventsAdminController extends Controller {

    @Autowired
    private EventService eventService;

    @ApiOperation(value = "Executes the specified domain event")
    @PutMapping(value = "{eventId}/cancel")
    public ResponseEntity cancelEvent(@PathVariable("eventId") Long eventId) {
        eventService.updateEventStatus(eventId, "CANCELED");
        return ResponseFactory.createSuccessResponse();
    }

    @ApiOperation(value = "Executes the specified domain event. This methods skips the event status validation.")
    @PutMapping(value = "{eventId}/replay")
    public ResponseEntity replayEvent(@PathVariable("eventId") Long eventId) {
        eventService.replayEvent(eventId, true);
        return ResponseFactory.createSuccessResponse();
    }

    @ApiOperation(value = "Executes the specified domain events")
    @PutMapping(value = "replay")
    public ResponseEntity replayEvents(@RequestBody List<Long> eventIds) {
        eventService.replayEvents(eventIds);
        return ResponseFactory.createSuccessResponse();
    }

    @ApiOperation(value = "Returns all the events for the specified work It")
    @GetMapping(value = "/history")
    public ResponseEntity getEventTrail(@RequestParam("workItemId") Long workItemId) {
        return ResponseFactory.createSuccessResponse(eventService.getEventTrail(workItemId));
    }
}
