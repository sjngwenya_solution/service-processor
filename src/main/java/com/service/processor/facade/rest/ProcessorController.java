package com.service.processor.facade.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//@RequestMapping("/service/processor")
public class ProcessorController {

    public void save(){

    }

    @ApiOperation(value = "Receive a service for a service")
    @PostMapping(value = "/dispute/{disputeType}")
    public ResponseEntity receiveServiceRequest(@RequestBody String servicePayload,
                                                @PathVariable("serviceType") String serviceType) {

        return null;
    }
}
