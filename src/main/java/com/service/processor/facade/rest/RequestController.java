package za.co.fnb.plexus.services.request.facade.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.facade.controller.Controller;
import za.co.fnb.plexus.services.facade.factory.ResponseFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.RequestService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingRequest;
import za.co.fnb.plexus.services.request.dto.mandate.UpdateMetadataDTO;

import static java.util.Objects.nonNull;
import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;
import static za.co.fnb.plexus.services.request.dto.domain.request.RequestType.valueOf;

@RestController
@RequestMapping("/service/request")
public class RequestController extends Controller {

    @Autowired
    private RequestService requestService;

    @ApiOperation(value="Returns a request as per specified parameter")
    @GetMapping
    public ResponseEntity getRequestByReference(@RequestParam(name = "workItemId", required = false) Long workItemId,
                                                @RequestParam(name = "requestReference", required = false) String requestReference,
                                                @RequestParam(name = "externalReferenceNumber", required = false) String externalReferenceNumber,
                                                @ApiParam("This option can only be used with the externalReferenceNumber specified.")
                                                @RequestParam(name = "statuses", required = false) String [] statuses ) {
        Object requestDTO;
        if(nonNull(workItemId))
            requestDTO = requestService.findByWorkItemId(workItemId);
        else if(nonNull(externalReferenceNumber) && nonNull(statuses)) {
            requestDTO = requestService.findByExternalReferenceNumberAndStatus(externalReferenceNumber, statuses);
        } else if(nonNull(externalReferenceNumber))
            requestDTO = requestService.findByExternalReferenceNumber(externalReferenceNumber);
        else if(nonNull(requestReference))
            requestDTO = requestService.getRequestByReference(requestReference);
        else {
            throw new ServiceException(INVALID_REQUEST, "At least one search param expected");
        }
        return ResponseFactory.createSuccessResponse(requestDTO);
    }

    @ApiOperation(value = "Create a service request of specified type")
    @PostMapping
    public ResponseEntity createServiceRequest(@RequestBody String disputePayload, @RequestParam("requestType") String requestType) {
        requestService.processServiceRequest(new IncomingRequest(disputePayload, valueOf(requestType)));
        return ResponseFactory.createSuccessResponse();
    }

    @ApiOperation(value = "Update dispute metadata value")
    @PutMapping(value = "/metadata")
    public ResponseEntity updateMetadata(@RequestParam("workItemId") long workItemId,
                                         @RequestBody UpdateMetadataDTO metadataDTO) {
        requestService.updateMetadata(workItemId, metadataDTO);
        return ResponseFactory.createSuccessResponse();
    }
}

