package com.service.processor.facade.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.fnb.plexus.services.facade.controller.Controller;
import za.co.fnb.plexus.services.facade.factory.ResponseFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.BranchLookUpService;
import za.co.fnb.plexus.services.request.businesslogic.service.SchedulingService;
import za.co.fnb.plexus.services.request.businesslogic.service.UtilityService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.Slot;
import za.co.fnb.plexus.services.request.businesslogic.util.common.CacheBasedLock;

import java.util.Date;
import java.util.Set;

@RestController
@RequestMapping("/admin/")
public class AdministrationController extends Controller {

    @Autowired
    private SchedulingService schedulingService;

    @Autowired
    private BranchLookUpService lookUpService;

    @Autowired
    private UtilityService utilityService;

    @ApiOperation(value = "Returns all the number of locks currently in the cache")
    @GetMapping(value = "/cacheLocks/count")
    public ResponseEntity getLocksCounter() {
        return ResponseFactory.createSuccessResponse(CacheBasedLock.countLocks());
    }

    @ApiOperation(value = "Clears all the lock still in cache")
    @GetMapping(value = "/cacheLocks/invalidate")
    public ResponseEntity invalidateAll() {
        CacheBasedLock.invalidateAll();
        return ResponseFactory.createSuccessResponse();
    }

    @ApiOperation(value = "Schedules a one time event execution job of status and event type")
    @GetMapping(value = "/job/schedule/")
    public ResponseEntity createSingleExecutionJob(@RequestParam("eventStatus") String eventStatus,
                                                   @RequestParam("eventType") String eventType) {
        schedulingService.scheduleJob(new Slot(new Date()), eventType, eventStatus);
        return ResponseFactory.createSuccessResponse();
    }

    @ApiOperation(value = "Executes the branch synchronization.")
    @PostMapping(value = "/branch/sync")
    public ResponseEntity runBranchSync() {
        lookUpService.syncBranchData();
        return ResponseFactory.createSuccessResponse();
    }

    @ApiOperation(value = "Registers easter holiday dates into the database.")
    @PostMapping(value = "/holiday/{year}/{month}")
    public ResponseEntity saveEasterCalender(@PathVariable("year") int year, @PathVariable("month") Integer month,
                                             @RequestBody Set<Integer> days) {
        utilityService.saveAdHocHolidayDates(year, month, days);
        return ResponseFactory.createSuccessResponse();
    }
}
