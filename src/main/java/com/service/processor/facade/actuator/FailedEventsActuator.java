package za.co.fnb.plexus.services.request.facade.actuator;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.dto.domainEvent.DomainEventDto;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;
import static za.co.fnb.plexus.services.request.facade.actuator.FailedEventsActuator.Report.fails;
import static za.co.fnb.plexus.services.request.facade.actuator.FailedEventsActuator.Report.ok;
import static za.co.fnb.plexus.services.request.facade.actuator.FailedEventsActuator.STATUS.*;

@Component
@RequiredArgsConstructor
@Endpoint(id = "failedEvents")
public class FailedEventsActuator {

    @NonNull private EventService eventService;

    @ReadOperation
    public Report checkFailedEvents() {
        List<DomainEventDto> failedEvents = eventService.getFailedEvents();
        if(nonNull(failedEvents) && !failedEvents.isEmpty()) {
            return fails(failedEvents);
        }else {
            return ok();
        }
    }

    enum STATUS {OK, FAILED}

    @Data
    @JsonInclude(NON_NULL)
    @NoArgsConstructor(access = PRIVATE)
    static class Report {
        private STATUS status = OK;
        private List<DomainEventDto> details;

        private Report(@NonNull STATUS status, List<DomainEventDto> details) {
            this.status = status;
            this.details = details;
        }

        public static Report ok() {
            return new Report();
        }

        public static Report fails(List<DomainEventDto> failedEvents) {
            return new Report(FAILED, failedEvents);
        }
    }
}
