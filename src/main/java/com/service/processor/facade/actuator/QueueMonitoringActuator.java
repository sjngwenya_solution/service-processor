package za.co.fnb.plexus.services.request.facade.actuator;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import za.co.fnb.plexus.services.request.businesslogic.service.io.MessagingMonitorService;
import za.co.fnb.plexus.services.request.dto.QueueDetails;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static lombok.AccessLevel.PRIVATE;
import static za.co.fnb.plexus.services.request.facade.actuator.QueueMonitoringActuator.Report.createWith;
import static za.co.fnb.plexus.services.request.facade.actuator.QueueMonitoringActuator.STATUS.FAILED;
import static za.co.fnb.plexus.services.request.facade.actuator.QueueMonitoringActuator.STATUS.OK;

@RequiredArgsConstructor
@Endpoint(id = "queueMonitor")
public class QueueMonitoringActuator {

    @NonNull MessagingMonitorService service;

    @ReadOperation
    public Report checkQueues() {
        return createWith(service.checkQueues());
    }

    enum STATUS {OK, FAILED}

    @Data
    @JsonInclude(NON_NULL)
    @AllArgsConstructor(access = PRIVATE)
    @NoArgsConstructor(access = PRIVATE)
    static class Report {
        @NonNull private STATUS status = OK;
        private List<QueueDetails> details;

        public static Report ok() {
            return new Report();
        }

        public static Report createWith(@NonNull List<QueueDetails> details) {
            return (details.isEmpty()) ? ok() : new Report(FAILED, details);
        }
    }
}
