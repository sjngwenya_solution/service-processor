package za.co.fnb.plexus.services.request.facade.actuator.config;

import io.vavr.Tuple2;
import lombok.*;
import org.apache.activemq.broker.jmx.DestinationViewMBean;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import za.co.fnb.plexus.services.request.businesslogic.service.io.MessagingMonitorService;
import za.co.fnb.plexus.services.request.dto.QueueDetails;
import za.co.fnb.plexus.services.request.facade.actuator.QueueMonitoringActuator;
import za.co.fnb.plexus.services.request.facade.actuator.config.ActiveMqActuatorConfig.ActiveMqJmxAdapter.OPERATION;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.vavr.control.Try.of;
import static java.lang.Thread.sleep;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.reverseOrder;
import static java.util.Objects.nonNull;
import static java.util.concurrent.CompletableFuture.supplyAsync;
import static org.apache.commons.lang3.StringUtils.contains;
import static org.apache.commons.lang3.StringUtils.stripToEmpty;
import static org.slf4j.LoggerFactory.getLogger;
import static za.co.fnb.plexus.services.request.facade.actuator.config.ActiveMqActuatorConfig.ActiveMqJmxAdapter.OPERATION.CONSUMER_COUNT;
import static za.co.fnb.plexus.services.request.facade.actuator.config.ActiveMqActuatorConfig.ActiveMqJmxAdapter.OPERATION.QUEUE_SIZE;

@Configuration
@ConditionalOnProperty(name = {"spring.activemq.brokerHost"})
public class ActiveMqActuatorConfig {

    @Bean
    public ActiveMqJmxAdapter jmxConnection(@Value("${spring.activemq.brokerHost}") String host,
                                            @Value("${spring.activemq.brokerName}") String brokerName) {

        return new ActiveMqJmxAdapter(new ConnectionProxy(host), brokerName);
    }

    @Bean
    public QueueMonitorConfigs queueManager() {
        return new QueueMonitorConfigs();
    }

    @Bean
    public MessagingMonitorService monitor(ActiveMqJmxAdapter jmxAdapter, QueueMonitorConfigs manager) {
        return () -> {
            CompletableFuture<List<QueueDetails>> queueThresholdResults = supplyAsync(manager.checksMessages() ? new Worker(jmxAdapter, manager.getSize().getThreshold(),
                            new Rule(QUEUE_SIZE, "threshold", "size", naturalOrder())) : () -> Collections.EMPTY_LIST);

            CompletableFuture<List<QueueDetails>> minimumConsumerResults = supplyAsync(manager.checksConsumers() ? new Worker(jmxAdapter, manager.getConsumer().getMinimum(),
                    new Rule(CONSUMER_COUNT, "minimum", "consumerCount", reverseOrder())) : () -> Collections.EMPTY_LIST);

            return Stream.of( of(() -> queueThresholdResults.get()).get(), of(() -> minimumConsumerResults.get()).get() )
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
        };
    }

    @Bean
    public QueueMonitoringActuator actuator(MessagingMonitorService monitorService) {
        return new QueueMonitoringActuator(monitorService);
    }

    @ConfigurationProperties(prefix = "spring.activemq.monitoring")
    static class QueueMonitorConfigs {

        @Setter @Getter private MonitorConsumers consumer;
        @Getter @Setter private MonitorMessages size;

        @Data static class MonitorConsumers {
            private Map<Integer, List<String>> minimum;
        }

        @Data static class MonitorMessages {
            private Map<Integer, List<String>> threshold;
        }

        boolean checksConsumers() { return nonNull(consumer); }
        boolean checksMessages() { return nonNull(size); }
    }

    static class ConnectionProxy {
        private MBeanServerConnection jmxConnection;

        private String host;
        public ConnectionProxy(@Value("${spring.activemq.brokerHost}") String host) {
            this.host = host;
            newConn();
        }

        Set<ObjectName> query(ObjectName nameConsumers) {
            try {
                return jmxConnection.queryNames(nameConsumers, null);
            } catch (Exception e) {
                processException(e);
            }
            return Collections.EMPTY_SET;
        }

        Optional<DestinationViewMBean> instance(ObjectName objectName) {
            try{
                return Optional.ofNullable(MBeanServerInvocationHandler.newProxyInstance(jmxConnection,
                        objectName, DestinationViewMBean.class, true));
            }catch (Exception e) {
                processException(e);
            }
            return null;
        }

        private void processException(Throwable throwable) {
            if("java.rmi.ConnectException".equals(throwable.getClass().getName()) ||
                    contains(throwable.getMessage(), "Connection refused to host:")) {
                newConn();
            }else {
                getLogger(ActiveMqActuatorConfig.class).error(throwable.getMessage());
            }
        }

        @SneakyThrows
        private void newConn() {
            try {
                String connectionString = String.format("service:jmx:rmi:///jndi/rmi://%s/jmxrmi", host);
                jmxConnection = JMXConnectorFactory.connect(new JMXServiceURL(connectionString))
                        .getMBeanServerConnection();
            }catch (Exception e) {
                getLogger(ActiveMqActuatorConfig.class).error(e.getMessage());
            }
        }
    }

    @RequiredArgsConstructor
    static class ActiveMqJmxAdapter {

        @NonNull private ActiveMqActuatorConfig.ConnectionProxy proxiedConnection;
        @NonNull private String brokerName;

        public enum OPERATION {QUEUE_SIZE, CONSUMER_COUNT}

        @SneakyThrows
        private long check(OPERATION operation, String destinationName) {
            ObjectName nameConsumers = new ObjectName("org.apache.activemq:type=Broker,brokerName=" + brokerName +
                    ",destinationType=Queue,destinationName=" + stripToEmpty(destinationName));

            Set<ObjectName> objectNames = proxiedConnection.query(nameConsumers);
            if(objectNames.isEmpty()) { return 0; }

            Optional<DestinationViewMBean> optionalDestinationView = proxiedConnection
                    .instance(objectNames.iterator().next());

            if(optionalDestinationView.isPresent()) {
                switch (operation) {
                    case QUEUE_SIZE: return optionalDestinationView.get().getQueueSize();
                    case CONSUMER_COUNT: return optionalDestinationView.get().getConsumerCount();
                    default: return 0;
                }
            } return -1;
        }
     }

    static class Worker implements Supplier<List<QueueDetails>> {

        private Rule rule;
        private ActiveMqJmxAdapter jmxAdapter;
        private Map<Integer, List<String>> configs;

        public Worker(ActiveMqJmxAdapter jmxAdapter, Map<Integer, List<String>> configs,
                      Rule rule) {
            this.rule = rule;
            this.jmxAdapter = jmxAdapter;
            this.configs = configs;
        }

        @Override
        public List<QueueDetails> get() {
            return configs.keySet().stream()
                    .map(required -> configs.get(required).stream()
                            .map(q -> new Tuple2<>(q, jmxAdapter.check(rule.getOperation(), q)))
                            .filter(detail -> rule.check(required, detail._2().intValue()))
                            .map(data -> new QueueDetails(data._1(), rule.getRequiredKey(), required, rule.getCurrentKey(), data._2()))
                            .collect(Collectors.toList()))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
        }
    }

    static class Rule {

        @Getter private OPERATION operation;
        @Getter private String requiredKey, currentKey;
        private Comparator<Integer> comparator;

        public Rule(OPERATION operation, String requiredKey, String currentKey, Comparator<Integer> comparator) {
            this.operation = operation;
            this.requiredKey = requiredKey;
            this.currentKey = currentKey;
            this.comparator = comparator;
        }

        public boolean check(Integer required, Integer current) {
            return (comparator.compare(required, current) < 0);
        }
    }
}
