package za.co.fnb.plexus.services.request.facade.actuator;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static java.time.Instant.ofEpochMilli;
import static lombok.AccessLevel.PRIVATE;
import static za.co.fnb.plexus.services.request.facade.actuator.QuartzTriggersActuator.Report.blocked;
import static za.co.fnb.plexus.services.request.facade.actuator.QuartzTriggersActuator.Report.ok;
import static za.co.fnb.plexus.services.request.facade.actuator.QuartzTriggersActuator.Status.BLOCKED;

@Component
@Endpoint(id = "triggers")
public class QuartzTriggersActuator {

    private static final SimpleDateFormat simpleDateFormat =new SimpleDateFormat("MM-dd-yyyy HH:mm:ss",
            Locale.getDefault());

    private static final String QUERY = "SELECT trigger_name, next_fire_time, prev_fire_time, trigger_state FROM qrtz_triggers";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @ReadOperation
    public Report triggers() {
        Stream<TriggerInfo> triggersStream = jdbcTemplate.query(QUERY, (rs, i) -> new TriggerInfo(
                rs.getString("trigger_name"),
                rs.getString("trigger_state"),
                rs.getLong("next_fire_time"),
                rs.getLong("prev_fire_time")
        )).stream();

        LocalDateTime now = LocalDateTime.now();

        List<TriggerInfo> blockedTriggers = triggersStream.filter(info -> ("BLOCKED".equals(info.getTriggerState()) && now.isAfter(ofEpochMilli(info.nextFireTime)
                .atZone(ZoneId.systemDefault()).toLocalDateTime().plusMinutes(5))))
                .collect(Collectors.toList());

        if(!blockedTriggers.isEmpty()) {
            return blocked(blockedTriggers);
        }
        return ok();
    }

    @Data
    @AllArgsConstructor
    static class TriggerInfo {
        private String triggerName, triggerState; private Long nextFireTime, prevFireTime;

        public String getNextFireTime() { return asString(nextFireTime); }
        public String getPrevFireTime() { return asString(prevFireTime); }

        private String asString(Long epochTime) {
            return (epochTime == -1) ? "-1" : simpleDateFormat.format(new Date(epochTime));
        }
    }

    enum Status { OK, BLOCKED }

    @JsonInclude(NON_NULL)
    @AllArgsConstructor(access = PRIVATE)
    @NoArgsConstructor(access = PRIVATE)
    static class Report {
        @Getter private Status status = Status.OK;
        @Getter private List<TriggerInfo> details;

        public static Report ok() { return new Report(); }

        public static Report blocked(List<TriggerInfo> details) {
            return new Report(BLOCKED, details);
        }
    }
}
