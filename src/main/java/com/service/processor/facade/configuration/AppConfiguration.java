package za.co.fnb.plexus.services.request.facade.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;

import static javax.jms.Session.SESSION_TRANSACTED;

@Configuration
public class AppConfiguration {

    @Value("${spring.jms.config.concurrency:2-10}")
    private String concurrencyLevel;

    @Value("${spring.jms.config.concurrency.low:8}")
    private String lowConcurrencyLevel;

    @Bean
    public JmsListenerContainerFactory<?> jmsListenerContainerFactory(ConnectionFactory connectionFactory,
                                                                      DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConcurrency(concurrencyLevel);
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    @Bean(name = "lowConcurrencyContainerFactory")
    public JmsListenerContainerFactory<?> lowConcurrencyContainerFactory(ConnectionFactory connectionFactory,
                                                                         DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConcurrency(lowConcurrencyLevel);
        configurer.configure(factory, connectionFactory);
        return factory;
    }
}
