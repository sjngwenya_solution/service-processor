package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction;
import za.co.fnb.plexus.services.request.businesslogic.service.io.Slot;

public interface MainframeAvailabilityService {

    boolean isFunctionAvailable(MainframeFunction mainframeFunction);

    Slot getNextAvailableSlot(MainframeFunction mainframeFunction);
}
