package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.EventStatus;
import za.co.fnb.plexus.services.request.dto.domainEvent.DomainEventDto;

import java.sql.Timestamp;
import java.util.List;

public interface EventService {

    void send(DomainEvent event);

    void replayEvents(List<Long> eventIds);

    void updateEventStatus(Long eventId, String eventStatus);

    void replayEvent(Long eventId, boolean ignoreEventStatus);

    void fireEventsOfTypeAndStatus(String className, EventStatus eventStatus);

    void fireEventsOfStatusCreatedBefore(EventStatus eventStatus, Timestamp createBefore);

    List<DomainEventDto> getFailedEvents();

    List<DomainEventDto> getEventTrail(Long workItemId);
}
