package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingRequest;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;
import za.co.fnb.plexus.services.request.businesslogic.service.io.SLAViolationWrapper;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestDTO;
import za.co.fnb.plexus.services.request.dto.mandate.UpdateMetadataDTO;

import java.util.List;
import java.util.Set;

public interface RequestService {

    Set<RequestDTO> findByExternalReferenceNumberAndStatus(String externalReferenceNumber, String [] statuses);

    RequestDTO findByExternalReferenceNumber(String externalReferenceNumber);

    RequestDTO getRequestByReference(String requestReference);

    RequestDTO findByWorkItemId(Long workItemId);

    String findMetadataValue(Long workItemId, String metadataKey);

    void updateMetadata(Long workItemId, UpdateMetadataDTO UpdateMetadataEvent);

    void slaViolationReceived(SLAViolationWrapper slaViolationMessage);

    void processServiceResponse(IncomingResponse response);

    void processServiceRequest(IncomingRequest request);

    void documentNotificationReceived(String message);
}