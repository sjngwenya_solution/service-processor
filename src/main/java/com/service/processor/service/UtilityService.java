package za.co.fnb.plexus.services.request.businesslogic.service;

import java.util.Set;

/**
 * @author Tumelo Labase on 2020/10/21.
 */
public interface UtilityService {

    void saveAdHocHolidayDates(Integer year, Integer month, Set<Integer> days);
    boolean isEasterDatesSet();
}
