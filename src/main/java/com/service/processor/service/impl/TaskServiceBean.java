package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.businesslogic.service.TaskService;
import za.co.fnb.plexus.services.request.domain.task.Action;
import za.co.fnb.plexus.services.request.domain.task.Task;
import za.co.fnb.plexus.services.request.domain.task.TaskStatus;
import za.co.fnb.plexus.services.request.domain.task.Tasklist;
import za.co.fnb.plexus.services.request.infrastructure.repository.TaskListRepository;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;
import static za.co.fnb.plexus.services.request.domain.task.TaskStatus.*;

@Service
@Transactional
public class TaskServiceBean implements TaskService {

    @Autowired
    TaskListRepository taskListRepository;

    @Override
    public void createList(Long referenceNumber) {
        createList(referenceNumber, null);
    }

    @Override
    public void createList(Long referenceNumber, Long requiredTaskCount) {
        Tasklist checklist = new Tasklist();
        checklist.setWorkItemId(referenceNumber);
        checklist.setRequiredTasksCount(requiredTaskCount);
        taskListRepository.save(checklist);
    }

    @Override
    public void createNewListWithTask(Long referenceNumber, String actionReference, Action action) {
        Tasklist checklist = new Tasklist();
        checklist.setWorkItemId(referenceNumber);

        Task task = new Task(action);
        task.setTaskReference(actionReference);
        checklist.getListTasks().add(task);

        taskListRepository.save(checklist);
    }

    @Override
    public void createListIfNotExistWithTask(Long referenceNumber, Action action, String actionReference) {

        Tasklist checklist = taskListRepository.findByWorkItemId(referenceNumber);

        if(checklist == null) {
            createNewListWithTask(referenceNumber, actionReference, action);
            return;
        }

        Task task = new Task(action);;
        task.setTaskReference(actionReference);
        checklist.getListTasks().add(task);

        taskListRepository.saveAndFlush(checklist);
    }

    @Override
    public Long retrieveWorkItemID(String actionCorrelationId) throws ServiceException {
        return loadCheckList(actionCorrelationId).getWorkItemId();
    }

    @Override
    public Long retryable(String taskCorrelationId) throws ServiceException {
        return updateItem(taskCorrelationId, TEMP_FAILURE);
    }

    @Override
    public Long completeTask(String taskCorrelationId) {
        return updateItem(taskCorrelationId, COMPLETED);
    }

    @Override
    public Long failTask(String taskCorrelationId) {
        return updateItem(taskCorrelationId, FAILED);
    }

    @Override
    public <T> T executeFunction(Long referenceNumber, Function<Tasklist, T> check) {
        Tasklist checklist = taskListRepository.findByWorkItemId(referenceNumber);
        if(Objects.isNull(checklist))
            return null;

        return check.apply(checklist);
    }

    @Override
    public <T> T executeFunction(Long referenceNumber, String filterParameter, BiFunction<Tasklist, String, T> check) {
        Tasklist checklist = taskListRepository.findByWorkItemId(referenceNumber);
        if (Objects.isNull(checklist)) return null;
        return check.apply(checklist, filterParameter);
    }

    private Long updateItem(String itemCorrelationId, TaskStatus status) {

        Tasklist checklist = loadCheckList(itemCorrelationId);
        //Should never be absent
        Task task = checklist.getTaskWithCorrelationId(itemCorrelationId).get();

        switch (status) {
            case TEMP_FAILURE:
                task.retry();
                break;
            case COMPLETED:
                task.compete();
                break;
            case FAILED:
                task.failed();
                break;
            default:
                task.setStatus(status);
        }

        Tasklist save = taskListRepository.save(checklist);
        return save.getWorkItemId();
    }

    private Tasklist loadCheckList(String itemCorrelationId) throws ServiceException {
        //Load checklist request
        Tasklist checklist = taskListRepository.findByListTasksTaskReference(itemCorrelationId);

        if(checklist == null)
            throw new ServiceException(INVALID_REQUEST,
                    String.format("Correlation Id %s does not have a mapped checklist.", itemCorrelationId));

        return checklist;
    }
}
