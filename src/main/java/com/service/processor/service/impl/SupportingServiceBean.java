package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.fnb.plexus.services.request.businesslogic.service.SupportingService;
import za.co.fnb.plexus.services.request.dto.DebitOrderSearchCriteria;
import za.co.fnb.plexus.services.request.dto.DebitOrderDetailsDTO;
import za.co.fnb.plexus.services.request.dto.common.Product;
import za.co.fnb.plexus.services.request.dto.customer.CustomerAndAccountSearchRequest;
import za.co.fnb.plexus.services.request.dto.customer.CustomerProfile;
import za.co.fnb.plexus.services.request.dto.customer.account.AccountDTO;

import za.co.fnb.plexus.services.request.infrastructure.adapter.AccountServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.adapter.CustomerServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.adapter.MandateServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.adapter.impl.DebitOrderDetailsAggregator;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class SupportingServiceBean implements SupportingService {

    @NonNull private List<AccountServiceAdapter> accountServiceAdapters;
    @NonNull private CustomerServiceAdapter customerServiceAdapter;
    @NonNull private DebitOrderDetailsAggregator detailsAggregator;
    @Autowired
    MandateServiceAdapter mandateServiceAdapter;

    @Override
    public AccountDTO searchForAccount(String accountNumber, Product product) {
        return accountServiceAdapters.stream()
                .filter(accountServiceAdapter -> Objects.equals(product, accountServiceAdapter.getProduct()))
                .findFirst()
                .get().accountSearch(accountNumber);
    }

    @Override
    public List<CustomerProfile> customerSearchByIdNumber(String identityNumber) {
        return customerServiceAdapter.customerSearchByIdNumber(identityNumber);
    }

    @Override
    public CustomerProfile customerSearchByUcn(Long ucn) {
        return customerServiceAdapter.customerSearchByUcn(ucn);
    }

    @Override
    public DebitOrderDetailsDTO searchProfileAndAccount(CustomerAndAccountSearchRequest searchRequest) {

        DebitOrderSearchCriteria searchCriteria = new DebitOrderSearchCriteria();
        searchCriteria.setUcn(searchRequest.getUcn());
        searchCriteria.setAccountNumber(searchRequest.getAccountNumber());
        searchCriteria.setIdentityNumber(searchRequest.getIdentityNumber());
        searchCriteria.setProduct(searchRequest.getProduct());

        return detailsAggregator.searchForDetails(searchCriteria);
    }

    @Override
    public boolean isExistingMandateInFlight(String applicationNumber) {
        return mandateServiceAdapter.isExistingMandateInFlight(applicationNumber);
    }
}
