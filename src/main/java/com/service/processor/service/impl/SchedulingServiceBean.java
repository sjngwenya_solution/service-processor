package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import za.co.fnb.plexus.services.request.businesslogic.jobs.FireDomainEventsJob;
import za.co.fnb.plexus.services.request.businesslogic.service.SchedulingService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.Slot;
import za.co.fnb.plexus.services.request.domain.events.EventStatus;
import za.co.fnb.plexus.services.schedule.dto.job.JobDataEntry;
import za.co.fnb.plexus.services.schedule.dto.job.JobId;
import za.co.fnb.plexus.services.schedule.dto.job.JobInfo;
import za.co.fnb.plexus.services.schedule.dto.request.AddSingleExecutionJobRequest;
import za.co.fnb.plexus.services.schedule.dto.schedule.*;
import za.co.fnb.plexus.services.schedule.service.SchedulerManager;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.Date.from;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static za.co.fnb.plexus.services.request.businesslogic.config.JobsConfig.APPLICATION_NAME;
import static za.co.fnb.plexus.services.request.domain.events.EventStatus.SCHEDULED;

@Service
@RequiredArgsConstructor
public class SchedulingServiceBean implements SchedulingService {

    private static final Function<JobInfo, Boolean> IS_COMPLETE = jobInfo -> {
        List<ScheduleInfo> scheduleInfos = jobInfo.getJobSchedules();

        if(isNull(scheduleInfos) || scheduleInfos.isEmpty())
            return true;

        return scheduleInfos.stream()
                .anyMatch(scheduleInfo -> ScheduleState.COMPLETE == scheduleInfo.getState());
    };

    @NonNull
    private SchedulerManager schedulerManager;

    @Override
    public void scheduleJobNextHour(String event) {
        scheduleJobNextHour(event, SCHEDULED.name());
    }

    @Override
    public synchronized void scheduleJobNextHour(String eventType, String eventStatus) {
        scheduleJob(new Slot(constructFutureDate(60)), eventType, eventStatus);
    }

    @Override
    public synchronized void scheduleJob(Slot timeSlot, String eventType, String eventStatus) {

        EventStatus.valueOf(eventStatus); //Validation

        JobId jobId = new JobId(APPLICATION_NAME, eventType +"_" + eventStatus);

        if(!schedulerManager.isJobPresent(jobId) || IS_COMPLETE.apply( schedulerManager.getJobInfo(jobId) ) )
        {
            addJob(jobId, eventType, asList(new JobDataEntry("eventStatus", encode(eventStatus))),
                    timeSlot.getAvailableFrom());
        }
    }

    @Override
    public synchronized JobId scheduleJob(Slot timeSlot, String eventType) {

        JobId jobId = new JobId(APPLICATION_NAME, eventType);

        if(!schedulerManager.isJobPresent(jobId))
        {
            addJob(jobId, eventType, Collections.emptyList(), timeSlot.getAvailableFrom());
            return jobId;
        }else
            return null;
    }
    public synchronized void scheduleMandateNextHour(String eventType, EventStatus eventStatus) {

        JobId jobId = new JobId(APPLICATION_NAME, eventType +"_" + eventStatus.name());

        if(!schedulerManager.isJobPresent(jobId) || IS_COMPLETE.apply( schedulerManager.getJobInfo(jobId) ) )
        {
            addJob(jobId, eventType, asList(new JobDataEntry("eventStatus", encode(eventStatus.name()))),
                    constructFutureDate(60));
        }
    }

    private void addJob(JobId jobId, String eventType, List<JobDataEntry> jobData, Date executionDate) {

        //Create schedule
        AddSingleExecutionJobRequest executionJobRequest = new AddSingleExecutionJobRequest();

        executionJobRequest.setJobClassName(FireDomainEventsJob.class.getName());
        executionJobRequest.setJobId(jobId);
        executionJobRequest.setSimpleSchedule( new SimpleSchedule(
                new ScheduleId("ADO", jobId.getJobName() +"_SCHEDULE"),
                executionDate,
                SimpleMisfireInstruction.FIRE_NOW
        ));

        List<JobDataEntry> jobDataEntries = new ArrayList<>();
        jobDataEntries.add(new JobDataEntry("eventType", encode(eventType)));

        if(nonNull(jobData)) jobDataEntries.addAll(jobData);

        executionJobRequest.setJobData(jobDataEntries);

        schedulerManager.addSingleExecutionJob(executionJobRequest);
    }


    private Date constructFutureDate(int minutes) {
        LocalDateTime currentDate = LocalDateTime.now();
        currentDate = currentDate.plusMinutes(minutes);
        return from( currentDate.atZone( ZoneId.systemDefault()).toInstant());
    }

    private String encode(String payload) {
        return Base64Utils.encodeToString(payload.getBytes());
    }
}
