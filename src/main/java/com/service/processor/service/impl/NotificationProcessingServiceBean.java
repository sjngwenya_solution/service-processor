package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import za.co.fnb.plexus.services.request.businesslogic.service.NotificationProcessingService;
import za.co.fnb.plexus.services.request.domain.LookupData;
import za.co.fnb.plexus.services.request.dto.dispute.EmailDetailsDTO;
import za.co.fnb.plexus.services.request.dto.dispute.EmailNotificationDTO;
import za.co.fnb.plexus.services.request.dto.workflow.WorkflowEventDTO;
import za.co.fnb.plexus.services.request.infrastructure.adapter.CommunicationServiceAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.LookupDataRepository;

import java.util.Optional;

import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshallJsonFile;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.ADO_FILE_EMAIL_NOTIFICATION;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.ADO_SLA_EMAIL_NOTIFICATION;

@Service
@RefreshScope
public class NotificationProcessingServiceBean implements NotificationProcessingService {

    @Autowired
    private CommunicationServiceAdapter communicationServiceAdapter;

    @Autowired
    private LookupDataRepository lookupDataRepository;

    @Override
    public void processFileNotification(String message){
        EmailNotificationDTO emailNotificationDTO = unmarshallJsonFile(message, EmailNotificationDTO.class);

        if( emailNotificationDTO.getCode() > 0)
            communicationServiceAdapter.sendEmailNotification(emailNotificationDTO, getEmailSetupDetails(ADO_FILE_EMAIL_NOTIFICATION));
    }

    @Override
    public void processAdoSLANotification(WorkflowEventDTO workflowEventDTO) {
        communicationServiceAdapter.sendAdoSLAEmailNotification(workflowEventDTO, getEmailSetupDetails(ADO_SLA_EMAIL_NOTIFICATION));
    }

    private EmailDetailsDTO getEmailSetupDetails(String lookupKey){
        Optional<LookupData> lookupData = lookupDataRepository.findTopByActiveAndLookupKey(true, lookupKey);
        if(lookupData.isPresent()){
            return unmarshallJsonFile(lookupData.get().getData(), EmailDetailsDTO.class);
        }
        return null;
    }
}
