package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.base.notification.Notification;
import za.co.fnb.plexus.services.request.businesslogic.factory.DocumentFactory;
import za.co.fnb.plexus.services.request.businesslogic.factory.RequestHandlerFactory;
import za.co.fnb.plexus.services.request.businesslogic.factory.SLAViolationTemplateFactory;
import za.co.fnb.plexus.services.request.businesslogic.service.RequestService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingRequest;
import za.co.fnb.plexus.services.request.businesslogic.service.io.IncomingResponse;
import za.co.fnb.plexus.services.request.businesslogic.service.io.SLAViolationWrapper;
import za.co.fnb.plexus.services.request.businesslogic.template.SLAViolationTemplate;
import za.co.fnb.plexus.services.request.businesslogic.transformation.TypeConverter;
import za.co.fnb.plexus.services.request.businesslogic.validation.DocumentNotificationValidator;
import za.co.fnb.plexus.services.request.domain.request.Request;
import za.co.fnb.plexus.services.request.domain.request.RequestStatus;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestDTO;
import za.co.fnb.plexus.services.request.dto.mandate.UpdateMetadataDTO;
import za.co.fnb.plexus.services.request.dto.notification.DocumentNotification;
import za.co.fnb.plexus.services.request.infrastructure.adapter.PlexusWorkflowAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.RequestRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;
import static za.co.fnb.plexus.services.request.businesslogic.util.common.ServiceRequestMarshaller.unmarshallIgnoreUnknownFields;

@Service
@RefreshScope
public class RequestServiceBean implements RequestService {

    @Autowired private SLAViolationTemplateFactory slaTemplateFactory;
    @Autowired private TypeConverter<Request, RequestDTO> requestTypeConverter;
    @Autowired private DocumentNotificationValidator notificationValidator;
    @Autowired private PlexusWorkflowAdapter plexusWorkflowAdapter;
    @Autowired private RequestHandlerFactory handlerFactory;
    @Autowired private RequestRepository requestRepository;
    @Autowired private DocumentFactory documentFactory;

    @Override
    public RequestDTO getRequestByReference(String requestReference) {
        Request request = requestRepository.findByReferenceNumber(requestReference);
        return requestTypeConverter.convert(request);
    }

    @Override
    public RequestDTO findByWorkItemId(Long workItemId) {
        Request request = requestRepository.findByWorkItemId(workItemId);
        return requestTypeConverter.convert(request);
    }

    @Override
    public RequestDTO findByExternalReferenceNumber(String externalReferenceNumber) {
        Request request = requestRepository.findByExternalReferenceNumber(externalReferenceNumber);
        return requestTypeConverter.convert(request);
    }

    @Override
    public Set<RequestDTO> findByExternalReferenceNumberAndStatus(String externalReferenceNumber, String [] statuses) {
        List<RequestStatus> requestStatuses = Arrays.stream(statuses)
                .map(status -> RequestStatus.valueOf(status))
                .collect(Collectors.toList());

        Set<Request> request = requestRepository.findByExternalReferenceNumberAndStatus(externalReferenceNumber, requestStatuses);
        return requestTypeConverter.convert(request);
    }

    @Override
    public String findMetadataValue(Long workItemId, String metadataKey) {
        return requestRepository.findMetadataValue(workItemId, metadataKey);
    }

    @Override
    @Transactional
    public void processServiceRequest(IncomingRequest request) {
        handlerFactory.handler(request.getRequestType())
                .createRequest((String) request.getPayload(), request.getParameters());
    }

    @Override
    @Transactional
    public void processServiceResponse(IncomingResponse response) {
        handlerFactory.handler(response.getRequestType())
                .handleResponse(response);
    }

    @Override
    @Transactional
    public void slaViolationReceived(SLAViolationWrapper wrapper) {
        SLAViolationTemplate slaViolationTemplate = slaTemplateFactory.getTemplate(wrapper.getRequestType());
        slaViolationTemplate.process(wrapper.getViolation());
    }

    @Override
    @Transactional
    public void updateMetadata(Long workItemId, UpdateMetadataDTO updateMetadataDTO) {
        requestRepository.updateRequestMetaData(workItemId, updateMetadataDTO.getKey(),
                updateMetadataDTO.getValue());
    }

    @Override
    @Transactional
    public void documentNotificationReceived(String notificationMessage){

        DocumentNotification documentNotification=  unmarshallIgnoreUnknownFields(notificationMessage, DocumentNotification.class);
        Notification notification = notificationValidator.validateDocumentNotification(documentNotification);

        if (notification.hasErrors())
            throw new ServiceException(INVALID_REQUEST, notification.getFirstErrorMessage());

        Request request = requestRepository.findByExternalReference(documentNotification.getFrDocRef());
        if(Objects.isNull(request))
            return;

        documentFactory.buildSupportingDocument(request, documentNotification.getRObjectId(),null, null);
        requestRepository.save(request);
    }
}