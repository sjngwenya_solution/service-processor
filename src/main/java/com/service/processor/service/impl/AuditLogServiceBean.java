package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.fnb.plexus.services.request.businesslogic.service.AuditLogService;
import za.co.fnb.plexus.services.request.domain.request.ServiceRequestAudit;
import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;
import za.co.fnb.plexus.services.request.infrastructure.repository.ServiceRequestAuditRepository;

@Service
public class AuditLogServiceBean implements AuditLogService {

    @Autowired
    private ServiceRequestAuditRepository serviceRequestAuditRepository;

    @Override
    public void log(ServiceRequestAuditDTO auditDTO) {
        ServiceRequestAudit serviceRequestAudit = new ServiceRequestAudit();
        serviceRequestAudit.setWorkItemId(auditDTO.getWorkItemId());
        serviceRequestAudit.setDebitOrderDisputePayload(auditDTO.getPayload());
        serviceRequestAudit.setPayloadType(auditDTO.getPayloadType());
        serviceRequestAudit.setExternalReferenceNumber(auditDTO.getReference());
        serviceRequestAudit.setCorrelationId(auditDTO.getCorrelationID());
        serviceRequestAudit.setRequestType(auditDTO.getRequestType().name());
        serviceRequestAuditRepository.save(serviceRequestAudit);
    }
}
