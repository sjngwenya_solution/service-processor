package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.base.exception.ServiceExceptionType;
import za.co.fnb.plexus.services.request.businesslogic.service.UtilityService;
import za.co.fnb.plexus.services.request.businesslogic.util.workDay.EasterHolidayMarshaller;
import za.co.fnb.plexus.services.request.domain.LookupData;
import za.co.fnb.plexus.services.request.infrastructure.repository.LookupDataRepository;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;
import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static za.co.fnb.plexus.services.request.dto.ApplicationConstants.EASTER_HOLIDAY_POST_FIX;

/**
 * @author Tumelo Labase on 2020/10/21.
 */
@Service
@AllArgsConstructor
public class UtilityServiceBean implements UtilityService {

    private LookupDataRepository repository;

    @Override
    public void saveAdHocHolidayDates(Integer year, Integer month, Set<Integer> days) {
        if(isNull(days) || days.isEmpty())
            throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, "Easter days may not be null");

        Month easterMonth = Month.of(month);
        days.forEach(day -> {
            if(day > easterMonth.maxLength() || day < 1)
                throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, format("Invalid day (%s) of month (%s)", day, easterMonth.getValue()));
        });

        String key = format("%s%s", year, EASTER_HOLIDAY_POST_FIX);
        LookupData lookupData = repository.findTopByActiveAndLookupKey(true, key)
                .orElse(new LookupData(key));

        try {
            Map<Month, Set<Integer>> dates = new HashMap<>();

            if (!isEmpty(lookupData.getData())) {
                dates = EasterHolidayMarshaller.deserialize(lookupData.getData());
            }
            dates.put(easterMonth, days);

            lookupData.setData(EasterHolidayMarshaller.serialize(dates));
            repository.save(lookupData);
        } catch (IOException e) {
            throw new ServiceException(ServiceExceptionType.INVALID_REQUEST, e.getMessage());
        }
    }

    @Override
    public boolean isEasterDatesSet() {
        return repository.findTopByActiveAndLookupKey(true,
                format("%s%s", LocalDate.now().getYear(), EASTER_HOLIDAY_POST_FIX)).isPresent();
    }
}
