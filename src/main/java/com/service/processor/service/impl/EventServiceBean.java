package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.base.exception.ServiceExceptionType;
import za.co.fnb.plexus.services.request.businesslogic.service.EventService;
import za.co.fnb.plexus.services.request.domain.events.DomainEvent;
import za.co.fnb.plexus.services.request.domain.events.EventStatus;
import za.co.fnb.plexus.services.request.dto.domainEvent.DomainEventDto;
import za.co.fnb.plexus.services.request.infrastructure.mapper.TypeMapper;
import za.co.fnb.plexus.services.request.infrastructure.repository.EventRepository;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;
import static za.co.fnb.plexus.services.request.domain.events.EventStatus.FAILED;
import static za.co.fnb.plexus.services.request.domain.events.EventStatus.replayCandidates;

@Service
@RequiredArgsConstructor
public class EventServiceBean implements EventService {

    @NonNull
    private TypeMapper mapper;

    @NonNull
    private EventRepository eventRepository;

    @NonNull
    private ApplicationEventPublisher publisher;

    @Override
    public void send(DomainEvent domainEvent) {
        publisher.publishEvent(domainEvent);
    }

    @Override
    public void replayEvent(Long eventId, boolean ignoreEventStatus) {
        eventRepository.findById(eventId).ifPresent(domainEvent -> {

            if(ignoreEventStatus || replayCandidates().anyMatch(status -> status == domainEvent.getStatus())) {
                send(domainEvent);
            }else {
                throw new ServiceException(ServiceExceptionType.INVALID_REQUEST,
                        "May not replay event is " + domainEvent.getStatus()+ " state.");
            }
        });
    }

    @Override
    public void replayEvents(List<Long> eventIds) {
        eventIds.stream().forEach(eventId -> this.replayEvent(eventId, false));
    }

    @Override
    public void updateEventStatus(Long eventId, String eventStatus) {
        EventStatus newStatus = EventStatus.valueOf(eventStatus);
        eventRepository.findById(eventId).ifPresent(domainEvent -> {
            domainEvent.setStatus(newStatus);
            eventRepository.save(domainEvent);
        });
    }

    @Override
    @Transactional(propagation = REQUIRES_NEW)
    public List<DomainEventDto> getFailedEvents() {
        return eventRepository.findByStatusAndRetryCountGreaterThan(FAILED, 3)
                .map(domainEvent -> mapper.convert(domainEvent, DomainEventDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(propagation = REQUIRES_NEW)
    public void fireEventsOfTypeAndStatus(String className, EventStatus eventStatus) {
        eventRepository.findByClassNameAndStatus(className, eventStatus)
                .forEach(this::send);
    }

    @Override
    @Transactional(propagation = REQUIRES_NEW)
    public void fireEventsOfStatusCreatedBefore(EventStatus eventStatus, Timestamp createBefore) {
        eventRepository.findByStatusAndCreateDateBefore(eventStatus, createBefore)
                .forEach(this::send);
    }

    @Override
    @Transactional(propagation = REQUIRES_NEW, readOnly = true)
    public List<DomainEventDto> getEventTrail(Long workItemId) {
        return eventRepository.findByWorkItemId(workItemId)
                .map(domainEvent -> mapper.convert(domainEvent, DomainEventDto.class))
                .collect(Collectors.toList());
    }
}
