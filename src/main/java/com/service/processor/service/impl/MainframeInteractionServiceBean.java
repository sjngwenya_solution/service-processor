package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import za.co.fnb.plexus.services.request.businesslogic.command.CommandFactory.CommandToEventMapping;
import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand;
import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction;
import za.co.fnb.plexus.services.request.businesslogic.service.MainframeAvailabilityService;
import za.co.fnb.plexus.services.request.businesslogic.service.MainframeInteractionService;
import za.co.fnb.plexus.services.request.businesslogic.service.SchedulingService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.Slot;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction.PAYMENT_REVERSAL;
import static za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand.MainframeFunction.STOP_PAYMENT;

@Service
@RequiredArgsConstructor
public class MainframeInteractionServiceBean implements MainframeInteractionService {

    private static final CircuitManager CIRCUIT_MANAGER = new CircuitManager();

    @NonNull
    private MainframeAvailabilityService availabilityService;

    @NonNull
    private SchedulingService schedulingService;

    @Override
    public EventExecutionResponse execute(ServiceCommand command) {

        if(isWithinWindow(command)) {
            runCommand(command);
            return EventExecutionResponse.SUCCESSFUL;
        }else {
            schedule(command);
            return EventExecutionResponse.SCHEDULED;
        }
    }

    public void reset() {
        CIRCUIT_MANAGER.resetAll();
    }

    private boolean isWithinWindow(ServiceCommand command) {

        MainframeFunction function = command.getMainframeFunction();

        boolean functionAvailable = availabilityService.isFunctionAvailable(function);

        if(functionAvailable && CIRCUIT_MANAGER.isOpen(function))
            CIRCUIT_MANAGER.reset(function);

        if(! functionAvailable )
            CIRCUIT_MANAGER.open(function);

        return functionAvailable;
    }

    private void schedule(ServiceCommand command) {
        MainframeFunction function = command.getMainframeFunction();

        if(!CIRCUIT_MANAGER.isScheduled(function))
        {
            Slot availableSlot = availabilityService.getNextAvailableSlot(function);
            schedulingService.scheduleJob(availableSlot, CommandToEventMapping.getEventForCommand(command.getClass()));

            CIRCUIT_MANAGER.scheduled(function);
        }


    }

    private void runCommand(ServiceCommand command) {
        command.execute();
    }

    private static class CircuitManager {
        private Map<MainframeFunction, JobCircuit> BREAKERS = new HashMap<>();
        {
            BREAKERS.put(STOP_PAYMENT, new JobCircuit());
            BREAKERS.put(PAYMENT_REVERSAL, new JobCircuit());
        }

        boolean isOpen(MainframeFunction function) {
            return BREAKERS.get(function).isCircuitOpen();
        }

        boolean isScheduled(MainframeFunction function) {
            return BREAKERS.get(function).isJobScheduled();
        }

        void open(MainframeFunction function) {
            BREAKERS.get(function).open();
        }

        void scheduled(MainframeFunction function) {
            BREAKERS.get(function).scheduled();
        }

        void reset(MainframeFunction function) {
            BREAKERS.get(function).reset();
        }

        void resetAll() {
            BREAKERS.values().forEach(JobCircuit::reset);
        }

        class JobCircuit {

            private AtomicBoolean SCHEDULE_CREATED = new AtomicBoolean(false);
            private AtomicBoolean CIRCUIT_OPEN = new AtomicBoolean(false);

            boolean isCircuitOpen() {
                return CIRCUIT_OPEN.get();
            }

            boolean isJobScheduled() {
                return SCHEDULE_CREATED.get();
            }

            void open() {
                CIRCUIT_OPEN.set(true);
            }

            void scheduled() {
                SCHEDULE_CREATED.set(true);
            }

            void reset() {
                SCHEDULE_CREATED.set(false);
                CIRCUIT_OPEN.set(false);
            }
        }
    }
}
