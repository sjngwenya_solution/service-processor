package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import org.springframework.stereotype.Service;
import za.co.fnb.plexus.services.request.businesslogic.command.CommandHandler;
import za.co.fnb.plexus.services.request.businesslogic.service.CommandService;
import za.co.fnb.plexus.services.request.commands.ApplicationCommand;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

@Service
public class CommandServiceBean implements CommandService {

    private Map<Class, CommandHandler> commandRegistry = new HashMap<>();

    @Override
    public void dispatch(ApplicationCommand applicationCommand) {
        Optional.ofNullable(commandRegistry.get(applicationCommand.getClass())).ifPresent(handler -> {
            handler.handle(applicationCommand);
        });
    }

    @Override
    public void registerHandler(Class command, CommandHandler handler) {
        requireNonNull(command, "Command class cannot be null.");
        requireNonNull(handler, "Handler class cannot be null");

        commandRegistry.put(command, handler);
    }
}
