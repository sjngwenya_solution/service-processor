package za.co.fnb.plexus.services.request.businesslogic.service.impl;

public enum EventExecutionResponse {
    SUCCESSFUL, SCHEDULED, FAILED, DUPLICATE,RESCHEDULED
}
