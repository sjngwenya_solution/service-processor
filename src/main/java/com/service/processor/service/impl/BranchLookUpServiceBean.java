package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import za.co.fnb.plexus.services.request.businesslogic.service.BranchLookUpService;
import za.co.fnb.plexus.services.request.domain.Branch;
import za.co.fnb.plexus.services.request.domain.task.BranchLookUp;
import za.co.fnb.plexus.services.request.dto.domain.BranchDTO;
import za.co.fnb.plexus.services.request.infrastructure.adapter.KeystoneAdapter;
import za.co.fnb.plexus.services.request.infrastructure.repository.BranchLookUpRepository;

@Service
public class BranchLookUpServiceBean implements BranchLookUpService {

    private Logger loggingClassLogger = LoggerFactory.getLogger(BranchLookUpServiceBean.class);

    @Autowired
    private KeystoneAdapter keystoneAdapter;

    @Autowired
    private BranchLookUpRepository repository;

    @Override
    public Branch findBranchForHoganBranch(String hoganBranchCode) {
        return repository.findByHoganBranchCode(hoganBranchCode).map(branchLookUp ->
                new Branch(branchLookUp.getCode(), branchLookUp.getDescription(), branchLookUp.getBank())
        ).orElse(null);
    }

    @Async
    @Override
    public void syncBranchData() {
        keystoneAdapter.fetchBranchList().ifPresent(branchDTOS ->
            branchDTOS.forEach(branchDTO -> {
                try {
                    BranchLookUp branch = repository.findByCode(branchDTO.getCode());

                    if(branch == null)
                        branch = new BranchLookUp();

                    copy(branchDTO, branch);

                    repository.save(branch);
                } catch (Exception e) {
                    loggingClassLogger.error(e.getMessage(), e);
                }
            }));
    }

    private void copy(BranchDTO source, BranchLookUp destination) {
        destination.setCode(source.getCode());
        destination.setBank(source.getBank());
        destination.setCompanyId(source.getCompanyId());
        destination.setDescription(source.getDescription());
        destination.setHoganBranchCode(String.valueOf(source.getHoganBranchCode()));
    }
}
