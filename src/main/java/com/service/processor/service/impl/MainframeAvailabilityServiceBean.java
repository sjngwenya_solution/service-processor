package za.co.fnb.plexus.services.request.businesslogic.service.impl;

import lombok.SneakyThrows;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand;
import za.co.fnb.plexus.services.request.businesslogic.service.MainframeAvailabilityService;
import za.co.fnb.plexus.services.request.businesslogic.service.io.Slot;
import za.co.fnb.plexus.services.request.businesslogic.util.common.MainframeTimetable;
import za.co.fnb.plexus.services.request.domain.LookupData;
import za.co.fnb.plexus.services.request.infrastructure.repository.LookupDataRepository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static za.co.fnb.plexus.services.base.exception.ServiceExceptionType.INVALID_REQUEST;


@Service
public class MainframeAvailabilityServiceBean implements MainframeAvailabilityService, InitializingBean {

    private static final SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm");
    private static final String slotFormat = "%s_%s";

    private TimetableRegistry registry;

    private ApplicationContext context;

    @Autowired
    public MainframeAvailabilityServiceBean(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public boolean isFunctionAvailable(ServiceCommand.MainframeFunction mainframeFunction) {
        return registry.getTable(mainframeFunction).isAvailable();
    }

    @Override
    public Slot getNextAvailableSlot(ServiceCommand.MainframeFunction mainframeFunction) {
        Slot slot = new Slot();

        Date futureDateMn = registry.getTable(mainframeFunction).getNextAvailableSlot();
        Date futureDateMX = registry.getTable(mainframeFunction).getLastAvailableSlot();

        slot.setAvailableFrom(futureDateMn);
        slot.setAvailableTo(futureDateMX);

        slot.setSlotName(String.format(slotFormat, mainframeFunction.name(), formatter.format(futureDateMn)));

        return slot;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        registry = new TimetableRegistry(context);
    }

    class TimetableRegistry {

        private ApplicationContext context; //used later to look up repository

        private Map<ServiceCommand.MainframeFunction, MainframeTimetable> registry = new HashMap<>();

        public TimetableRegistry(ApplicationContext context) {
            this.context = context;
        }

        public MainframeTimetable getTable(ServiceCommand.MainframeFunction mainframeFunction) {
            MainframeTimetable mainframeTimetable = registry.get(mainframeFunction);
            if(mainframeTimetable == null) {
                load(mainframeFunction);
            }
            return registry.get(mainframeFunction);
        }

        @SneakyThrows
        private void load(ServiceCommand.MainframeFunction mainframeFunction) {

            LookupDataRepository scheduleRepository = context.getBean(LookupDataRepository.class);
            Optional<LookupData> topByActiveAndFunctionName = scheduleRepository.findTopByActiveAndLookupKey(true, mainframeFunction.name());

            if(topByActiveAndFunctionName.isPresent())
                registry.put(mainframeFunction, new MainframeTimetable( topByActiveAndFunctionName.get().getData()));
            else
                throw new ServiceException(INVALID_REQUEST, mainframeFunction + " schedule not defined.");
        }
    }
}
