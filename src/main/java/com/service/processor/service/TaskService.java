package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.base.exception.ServiceException;
import za.co.fnb.plexus.services.request.domain.task.Action;
import za.co.fnb.plexus.services.request.domain.task.Tasklist;

import java.util.function.BiFunction;
import java.util.function.Function;

public interface TaskService {

    void createList(Long referenceNumber);

    void createList(Long referenceNumber, Long requiredTaskCount);

    void createListIfNotExistWithTask(Long referenceNumber, Action action, String itemReference);

    void createNewListWithTask(Long referenceNumber, String actionReference, Action action);

    Long retrieveWorkItemID(String actionCorrelationId) throws ServiceException;

    Long retryable(String taskCorrelationId) throws ServiceException;

    Long completeTask(String taskCorrelationId);

    Long failTask(String taskCorrelationId);

    <T> T executeFunction(Long referenceNumber, Function<Tasklist, T> check);

    <T> T executeFunction(Long referenceNumber, String filterParameter, BiFunction<Tasklist, String, T> check);
}
