package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.request.domain.Branch;

public interface BranchLookUpService {

    Branch findBranchForHoganBranch(String hoganBranchId);

    void syncBranchData();
}
