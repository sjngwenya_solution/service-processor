package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.request.businesslogic.command.CommandHandler;
import za.co.fnb.plexus.services.request.commands.ApplicationCommand;

public interface CommandService {

    void dispatch(ApplicationCommand applicationCommand);

    void registerHandler(Class aClass, CommandHandler handler);
}
