package com.service.processor.service;


import com.service.processor.service.io.Slot;

public interface SchedulingService {


    void scheduleJobNextHour(String event);

    void scheduleJobNextHour(String event, String eventStatus);

    void scheduleJob(Slot timeSlot, String eventType, String eventStatus);

    JobId scheduleJob(Slot timeSlot, String event);
}
