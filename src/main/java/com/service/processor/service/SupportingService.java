package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.request.dto.DebitOrderDetailsDTO;
import za.co.fnb.plexus.services.request.dto.DebitOrderSearchCriteria;
import za.co.fnb.plexus.services.request.dto.common.Product;
import za.co.fnb.plexus.services.request.dto.customer.CustomerAndAccountSearchRequest;
import za.co.fnb.plexus.services.request.dto.customer.CustomerProfile;
import za.co.fnb.plexus.services.request.dto.customer.account.AccountDTO;
import za.co.fnb.plexus.services.request.dto.customer.account.RFAccountDetailsDTO;

import java.util.List;

public interface SupportingService {

    CustomerProfile customerSearchByUcn(Long ucn);

    AccountDTO searchForAccount(String accountNumber, Product product);

    List<CustomerProfile> customerSearchByIdNumber(String identityNumber);

    DebitOrderDetailsDTO searchProfileAndAccount(CustomerAndAccountSearchRequest searchRequest);

     boolean isExistingMandateInFlight(String applicationNumber);
}
