package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.request.dto.dispute.ServiceRequestAuditDTO;

public interface AuditLogService {
    void log(ServiceRequestAuditDTO serviceRequestAuditDTO);
}
