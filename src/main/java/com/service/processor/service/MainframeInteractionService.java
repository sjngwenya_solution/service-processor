package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.request.businesslogic.command.ServiceCommand;
import za.co.fnb.plexus.services.request.businesslogic.service.impl.EventExecutionResponse;

public interface MainframeInteractionService {

    EventExecutionResponse execute(ServiceCommand command);
}
