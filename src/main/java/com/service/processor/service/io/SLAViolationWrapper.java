package za.co.fnb.plexus.services.request.businesslogic.service.io;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SLAViolationWrapper<T> {

    public enum VIOLATION_TYPE { DEBIT_ORDER_DISPUTE, AUTOMATED_DEBIT_ORDER_DISPUTE,DEBICHECK_MANDATE}

    private T violation;
    private VIOLATION_TYPE requestType;

    public SLAViolationWrapper(T violation, VIOLATION_TYPE requestType) {
        this.violation = violation;
        this.requestType = requestType;
    }
}
