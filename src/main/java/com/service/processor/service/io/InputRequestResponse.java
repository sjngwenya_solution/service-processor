package za.co.fnb.plexus.services.request.businesslogic.service.io;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

import java.util.HashMap;
import java.util.Map;

@Data
@RequiredArgsConstructor
public abstract class InputRequestResponse<T>  {

    @NonNull protected T payload;
    @NonNull protected RequestType requestType;

    protected Map<String, String> parameters = new HashMap<>();

    public void addParam(String key, String value) {
        parameters.put(key, value);
    }

    public String getParam(String key) {
        return parameters.getOrDefault(key, "");
    }
}
