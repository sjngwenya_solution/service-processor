package za.co.fnb.plexus.services.request.businesslogic.service.io;

import lombok.NonNull;
import za.co.fnb.plexus.services.request.dto.domain.request.RequestType;

public class IncomingResponse extends InputRequestResponse {

    public IncomingResponse(Object request, @NonNull RequestType requestType) {
        super(request, requestType);
    }
}
