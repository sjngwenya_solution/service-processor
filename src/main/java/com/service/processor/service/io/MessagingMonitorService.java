package za.co.fnb.plexus.services.request.businesslogic.service.io;

import za.co.fnb.plexus.services.request.dto.QueueDetails;

import java.util.List;

public interface MessagingMonitorService {

    List<QueueDetails> checkQueues();
}
