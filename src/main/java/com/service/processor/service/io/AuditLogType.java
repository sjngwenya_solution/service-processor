package za.co.fnb.plexus.services.request.businesslogic.service.io;

public enum AuditLogType {

    ServiceRequest("Create_Work_Request"),
    ServiceResponse("Create_Work_Response"),
    WorkItemCreated("Create_Work_Success"),
    ServiceRequestValidation("Create_Work_Request_Failure"),
    StatusUpdateEvent("Status_update_event"),

    //Post 40 specifics
    StatusUpdateResponseProcessingFailed("Status_Update_Response_Failed_Processing"),
    StatusUpdateRequest("Status_Update_Request"),
    StatusUpdateResponseSuccess("Status_Update_Response_Success"),
    StatusUpdateResponseFailure("Status_Update_Response_Failure"),
    StatusUpdateOutOfSync("Status_Update_Out_Of_Sync"),

    //ADO Specifics
    CreateStopPaymentRequest("Create_StopPayment_Request"),
    PaymentReversalRequest("Payment_Reversal_Request"),
    CreateStopPaymentResponse("Create_StopPayment_Response"),
    PaymentReversalResponse("Payment_Reversal_Response"),

    //DebiCheck specifics
    DebiCheckMandateRequest("DebiCheck_Mandate_Request"),
    DebiCheckEnrichmentData("DebiCheck_Enriched_Data"),
    DebiCheckMandateDuplicate("DebiCheck_Mandate_Duplicate_Request"),
    DebtCheckMandateValidationErrors("DebtCheck_Mandate_Request_Validation_Errors"),
    CreateMandateRequest("Create_Mandate_Request"),
    SendRequestToMandateService("Send_Request_To_Mandate_Service"),
    CancelMandateRequest("Cancel_Mandate_Request"),
    CancelMandateResponse("Cancel_Mandate_Response"),
    MandateServiceResponse("Mandate_Service_Response"),
    MandateRequestStatusCode("Mandate_Service_Request_status"),
    MandateAcceptedResponse("Mandate_Accepted_Response"),
    MandateExceptionResponse("Mandate_Exception_Response");

    private String auditLogType;

    AuditLogType(String auditLogType) {
        this.auditLogType = auditLogType;
    }

    public String getAuditLogType() {
        return auditLogType;
    }
}
