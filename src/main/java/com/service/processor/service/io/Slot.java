package za.co.fnb.plexus.services.request.businesslogic.service.io;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class Slot {

    private String slotName;
    private Date availableFrom;
    private Date availableTo;

    public Slot(Date availableFrom) {
        this.availableFrom = availableFrom;
    }

    public Slot(String slotName, Date availableFrom, Date availableTo) {
        this.slotName = slotName;
        this.availableFrom = availableFrom;
        this.availableTo = availableTo;
    }
}


