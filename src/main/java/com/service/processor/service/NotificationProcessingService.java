package za.co.fnb.plexus.services.request.businesslogic.service;

import za.co.fnb.plexus.services.request.dto.workflow.WorkflowEventDTO;

public interface NotificationProcessingService {
    void processFileNotification(String message);
    void processAdoSLANotification(WorkflowEventDTO workflowEventDTO);
}
