package com.service.processor.dto;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { BookRequestValidator.class })
public @interface IsBookRequest {

    String message() default "DebitCheckMandateRequest is invalid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
