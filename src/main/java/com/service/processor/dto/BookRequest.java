package com.service.processor.dto;


import lombok.Data;

@IsBookRequest
@Data
public class BookRequest {
    private double price;
    private int quantity;
    private String title;
    private String type;
    private String reference;
}
