package com.service.processor.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class BookRequestValidator implements ConstraintValidator<IsBookRequest, BookRequest> {
    private static final ObjectMapper SERIALIZER = new ObjectMapper();

    @Override
    public boolean isValid(BookRequest bookRequest, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        Map<String, String> validationErrors = new HashMap<>();

        notEmptyValidation("Title", bookRequest.getTitle(), validationErrors);
        notEmptyValidation("Price", String.valueOf(bookRequest.getPrice()), validationErrors);
        notEmptyValidation("Reference", bookRequest.getReference(), validationErrors);
        notEmptyValidation("Quantity", String.valueOf(bookRequest.getQuantity()), validationErrors);

        valueInRange("Book Type",  bookRequest.getType(),
                Arrays.asList("ACADEMIC", "RELIGIOUS","LEADERSHIP"), validationErrors);

        if( ! validationErrors.isEmpty()) {
            String errorMsg = "Failed validations";
            try {errorMsg = SERIALIZER.writeValueAsString(validationErrors);  } catch (Exception e) {}
            context.buildConstraintViolationWithTemplate(errorMsg).addConstraintViolation();

            //How do we audit these? validationErrors
            return false;
        }else {
            return true;
        }
    }

    private void notEmptyValidation(String columnName, String value, Map<String, String> validationErrors) {
        if(StringUtils.isBlank(value)) {
            //Audit ::: errorColumn
            validationErrors.put(columnName, "Required field missing");
        }
    }
    private void valueInRange(String columnName, String value, Collection<String> validValues,
                              Map<String, String> validationErrors) {
        if(!validValues.contains(value)) {
            validationErrors.put(columnName, "Contains unexpected value : " + value);
        }
    }
}
